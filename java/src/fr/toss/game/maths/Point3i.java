package fr.toss.game.maths;

public class Point3i
{
	private int	_x;
	private int	_y;
	private int	_z;
	
	public Point3i(int x, int y, int z)
	{
		this._x = x;
		this._y = y;
		this._z = z;
	}
	
	public int	getX()
	{
		return (this._x);
	}

	public int	getY()
	{
		return (this._y);
	}
	
	public int	getZ()
	{
		return (this._z);
	}
}
