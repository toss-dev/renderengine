package fr.toss.game.maths;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

import fr.toss.game.renderer.camera.Camera;

public class MatrixUtils
{
	public static Matrix4f	loadProjectionMatrix(Camera camera, float aspect)
	{
		Matrix4f	matrix;
		float		x_scale;
		float		y_scale;
		float		frustrum;

		matrix = new Matrix4f();
		y_scale = (float)((1.0f / Math.tan(Math.toRadians(camera.getFov() / 2.0f))) * aspect);
		x_scale = y_scale / aspect;
		frustrum = camera.getFar() - camera.getNear();
		matrix.m00 = x_scale;
		matrix.m11 = y_scale;
		matrix.m22 = -((camera.getFar() + camera.getNear()) / frustrum);
		matrix.m23 = -1;
		matrix.m32 = -((2 * camera.getNear() * camera.getFar()) / frustrum);
		matrix.m33 = 0;
		return (matrix);
	}

	public static Point3	vectorPitchYaw(float pitch, float yaw)
	{
		double	pitchcos;

		pitchcos = Math.cos(pitch);
		return (new Point3(pitchcos * Math.sin(-yaw), Math.sin(pitch), pitchcos * Math.cos(yaw)));
	}

	public static Matrix4f loadTransformationMatrix(Vector3f position, Vector3f rotation, Vector3f scale)
	{
		Matrix4f	matrix;
		
		matrix = new Matrix4f();
		matrix.setIdentity();
		Matrix4f.translate(position, matrix, matrix);
		Matrix4f.rotate((float) Math.toRadians(rotation.x), new Vector3f(1, 0, 0), matrix, matrix);
		Matrix4f.rotate((float) Math.toRadians(rotation.y), new Vector3f(0, 1, 0), matrix, matrix);
		Matrix4f.rotate((float) Math.toRadians(rotation.z), new Vector3f(0, 0, 1), matrix, matrix);
		Matrix4f.scale(scale, matrix, matrix);
		return (matrix);
	}
}
