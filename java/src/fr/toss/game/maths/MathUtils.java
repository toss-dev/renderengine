package fr.toss.game.maths;

public class MathUtils
{
	public static float	max(float a, float b)
	{
		return (a > b ? a : b);
	}
	
	public static float	min(float a, float b)
	{
		return (a < b ? a : b);
	}
	
	public static int	fastfloor(double x)
	{
		int xi;

		xi = (int)x;
		return (x < xi) ? (xi - 1) : xi;
	}

	public static float	clamp(float val, float m, float mm)
	{
		return (max(m, min(mm, val)));
	}
}
