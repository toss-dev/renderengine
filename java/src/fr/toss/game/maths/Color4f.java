package fr.toss.game.maths;

import org.lwjgl.util.vector.Vector3f;

public class Color4f
{
	private float	_r;
	private float	_g;
	private float	_b;
	private float	_a;
	
	public Color4f(float r, float g, float b, float a)
	{
		this._r = r;
		this._g = g;
		this._b = b;
		this._a = a;
	}
	
	public Color4f	setRed(float r)
	{
		this._r = r;
		return (this);
	}
	
	public Color4f	setGreen(float g)
	{
		this._g = g;
		return (this);
	}
	
	public Color4f	setBlue(float b)
	{
		this._b = b;
		return (this);
	}
	
	public float	getRed()
	{
		return (this._r);
	}
	
	public float	getGreen()
	{
		return (this._g);
	}
	
	public float	getBlue()
	{
		return (this._b);
	}
	
	public float	getAlpha()
	{
		return (this._a);
	}

	public Vector3f toVec3f()
	{
		return (new Vector3f(this.getRed(), this.getGreen(), this.getBlue()));
	}
}
