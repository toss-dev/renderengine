package fr.toss.game.maths;

import org.lwjgl.util.vector.Vector3f;

public class Point3 extends Vector3f
{
	private static final long serialVersionUID = 1L;

	public Point3(double x, double y, double z)
	{
		super((float)x, (float)y, (float)z);
	}
	
	public Point3(float x, float y, float z)
	{
		super(x, y, z);
	}

	/** convert to a float array {x, y, z} */
	public float[]	toFloatArray()
	{
		float	array[];
		
		array = new float[3];
		array[0] = this.x;
		array[1] = this.y;
		array[2] = this.z;
		return (array);
	}

	/** add a point to the current instance */
	public void add(Vector3f vec)
	{
		this.setX(this.getX() + vec.getX());
		this.setY(this.getY() + vec.getY());
		this.setZ(this.getZ() + vec.getZ());
	}
	
	/** add a point {x * factor, y * factor, z * factor to the current instance */
	public void addFactor(Vector3f vec, float factor)
	{
		this.setX(this.getX() + vec.getX() * factor);
		this.setY(this.getY() + vec.getY() * factor);
		this.setZ(this.getZ() + vec.getZ() * factor);
	}

	public Point3 reverseSign()
	{
		return (new Point3(-this.x, -this.y, -this.z));
	}
	
}
