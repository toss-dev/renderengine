package fr.toss.game;

import org.lwjgl.glfw.GLFW;
import org.lwjgl.opengl.GL11;

import fr.toss.game.logger.Logger;
import fr.toss.game.logger.LoggerID;
import fr.toss.game.renderer.Renderer;
import fr.toss.game.world.World;

public class Game
{
	public static Game	instance;
	
	private World		_world;
	private Renderer	_renderer;
	private Timer		_timer;
	private Logger		_logger;

	public Game()
	{
		instance = this;
		this._logger = new Logger(System.out);
		this._world = new World();
		this._renderer = new Renderer();
		this._timer = new Timer();
	}
	
	public void	start() throws Exception
	{
		Game.log("Starting ...", LoggerID.FINE);
		this._renderer.start();
		this._world.start();
		Game.log("Started.", LoggerID.FINE);
	}
	
	public void	loop()
	{
		Game.log("Looping...", LoggerID.FINE);
	    while (GLFW.glfwWindowShouldClose(this._renderer.getWindow().getPtr()) == GL11.GL_FALSE)
	    {
	    	this._timer.prepare();
	    	this._world.update();
	    	this._renderer.update(this._world);
	    	this._renderer.render(this._world, this._renderer.getWindow());
	    	GLFW.glfwPollEvents();
	        this.checkGLErrors();
	        this._timer.end();
	    }
	    Game.log("Loop ended", LoggerID.FINE);
	}
	
	private void checkGLErrors()
	{
		int	err;
		
		err = GL11.glGetError();
		if (err != 0)
		{
			Game.log("An OpenGL error occured, error code: " + err, LoggerID.WARNING);			
		}
	}

	public void end()
	{
		Game.log("Ending ...", LoggerID.FINE);
		this._world.stop();
		this._renderer.stop();
		this._logger.stop();
		this._timer.stop();
	}

	public static void	log(String str, LoggerID id)
	{
		Game.instance.getLogger().log(str, id);
	}
	
	public World	getWorld()
	{
		return (this._world);
	}
	
	public Renderer	getRenderer()
	{
		return (this._renderer);
	}
	
	public Logger	getLogger()
	{
		return (this._logger);
	}
	
	public Timer	getTimer()
	{
		return (this._timer);
	}
}
