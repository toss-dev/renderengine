package fr.toss.game;

public class Main
{	
	public static void main(String[] args)
	{
		Game	game;
		
		game = new Game();
		try {
			game.start();
		} catch (Exception e) {
			System.err.println("An error occured while starting program, stopping...");
			return ;
		}
		game.loop();
		game.end();
	}
}
