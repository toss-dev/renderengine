package fr.toss.game.entity;

import fr.toss.game.maths.Point3;
import fr.toss.game.renderer.Textures;
import fr.toss.game.renderer.model.Model;
import fr.toss.game.world.World;
import fr.toss.game.world.chunk.Chunk;

public abstract class AEntity
{
	private static final int	_DEFAULT_MOVE_SPEED 	= 1;
	private static final int	_DEFAULT_ROT_SPEED 	= 1;
	private static final int	_DEFAULT_SCALE_SPEED = 1;
	
	private static int _CURRENT_ID = 0;
	
	private World	_world;
	
	private Model	_model;
	
	/** position, rotation and scaling for this entity */
	private Point3	_position;
	private Point3	_rotation;
	private Point3	_scale;
	
	/** position, rotation and scaling vector for this entity*/
	private Point3	_position_vel;
	private Point3	_rotation_vel;
	private Point3	_scale_vel;
	
	/** entity textureID */
	private int			_textureID;
	
	/** entity unique ID */
	private int			_id;
	
	/** entity speed */
	private float		_move_speed;
	private float		_rot_speed;
	private float		_scale_speed;
	
	/** 
	 * @param pos : entity's position
	 * @param rot : entity's rotation
	 * @param sca : entity's scaling
	 * @param textID : entity's texture, cf Textures.TEXTURE_*
	 */
	public AEntity(Point3 pos, Point3 rot, Point3 sca, int textID, Model model)
	{
		this._world = null;
		this._model = model;
		this._position = pos;
		this._rotation = rot;
		this._scale = sca;
		this._position_vel = new Point3(0, 0, 0);
		this._rotation_vel = new Point3(0, 0, 0);
		this._scale_vel = new Point3(0, 0, 0);
		this._move_speed = _DEFAULT_MOVE_SPEED;
		this._rot_speed = _DEFAULT_ROT_SPEED;
		this._scale_speed = _DEFAULT_SCALE_SPEED;
		this._id = _CURRENT_ID;
		this._textureID = textID;
		_CURRENT_ID++;
	}
	
	public AEntity(Model model)
	{
		this(new Point3(0, 0, 0), new Point3(0, 0, 0), new Point3(10, 10, 10), Textures.CHUNK, model);
	}
	
	public void	updateMove()
	{
		this._position.addFactor(this._position_vel, this._move_speed);
		this._rotation.addFactor(this._rotation_vel, this._rot_speed);
		this._scale.addFactor(this._scale_vel, this._scale_speed);
	}
	
	public abstract void update();

	public World	getWorld()
	{
		return (this._world);
	}
	
	public Model	getModel()
	{
		return (this._model);
	}
	
	public void	setModel(Model model)
	{
		this._model = model;
	}
	
	public Point3 getPosition()
	{
		return (this._position);
	}

	public void setPosition(Point3 pos)
	{
		this._position = pos;
	}

	public Point3 getRotation()
	{
		return (this._rotation);
	}

	public void setRotation(Point3 rot)
	{
		this._rotation = rot;
	}

	public Point3 getScale()
	{
		return (this._scale);
	}

	public void setScale(Point3 scale)
	{
		this._scale = scale;
	}

	public Point3 getPositionVel()
	{
		return (this._position_vel);
	}

	public void setPositionVel(Point3 pos_vel)
	{
		this._position_vel = pos_vel;
	}

	public Point3 getRotationVel()
	{
		return (this._rotation_vel);
	}

	public void setRotationVel(Point3 rot)
	{
		this._rotation_vel = rot;
	}

	public Point3 getScaleVel()
	{
		return (this._scale_vel);
	}

	public void setScaleVel(Point3 scale)
	{
		this._scale_vel = scale;
	}
	
	public void	 increasePosition(Point3 vec)
	{
		this._position.add(vec);
	}
	
	public void	 increaseRotation(Point3 vec)
	{
		this._rotation.add(vec);
	}
	
	public void	 increaseScale(Point3 vec)
	{
		this._scale.add(vec);
	}

	public int getTextureID()
	{
		return (this._textureID);
	}

	public void setTextureID(int id)
	{
		this._textureID = id;
	}

	public int getUniqueID()
	{
		return (this._id);
	}
	
	public Chunk	getCurrentChunk()
	{
		return (this._world.getChunkForPos(this._position));
	}

	public void setWorld(World world)
	{
		this._world = world;
	}
	
	public String	toString()
	{
		return ("Entity: ID: " + this._id + " ; " + this.getModel().toString());
	}
}
