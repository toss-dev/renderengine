package fr.toss.game.entity;

import fr.toss.game.maths.Point3;
import fr.toss.game.renderer.model.Model;
import fr.toss.game.renderer.model.ModelLoader;

public class EntityCube extends AEntity
{
	public EntityCube(Point3 pos, Point3 rot, Point3 sca, int textID) 
	{
		super(pos, rot, sca, textID, ModelLoader.MODEL_CUBE);
	}

	public EntityCube(Model model)
	{
		super(model);
	}

	@Override
	public void update()
	{
		super.updateMove();
	}

}
