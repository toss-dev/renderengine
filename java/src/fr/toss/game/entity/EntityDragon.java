package fr.toss.game.entity;

import fr.toss.game.maths.Point3;
import fr.toss.game.renderer.Textures;
import fr.toss.game.renderer.model.ModelLoader;

public class EntityDragon extends AEntity
{
	public EntityDragon(Point3 pos) 
	{
		super(pos, new Point3(0, 0, 0), new Point3(5, 5, 5), Textures.CHUNK, ModelLoader.MODEL_DRAGON);
	}

	@Override
	public void update()
	{
		super.updateMove();
	}

}
