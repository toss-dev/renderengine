package fr.toss.game.event;

import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWKeyCallback;
import org.lwjgl.opengl.GL11;

import fr.toss.game.Game;

public class KeyEvent extends GLFWKeyCallback
{
	@Override
	public void invoke(long window, int key, int scancode, int action, int mods)
	{
		if (key == GLFW.GLFW_KEY_ESCAPE)
		{
			GLFW.glfwSetWindowShouldClose(window, GL11.GL_TRUE);
		}
		if (action == GLFW.GLFW_RELEASE)
		{
			Game.instance.getRenderer().getCamera().getControl().onKeyRelease(key);
		}
		else if (action == GLFW.GLFW_PRESS)
		{
			Game.instance.getRenderer().getCamera().getControl().onKeyPress(key);
		}
	}
}
