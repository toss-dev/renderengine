package fr.toss.game.event;

import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWCursorPosCallback;
import org.lwjgl.glfw.GLFWKeyCallback;
import org.lwjgl.glfw.GLFWMouseButtonCallback;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GLContext;

import fr.toss.game.Game;
import fr.toss.game.logger.LoggerID;

public class Window
{
	private String	_title;
	private int 	_width;
	private int 	_height;
	private long 	_ptr;
	
	private GLFWKeyCallback _keyEvent;
	private GLFWCursorPosCallback _mouseCursorEvent;
	private GLFWMouseButtonCallback _mouseButtonEvent;
	
	public Window()
	{
		this._title = "Render Engine";
		this._width = 1000;
		this._height = (int) (this._width / 1.6f);
		this._ptr = 0;
	}
	
	public int getWidth()
	{
		return (this._width);
	}
	
	public int getHeight()
	{
		return (this._height);
	}
	
	public long getPtr()
	{
		return (this._ptr);
	}

	/** throw an Exception if an initalizing error occurs */
	public void start() throws Exception
	{
		Game.log("Opening window ...", LoggerID.FINE);
		if (GLFW.glfwInit() == GL11.GL_FALSE)
		{
			Game.log("Couldnt initialize glfw", LoggerID.ERROR);
			throw new Exception();
		}
		this._ptr = GLFW.glfwCreateWindow(this._width, this._height, this._title, 0, 0);
		if (this._ptr == 0)
		{
			Game.log("Couldnt create glfw window", LoggerID.ERROR);
			GLFW.glfwTerminate();
			throw new Exception();
		}
		GLFW.glfwMakeContextCurrent(this._ptr);
		GLFW.glfwSwapInterval(1);
		GLFW.glfwShowWindow(this._ptr);
        GLContext.createFromCurrent();
        this.loadCallbacks();
        Game.log("Window oppened: " + this._ptr, LoggerID.FINE);
	}

	private void loadCallbacks()
	{
		this._keyEvent = new KeyEvent();
		this._mouseCursorEvent = new MouseCursorEvent();
		this._mouseButtonEvent = new MouseButtonEvent();
		GLFW.glfwSetKeyCallback(this._ptr, this._keyEvent);
		GLFW.glfwSetCursorPosCallback(this._ptr, this._mouseCursorEvent);
		GLFW.glfwSetMouseButtonCallback(this._ptr, this._mouseButtonEvent);
	}

	/** clean up on program ended */
	public void stop()
	{
		Game.log("Stopping window...", LoggerID.FINE);
	}

	public float getAspect()
	{
		return (this._width / (float)this._height);
	}
}
