package fr.toss.game.logger;

import java.io.PrintStream;

import fr.toss.game.Game;

public class Logger
{
	private PrintStream _stream;

	public Logger(PrintStream stream)
	{
		this._stream = stream;
	}
	
	public void	log(String str, LoggerID id)
	{
		this._stream.print("[Render Engine] - ");
		this._stream.print("[" + id.toString() + "] - ");
		this._stream.println(str);
	}
	
	/** clean up on program ended */
	public void stop()
	{
		Game.log("Stopping logger...", LoggerID.FINE);
	}
}
