package fr.toss.game.logger;

public enum	LoggerID
{
	FINE("FINE"),
	WARNING("WARNING"),
	DEBUG("DEBUG"),
	ERROR("ERROR");
	
	private String	_str;
	
	LoggerID(String str)
	{
		this._str = str;
	}
	
	public String	toString()
	{
		return (this._str);
	}
}