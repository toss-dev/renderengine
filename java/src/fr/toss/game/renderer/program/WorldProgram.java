package fr.toss.game.renderer.program;

import java.util.ArrayList;

import org.lwjgl.opengl.GL20;
import org.lwjgl.util.vector.Matrix4f;

import fr.toss.game.maths.MatrixUtils;
import fr.toss.game.maths.Point3;
import fr.toss.game.renderer.Light;
import fr.toss.game.renderer.Renderer;
import fr.toss.game.renderer.camera.Camera;
import fr.toss.game.world.World;

public abstract class WorldProgram extends AProgram
{
	private static final int MAX_LIGHTS = 4;
	
	private int	_viewMatrix;
	private int	_projectionMatrix;
	private int	_fogColor;
	private int	_fogDensity;
	private int	_fogGradient;
	private int _lightPosition[];
	private int _lightColor[];
	private int _lightAttenuation[];
	
	public WorldProgram()
	{
		this._viewMatrix = 0;
		this._projectionMatrix = 0;
		this._fogColor = 0;
		this._fogDensity = 0;
		this._fogGradient = 0;
		this._lightPosition = new int[MAX_LIGHTS];
		this._lightColor = new int[MAX_LIGHTS];
		this._lightAttenuation = new int[MAX_LIGHTS];
	}
	
	@Override
	public void loadFromFiles(String vertexFile, String fragmentFile)
	{
		super.loadFromFiles(vertexFile, fragmentFile);
	}
	
	@Override
	protected void bindAttributesList() {}

	@Override
	protected void getAllUniforms()
	{
		this._viewMatrix = GL20.glGetUniformLocation(this.getID(), "view_matrix");
		this._projectionMatrix = GL20.glGetUniformLocation(this.getID(), "proj_matrix");
		this._fogColor = GL20.glGetUniformLocation(this.getID(), "fog_color");
		this._fogDensity = GL20.glGetUniformLocation(this.getID(), "fog_gradient");
		this._fogGradient = GL20.glGetUniformLocation(this.getID(), "fog_density");
		
		for (int i = 0 ; i < 4 ; i++)
		{
			this._lightColor[i] = GL20.glGetUniformLocation(this.getID(), "light_color[" + i + "]");
			this._lightPosition[i] = GL20.glGetUniformLocation(this.getID(), "light_position[" + i + "]");
			this._lightAttenuation[i] = GL20.glGetUniformLocation(this.getID(), "light_attenuation[" + i + "]");
		}
	}

	/** load global world uniform variables */
	public void loadUniformWorld(World world, Renderer renderer)
	{
		this.loadUniformMatrix(this.getProjectionMatrixID(), MatrixUtils.loadProjectionMatrix(renderer.getCamera(), renderer.getWindow().getAspect()));
		this.loadUniformMatrix(this.getViewMatrixID(), renderer.getCamera().getViewMatrix());
		this.loadUniformVec(this.getFogColorID(), renderer.getFogColor().toVec3f());
		this.loadUniformFloat(this.getFogDensityID(), renderer.getFogDensity());
		this.loadUniformFloat(this.getFogGradientID(), renderer.getFogGradient());
		this.loadUniformLight(world, renderer);
	}
	
	private void loadUniformLight(World world, Renderer renderer)
	{
		ArrayList<Light>	lights;
		Light	light;
		int		i;
	
		i = 0;
		lights = renderer.getLights();
		while (i < 4)
		{
			if (i < lights.size())
			{
				light = lights.get(i);
			}
			else
			{
				light = new Light(new Point3(0, 0, 0), new Point3(0, 0, 0), new Point3(1, 0, 0));
			}
			this.loadUniformVec(this.getLightColorID(i), light.getColor());
			this.loadUniformVec(this.getLightPositionID(i), light.getPos());
			this.loadUniformVec(this.getLightAttenuationID(i), light.getAttenuation());
			i++;
		}
	}
	
	public int getViewMatrixID()
	{
		return (this._viewMatrix);
	}
	
	public int getProjectionMatrixID()
	{
		return (this._projectionMatrix);
	}
	
	public int getFogColorID()
	{
		return (this._fogColor);
	}
	
	public int getFogDensityID()
	{
		return (this._fogDensity);
	}
	
	public int getFogGradientID()
	{
		return (this._fogGradient);
	}

	public int	getLightPositionID(int index)
	{
		return (this._lightPosition[index]);
	}
	
	public int	getLightColorID(int index)
	{
		return (this._lightColor[index]);
	}
	
	public int	getLightAttenuationID(int index)
	{
		return (this._lightAttenuation[index]);
	}
}
