package fr.toss.game.renderer.program;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;

import fr.toss.game.Game;
import fr.toss.game.logger.LoggerID;

public class Shader
{
	private String	_filename;
	private int		_type;
	private int		_id;
	
	public Shader()
	{
		this._id = 0;
		this._filename = null;
	}
	
	public Shader(String filename, int type)
	{
		this._filename = filename;
		this._type = type;
	}
	
	public void	setFilename(String filename)
	{
		this._filename = filename;
	}
	
	public void	setType(int type)
	{
		this._type = type;
	}

	
	/** load from current set filename */
	public void	load()
	{
		byte[]	rawfile;
		String	filecontent;
		String	path;
		
		path = "../assets/shaders/" + this._filename;
		Game.log("Loading shader: " + path, LoggerID.FINE);
		try
		{
			rawfile = Files.readAllBytes(Paths.get(path));
			filecontent = new String(rawfile);
			this._id = GL20.glCreateShader(this._type);
			GL20.glShaderSource(this._id, filecontent);
			GL20.glCompileShader(this._id);
			if (GL20.glGetShaderi(this._id, GL20.GL_COMPILE_STATUS) == GL11.GL_FALSE)
			{
				Game.log("Couldnt compile shader: " + this._filename, LoggerID.ERROR);
				Game.log(GL20.glGetShaderInfoLog(this._id, 512), LoggerID.ERROR);
				return ;
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
			Game.log("Error occured while loading: " + this._filename, LoggerID.ERROR);
		}
	}

	public int	getID()
	{
		return (this._id);
	}
}
