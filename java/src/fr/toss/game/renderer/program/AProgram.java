package fr.toss.game.renderer.program;

import java.nio.FloatBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL20;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

public abstract class AProgram
{
	private static FloatBuffer matrix_buffer = BufferUtils.createFloatBuffer(4 * 4);

	private int		_id;
	private Shader	_vertexShader;
	private Shader	_fragmentShader;
	
	public AProgram()
	{
		this._id = 0;
		this._vertexShader = null;
		this._fragmentShader = null;
	}
	
	public AProgram(String vertexFile, String fragmentFile)
	{
		this.loadFromFiles(vertexFile, fragmentFile);
	}
	
	public void loadFromFiles(String vertexFile, String fragmentFile)
	{
		this._id = GL20.glCreateProgram();
		this._vertexShader = new Shader(vertexFile, GL20.GL_VERTEX_SHADER);
		this._fragmentShader = new Shader(fragmentFile, GL20.GL_FRAGMENT_SHADER);
		this._vertexShader.load();
		this._fragmentShader.load();
		GL20.glAttachShader(this._id, this._vertexShader.getID());
		GL20.glAttachShader(this._id, this._fragmentShader.getID());
		this.bindAttributesList();
		GL20.glLinkProgram(this._id);
		GL20.glValidateProgram(this._id);
		this.getAllUniforms();
	}
	
	/** @return the _id */
	public int getID()
	{
		return (this._id);
	}

	/** @return the _vertexShader */
	public Shader getVertexShader()
	{
		return (this._vertexShader);
	}

	/** @param _vertexShader the _vertexShader to set */
	public void setVertexShader(Shader vertexShader)
	{
		this._vertexShader = vertexShader;
	}

	/** @return the _fragmentShader */
	public Shader getFragmentShader()
	{
		return (this._fragmentShader);
	}

	/** @param _fragmentShader the _fragmentShader to set */
	public void setFragmentShader(Shader fragmentShader)
	{
		this._fragmentShader = fragmentShader;
	}
		
	public void	loadUniformMatrix(int location, Matrix4f matrix)
	{
		matrix.store(matrix_buffer);
		matrix_buffer.flip();
		GL20.glUniformMatrix4fv(location, false, matrix_buffer);
	}

	public void	loadUniformFloat(int id, float value)
	{
		GL20.glUniform1f(id, value);
	}

	public void	loadUniformVec(int id, Vector3f vec)
	{
		GL20.glUniform3f(id, vec.x, vec.y, vec	.z);
	}

	public void	loadUniformInt(int id, int integer)
	{
		GL20.glUniform1i(id, integer);
	}

	public void use()
	{
		GL20.glUseProgram(this.getID());
	}

	public void stopUse()
	{
		GL20.glUseProgram(0);
	}
	
	public void	stop()
	{
		GL20.glDetachShader(this.getID(), this.getVertexShader().getID());
		GL20.glDetachShader(this.getID(), this.getFragmentShader().getID());
		GL20.glDeleteShader(this.getVertexShader().getID());
		GL20.glDeleteShader(this.getFragmentShader().getID());
		GL20.glDeleteProgram(this.getID());
	}
	
	/** bind attributes (`in` variables in shaders code */
	protected abstract void	bindAttributesList();
	
	/** bind attributes (`uniforms` variables in shaders code */
	protected abstract void	getAllUniforms();

	
	public enum	ATTR
	{
		POS,
		UVS,
		NORMALS
	}


}
