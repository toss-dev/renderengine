package fr.toss.game.renderer.camera;

import org.lwjgl.glfw.GLFW;

import fr.toss.game.maths.MatrixUtils;
import fr.toss.game.maths.Point3;

public class CameraControl
{
	private static final	int C_MOVE_FORWARD	= 1;
	private static final	int C_MOVE_BACKWARD	= 2;
	private static final	int C_MOVE_LEFT		= 4;
	private static final	int C_MOVE_RIGHT 	= 8;
	
	private Camera	_camera;
	
	public CameraControl(Camera camera)
	{
		this._camera = camera;
	}

	public void	onUpdate()
	{
		Point3	direction;
		
		direction = MatrixUtils.vectorPitchYaw(this._camera.getPitch(), this._camera.getYaw());
		if (this._camera.hasState(C_MOVE_FORWARD))
		{
			this._camera.setPositionVel(direction.reverseSign());
		}
		else if (this._camera.hasState(C_MOVE_BACKWARD))
		{
			this._camera.setPositionVel(direction);
		}
		else if (this._camera.hasState(C_MOVE_LEFT))
		{
			this._camera.setPositionVel(new Point3(-direction.getZ(), 0, direction.getX()));
		}
		else if (this._camera.hasState(C_MOVE_RIGHT))
		{
			direction = direction.reverseSign();
			this._camera.setPositionVel(new Point3(-direction.getZ(), 0, direction.getX()));
		}
		else
		{
			this._camera.setPositionVel(new Point3(0, 0, 0));
		}
	}

	public void	onKeyRelease(int key)
	{
		if (key == GLFW.GLFW_KEY_UP|| key == GLFW.GLFW_KEY_DOWN || key == GLFW.GLFW_KEY_RIGHT || key == GLFW.GLFW_KEY_LEFT)
			this._camera.setRotationVel(new Point3(0, 0, 0));
		if (key == GLFW.GLFW_KEY_W)
			this._camera.disableState(C_MOVE_FORWARD);
		else if (key == GLFW.GLFW_KEY_S)
			this._camera.disableState(C_MOVE_BACKWARD);
		else if (key == GLFW.GLFW_KEY_A)
			this._camera.disableState(C_MOVE_LEFT);
		else if (key == GLFW.GLFW_KEY_D)
			this._camera.disableState(C_MOVE_RIGHT);
	}

	public void	onKeyPress(int key)
	{
		if (key == GLFW.GLFW_KEY_S)
			this._camera.toggleState(C_MOVE_BACKWARD);
		if (key == GLFW.GLFW_KEY_W)
			this._camera.toggleState(C_MOVE_FORWARD);
		if (key == GLFW.GLFW_KEY_A)
			this._camera.toggleState(C_MOVE_LEFT);
		if (key == GLFW.GLFW_KEY_D)
			this._camera.toggleState(C_MOVE_RIGHT);
		if (key == GLFW.GLFW_KEY_UP)
			this._camera.setRotationVel(new Point3(-1, 0, 0));
		if (key == GLFW.GLFW_KEY_DOWN)
			this._camera.setRotationVel(new Point3(1, 0, 0));
		if (key == GLFW.GLFW_KEY_LEFT)
			this._camera.setRotationVel(new Point3(0, -1, 0));
		if (key == GLFW.GLFW_KEY_RIGHT)
			this._camera.setRotationVel(new Point3(0, 1, 0));
	}
	
	
}
