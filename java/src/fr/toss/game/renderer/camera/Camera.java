package fr.toss.game.renderer.camera;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

import fr.toss.game.Game;
import fr.toss.game.maths.Point3;
import fr.toss.game.renderer.Renderer;
import fr.toss.game.world.World;
import fr.toss.game.world.chunk.Chunk;


public class Camera
{
	private CameraControl	_control;
	
	/** position, rotation for the camera */
	private Point3	_position;
	
	/** position, rotation velocity for the camera */
	private Point3	_position_vel;
	private Point3	_rotation_vel;
	
	private float	_fov;
	private float	_near;
	private float	_far;
	private float	_pitch;
	private float	_yaw;
	
	private int	_state;
	
	private Matrix4f	_viewmatrix;
	
	
	static float	rot_speed = 0.07f;
	static float	speed = 1.5f;
	
	public void start()
	{
		this._control = new CameraControl(this);
		this._position = new Point3(0, 0, 0);
		this._position_vel = new Point3(0, 0, 0);
		this._rotation_vel = new Point3(0, 0, 0);
		this._fov = 70;
		this._near = 0.1f;
		this._far = 1000;
		this._pitch = 0;
		this._yaw = (float) Math.PI;
		this._viewmatrix = new Matrix4f();
		this._state = 0;
	}
	
	public void stop()
	{
		// TODO Auto-generated method stub
	}
	
	public void	update(World world, Renderer renderer)
	{
		this._control.onUpdate();
		this.moveCamera();
		this.calculateViewMatrix();
		this.updateChunkLoads(world, renderer.getCamera());
	}
	
	private void updateChunkLoads(World world, Camera camera)
	{
		/*
		int	pos[];
		int indx;
		int indz;

		for (Chunk chunk : world.getChunks())
		{
			chunk.setLoaded(false);
		}
		pos = world.getChunkIndexForPos(camera.getPosition());
		for (int x = -3 ; x < 3 ; x++)
		{
			for (int z = -3 ; z < 3 ; z++)
			{
				indx = pos[0] + x;
				indz = pos[1] + z;
				world.createChunk(indx, indz);
				world.loadChunk(indx, indz);
			}
		}
		*/
	}

	private void	moveCamera()
	{
		this._pitch += this._rotation_vel.x * rot_speed;
		this._yaw += this._rotation_vel.y * rot_speed;
		this._position.x += this._position_vel.x * speed;
		this._position.y += this._position_vel.y * speed;
		this._position.z += this._position_vel.z * speed;
	}

	private void	calculateViewMatrix()
	{
		this._viewmatrix.setIdentity();
		this._viewmatrix.rotate(this.getPitch(), new Vector3f(1, 0, 0));
		this._viewmatrix.rotate(this.getYaw(), new Vector3f(0, 1, 0));
		this._viewmatrix.translate(this.getPosition().reverseSign());
	}

	public Point3 getPosition()
	{
		return (this._position);
	}

	public void setPosition(Point3 pos)
	{
		this._position = pos;
	}
	
	public Point3 getPositionVel()
	{
		return (this._position_vel);
	}

	public void setPositionVel(Point3 pos_vel)
	{
		this._position_vel = pos_vel;
	}

	public Point3 getRotationVel()
	{
		return (this._rotation_vel);
	}

	public void setRotationVel(Point3 rot)
	{
		this._rotation_vel = rot;
	}
	
	public void	 increasePosition(Point3 vec)
	{
		this._position.add(vec);
	}
	
	public float getFar()
	{
		return (this._far);
	}

	public float getNear()
	{
		return (this._near);
	}

	public float getFov()
	{
		return (this._fov);
	}
	
	public float getPitch()
	{
		return (this._pitch);
	}
	
	public float getYaw()
	{
		return (this._yaw);
	}

	public static Matrix4f	loadProjectionMatrix(Camera camera, float aspect)
	{
		Matrix4f	matrix;
		float		x_scale;
		float		y_scale;
		float		frustrum;

		matrix = new Matrix4f();
		y_scale = (float)((1.0f / Math.tan(Math.toRadians(camera.getFov() / 2.0f))) * aspect);
		x_scale = y_scale / aspect;
		frustrum = camera.getFar() - camera.getNear();
		matrix.m00 = x_scale;
		matrix.m11 = y_scale;
		matrix.m22 = -((camera.getFar() + camera.getNear()) / frustrum);
		matrix.m23 = -1;
		matrix.m32 = -((2 * camera.getNear() * camera.getFar()) / frustrum);
		matrix.m33 = 0;
		return (matrix);
	}

	public static Point3	vectorPitchYaw(float pitch, float yaw)
	{
		double	pitchcos;

		pitchcos = Math.cos(pitch);
		return (new Point3(pitchcos * Math.sin(-yaw), Math.sin(pitch), pitchcos * Math.cos(yaw)));
	}

	public Matrix4f getViewMatrix()
	{
		return (this._viewmatrix);
	}

	public void toggleState(int state)
	{
		this._state = this._state | state;
	}
	
	public void disableState(int state)
	{
		this._state = this._state & ~(state);
	}

	public boolean hasState(int state)
	{
		return ((this._state & state) != 0);
	}

	public CameraControl	getControl()
	{
		return (this._control);
	}
	
	
	public Matrix4f	getProjectionMatrix()
	{
		Matrix4f	matrix;
		float		x_scale;
		float		y_scale;
		float		frustrum;
		float		aspect;

		matrix = new Matrix4f();
		aspect = Game.instance.getRenderer().getWindow().getAspect();
		y_scale = (float)((1.0f / Math.tan(Math.toRadians(this.getFov() / 2.0f))) * aspect);
		x_scale = y_scale / aspect;
		frustrum = this.getFar() - this.getNear();
		matrix.m00 = x_scale;
		matrix.m11 = y_scale;
		matrix.m22 = -((this.getFar() + this.getNear()) / frustrum);
		matrix.m23 = -1;
		matrix.m32 = -((2 * this.getNear() * this.getFar()) / frustrum);
		matrix.m33 = 0;
		return (matrix);
	}
}
