package fr.toss.game.renderer.sky;

import fr.toss.game.renderer.ARenderer;
import fr.toss.game.renderer.Renderer;
import fr.toss.game.renderer.model.Model;
import fr.toss.game.world.World;

public class SkyRenderer extends ARenderer
{
	private SkyProgram	_program;
	private Model		_cubemapModel;
	private int			_cubemapDay;
	private int			_cubemapNight;
	
	public SkyRenderer(Renderer renderer)
	{
		super(renderer);
	}
	
	@Override
	public void start()
	{
		this._program = new SkyProgram();
		this._program.loadFromFiles("skybox.vertex", "skybox.fragment");
	}
	
	public void	stop()
	{
		//TODO
	}
	

	@Override
	public void prepareRendering(World world, Renderer renderer)
	{
		this._program.use();
		this._program.loadUniformWorld(world, renderer);
		// TODO Auto-generated method stub
	}
	
	public void render(World world, Renderer renderer)
	{
		//TODO
	}
	
	@Override
	public void finishRendering(World world, Renderer renderer)
	{
		this._program.stopUse();
	}
	
	public Model	getCubemapModel()
	{
		return (this._cubemapModel);
	}

	public int	getCubemapDay()
	{
		return (this._cubemapDay);
	}
	
	public int	getCubemapNight()
	{
		return (this._cubemapNight);
	}
}
