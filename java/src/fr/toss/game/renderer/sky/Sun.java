package fr.toss.game.renderer.sky;

import fr.toss.game.maths.Point3;
import fr.toss.game.renderer.Light;

public class Sun extends Light
{
	public Sun(Point3 pos)
	{
		super(new Point3(1, 1, 1), pos, new Point3(1, 0, 0));
	}

}
