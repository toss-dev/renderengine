package fr.toss.game.renderer.sky;

import org.lwjgl.opengl.GL20;

import fr.toss.game.renderer.program.WorldProgram;

public class SkyProgram extends WorldProgram
{
	@Override
	protected void bindAttributesList()
	{
		GL20.glBindAttribLocation(this.getID(), ATTR.POS.ordinal(), "position");
	}

	@Override
	protected void getAllUniforms()
	{
		super.getAllUniforms();
	}
}
