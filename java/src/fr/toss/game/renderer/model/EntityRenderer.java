package fr.toss.game.renderer.model;

import java.util.ArrayList;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Matrix4f;

import fr.toss.game.Game;
import fr.toss.game.entity.AEntity;
import fr.toss.game.logger.LoggerID;
import fr.toss.game.maths.MatrixUtils;
import fr.toss.game.renderer.ARenderer;
import fr.toss.game.renderer.Renderer;
import fr.toss.game.renderer.model.Model.VBO_IDS;
import fr.toss.game.world.World;

public class EntityRenderer extends ARenderer
{	
	public EntityRenderer(Renderer renderer)
	{
		super(renderer);
	}

	private ModelProgram _program;

	@Override
	public void	start()
	{
		this._program = new ModelProgram();
		this._program.loadFromFiles("model.vertex", "model.fragment");
	}
	
	@Override
	public void	stop()
	{
		Game.log("Stopping entities renderer", LoggerID.FINE);
	}
	

	@Override
	public void prepareRendering(World world, Renderer renderer)
	{
		this._program.use();
		this._program.loadUniformWorld(world, renderer);
		GL13.glActiveTexture(GL13.GL_TEXTURE0);
	}
	
	public void render(World world, Renderer renderer)
	{
		for (Model model : renderer.getBatchs().keySet())
		{
			this.startModelRendering(model);
			ArrayList<AEntity> entities = renderer.getBatchs().get(model);
			for (AEntity entity : entities)
			{
				this.prepareInstance(entity);
				this.renderEntity(entity);
			}
			this.stopModelRendering();
		}
	}
	
	@Override
	public void finishRendering(World world, Renderer renderer)
	{
		this._program.stopUse();
	}
	
	
	private void	prepareInstance(AEntity entity)
	{
		Matrix4f		matrix;
		
		matrix = MatrixUtils.loadTransformationMatrix(entity.getPosition(), entity.getRotation(), entity.getScale());
		this._program.loadUniformMatrix(this._program.getTransformationMatrixID(), matrix);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, entity.getTextureID());
	}
	
	private void renderEntity(AEntity entity)
	{
		GL11.glDrawElements(GL11.GL_TRIANGLES, entity.getModel().getVertexCount(), GL11.GL_UNSIGNED_INT, 0);
	}

	private void	startModelRendering(Model model)
	{
		GL30.glBindVertexArray(model.getVaoID());
		GL20.glEnableVertexAttribArray(VBO_IDS.ATTR_POS.ordinal());
		GL20.glEnableVertexAttribArray(VBO_IDS.ATTR_UVS.ordinal());
		GL20.glEnableVertexAttribArray(VBO_IDS.ATTR_NORMALS.ordinal());
	}

	private void	stopModelRendering()
	{
		GL20.glDisableVertexAttribArray(VBO_IDS.ATTR_POS.ordinal());
		GL20.glDisableVertexAttribArray(VBO_IDS.ATTR_UVS.ordinal());
		GL20.glDisableVertexAttribArray(VBO_IDS.ATTR_NORMALS.ordinal());
		GL30.glBindVertexArray(0);
	}
}
