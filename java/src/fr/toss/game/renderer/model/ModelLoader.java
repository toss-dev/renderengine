package fr.toss.game.renderer.model;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import fr.toss.game.Game;
import fr.toss.game.logger.LoggerID;

public class ModelLoader
{
	public static Model MODEL_CUBE = null;
	public static Model MODEL_DRAGON = null;

	public void	start()
	{
		Game.log("Loading models...", LoggerID.FINE);
		
		MODEL_CUBE = this.loadModelFromObjFile("cube.obj");
		MODEL_DRAGON = this.loadModelFromObjFile("dragon_fat.obj");

		Game.log("Models loaded", LoggerID.FINE);
	}
	
	public Model loadModelFromObjFile(String objFile)
	{
		ModelData	modeldata;
		String		path;
		
		path = "../assets/obj/" + objFile;
		Game.log("Loading model: " + path, LoggerID.FINE);
		try
		{
			modeldata = this.loadModelDataFromObjFile(path);
			return (new Model(modeldata, objFile));
		}
		catch (IOException e)
		{
			e.printStackTrace();
			Game.log("Error occured while loading model: " + path, LoggerID.ERROR);
		}
		return (null);
	}

	private ModelData loadModelDataFromObjFile(String objFile) throws IOException
	{
		BufferedReader	reader;
		ModelData		modelData;
		
		reader = new BufferedReader(new FileReader(objFile));
		modelData = this.parseObjFile(reader);
		reader.close();
		return (modelData);
	}

	private ModelData parseObjFile(BufferedReader reader) throws IOException
	{
		ArrayList<Vector3f>	vertices;
		ArrayList<Vector2f>	uvs;
		ArrayList<Vector3f>	normals;
		ArrayList<Indice>	indices;
		String				line;
		
		vertices = new ArrayList<Vector3f>();
		uvs = new ArrayList<Vector2f>();
		normals = new ArrayList<Vector3f>();
		indices = new ArrayList<Indice>();
		Game.log("Starting obj file parsing", LoggerID.DEBUG);
		while ((line = reader.readLine()) != null)
		{
			this.parseObjFileLine(vertices, uvs, normals, indices, line.trim());
		}
		Game.log("Obj file was registered to parsing lists", LoggerID.DEBUG);
		return (convertListToModelData(vertices, uvs, normals, indices));
	}

	private ModelData convertListToModelData(ArrayList<Vector3f> vertices, ArrayList<Vector2f> uvs, ArrayList<Vector3f> normals, ArrayList<Indice> indices)
	{
		ModelData	modelData;
		Vector3f	vec3;
		Vector2f	vec2;
		int			location;
		
		Game.log("Converting model data list to model data buffers", LoggerID.DEBUG);

		modelData = new ModelData(vertices.size(), indices.size() / 3);
		location = 0;
		for (Vector3f vertex : vertices)
		{
			modelData.setVertexFloat(location + 0, vertex.getX());
			modelData.setVertexFloat(location + 1, vertex.getY());
			modelData.setVertexFloat(location + 2, vertex.getZ());
			location += 3;
		}
		
		Game.log("Vertex copied", LoggerID.DEBUG);

		location = 0;
		for (Indice indice : indices)
		{
			modelData.setIndice(location, indice.getV());
			
			try
			{
				vec2 = uvs.get(indice.getVT());
			}
			catch (Exception e)
			{
				vec2 = new Vector2f(0, 0);
			}
			modelData.setUvFloat(indice.getV() * 2 + 0, vec2.getX());
			modelData.setUvFloat(indice.getV() * 2 + 1, vec2.getY());
			
			try
			{
				vec3 = normals.get(indice.getVN());
			}
			catch (Exception e)
			{
				vec3 = new Vector3f(0, 1, 0);
			}
			modelData.setNormalFloat(indice.getV() * 3 + 0, vec3.getX());
			modelData.setNormalFloat(indice.getV() * 3 + 1, vec3.getY());
			modelData.setNormalFloat(indice.getV() * 3 + 2, vec3.getZ());
			
			location++;
		}
		
		Game.log("Lists converted to model data succesfully", LoggerID.DEBUG);
		
		return (modelData);
	}

	private void parseObjFileLine(ArrayList<Vector3f> vertices, ArrayList<Vector2f> uvs, ArrayList<Vector3f> normals, ArrayList<Indice> indices, String line)
	{
		if (line.startsWith("v "))
		{
			this.addVertex(vertices, line.substring(2).trim());
		}
		else if (line.startsWith("f "))
		{
			this.addIndices(indices, line.substring(2).trim());
		}
		else if (line.startsWith("vn "))
		{
			this.addNormal(normals, line.substring(3).trim());
		}
		else if (line.startsWith("vt "))
		{
			this.addUvs(uvs, line.substring(3).trim());
		}
	}

	private void addVertex(ArrayList<Vector3f> vertices, String line)
	{
		Vector3f	v;
		String		data[];
		float		x;
		float		y;
		float		z;

		data = line.split("\\s+");
		x = this.parseFloat(data[0]);
		y = this.parseFloat(data[1]);
		z = this.parseFloat(data[2]);
		v = new Vector3f(x, y, z);
		vertices.add(v);
	}

	private void addIndices(ArrayList<Indice> indices, String line)
	{
		String	inds[];
		
		inds = line.split("\\s+");
		indices.add(new Indice(inds[0].split("/")));
		indices.add(new Indice(inds[1].split("/")));
		indices.add(new Indice(inds[2].split("/")));
	}

	private void addNormal(ArrayList<Vector3f> normals, String line)
	{
		String	data[];
		float	x;
		float	y;
		float	z;
				
		data = line.split("\\s+");
		x = this.parseFloat(data[0]);
		y = this.parseFloat(data[1]);
		z = this.parseFloat(data[2]);
		normals.add(new Vector3f(x, y, z));
	}

	private void addUvs(ArrayList<Vector2f> uvs, String line)
	{
		String	data[];
		float	x;
		float	y;
				
		data = line.split("\\s+");
		x = this.parseFloat(data[0]);
		y = this.parseFloat(data[1]);
		uvs.add(new Vector2f(x, y));
	}
	
	private float parseFloat(String str)
	{
		float	f;
		
		try
		{
			f = Float.parseFloat(str);
		}
		catch (Exception e)
		{
			f = 0;
		}
		return (f);
	}
}

class	Indice
{
	private int	_v;
	private int	_vt;
	private int	_vn;
	
	public Indice(int v, int vt, int vn)
	{
		this.setValues(v, vt, vn);
	}
	
	public Indice(String[] split)
	{
		if (split.length == 0)
		{
			this.setValues(0, 0, 0);
		}
		else if (split.length == 1)
		{
			this.setValues(Integer.valueOf(split[0]), 0, 0);
		}
		else if (split.length == 2)
		{
			this.setValues(Integer.valueOf(split[0]), Integer.valueOf(split[1]), 0);
		}
		else if (split.length == 3)
		{
			this.setValues(Integer.valueOf(split[0]), Integer.valueOf(split[1]), Integer.valueOf(split[2]));
		}
	}

	public void	setValues(int v, int vt, int vn)
	{
		this._v = v - 1;
		this._vt = vt - 1;
		this._vn = vn - 1;
	}
	
	public int	getV()
	{
		return (this._v);
	}
	
	public int	getVT()
	{
		return (this._vt);
	}
	
	public int	getVN()
	{
		return (this._vn);
	}
}

