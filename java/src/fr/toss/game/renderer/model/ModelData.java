package fr.toss.game.renderer.model;

import fr.toss.game.Game;
import fr.toss.game.logger.LoggerID;

public class ModelData
{
	private float	_vertices[];
	private int		_indices[];
	private float	_uvs[];
	private float	_normals[];
	
	public ModelData(int vertexCount, int triangleCount)
	{
		Game.log("new Model Data created: vertexCount(" + vertexCount + ") triangleCount(" + triangleCount + ")", LoggerID.DEBUG);
		this._vertices	= new float[vertexCount * 3];
		this._normals 	= new float[vertexCount * 3];
		this._uvs		= new float[vertexCount * 2];
		this._indices	= new int[triangleCount * 3];
	}
	
	public ModelData(float vertices[], int indices[])
	{
		this._vertices = vertices;
		this._indices = indices;
		this._uvs = vertices.clone();
		this._normals = vertices.clone();
	}
	
	public void	setVertices(float vertices[])
	{
		this._vertices = vertices;
	}
	
	public void	setIndices(int indices[])
	{
		this._indices = indices;
	}
	
	public void setVertexFloat(int location, float f)
	{
		if (location >= this._vertices.length)
		{
			//Game.log("OBJ FILE PARSER: out of vertices array : " + location + "/" + this._vertices.length, LoggerID.DEBUG);
			return ;
		}
		this._vertices[location] = f;
	}
	
	public void	setIndice(int location, int value)
	{
		if (location >= this._indices.length)
		{
			//Game.log("OBJ FILE PARSER: out of indices array : " + location + "/" + this._indices.length, LoggerID.DEBUG);
			return ;
		}
		this._indices[location] = value;
	}
	
	public void	setUvFloat(int location, float f)
	{
		if (location >= this._uvs.length)
		{
			//Game.log("OBJ FILE PARSER: out of uvs array : " + location + "/" + this._uvs.length, LoggerID.DEBUG);
			return ;
		}
		this._uvs[location] = f;
	}
	
	public void setNormalFloat(int location, float f)
	{
		if (location >= this._normals.length)
		{
			//Game.log("OBJ FILE PARSER: out of normals array : " + location + "/" + this._normals.length, LoggerID.DEBUG);
			return ;
		}
		this._normals[location] = f;
	}
	
	public float[]	getVertices()
	{
		return (this._vertices);
	}
	
	public int[]	getIndices()
	{
		return (this._indices);
	}
	
	public float[]	getUvs()
	{
		return (this._uvs);
	}
	
	public float[]	getNormals()
	{
		return (this._normals);
	}

	public void show()
	{
		for (int i = 0 ; i < this._vertices.length ; i += 3)
		{
			System.out.println("v " + this._vertices[i + 0] + " "  + this._vertices[i + 1] + " "  + this._vertices[i + 2]);
		}
	}
}
