package fr.toss.game.renderer.model;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

import fr.toss.game.Game;
import fr.toss.game.logger.LoggerID;
import fr.toss.game.world.chunk.ChunkGenerator;

public class Model
{
	private int		_vaoID;
	private int		_vbos[];
	private int		_vertexCount;
	private String	_name;
	
	public Model(ModelData modeldata, String name)
	{
		Game.log("Loading new model from Model data (" + name + ") : " + modeldata.getIndices().length + " vertices", LoggerID.FINE);
		this._name = name;
		this._vbos = new int[VBO_IDS.ATTR_MAX.ordinal()];
		this._vertexCount = modeldata.getIndices().length;
		this._vaoID = GL30.glGenVertexArrays();
		GL30.glBindVertexArray(this._vaoID);
		this.bindIndices(modeldata.getIndices());
		this.storeFloats(VBO_IDS.ATTR_POS.ordinal(), modeldata.getVertices(), 3);
		this.storeFloats(VBO_IDS.ATTR_UVS.ordinal(), modeldata.getUvs(), 2);
		this.storeFloats(VBO_IDS.ATTR_NORMALS.ordinal(), modeldata.getNormals(), 3);
		GL30.glBindVertexArray(0);
	}

	private void		bindIndices(int indices[])
	{
		IntBuffer	buffer;
		
		this._vbos[VBO_IDS.ATTR_INDICES.ordinal()] = GL15.glGenBuffers();
		GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, this._vbos[VBO_IDS.ATTR_INDICES.ordinal()]);
		buffer = storeDataInIntBuffer(indices);
		GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, buffer, GL15.GL_STATIC_DRAW);
	}
	
	private void	storeFloats(int attr, float floats[], int vertexSize)
	{
		FloatBuffer	buffer;
		
		buffer = storeDataInFloatBuffer(floats);
		this._vbos[attr] = GL15.glGenBuffers();
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, this._vbos[attr]);
		GL15.glBufferData(GL15.GL_ARRAY_BUFFER, buffer, GL15.GL_STATIC_DRAW);
		GL20.glVertexAttribPointer(attr, vertexSize, GL11.GL_FLOAT, false, 0, 0);
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
	}
	
	private IntBuffer	storeDataInIntBuffer(int data[])
	{
		IntBuffer	buffer;
		
		buffer = BufferUtils.createIntBuffer(data.length);
		buffer.put(data);
		buffer.flip();
		return (buffer);
	}
	
	
	private FloatBuffer	storeDataInFloatBuffer(float data[])
	{
		FloatBuffer	buffer;
		
		buffer = BufferUtils.createFloatBuffer(data.length);
		buffer.put(data);
		buffer.flip();
		return (buffer);
	}
	
	public int	getVaoID()
	{
		return (this._vaoID);
	}
	
	public int	getVBO(int id)
	{
		return (this._vbos[id]);
	}
	
	public int	getVertexCount()
	{
		return (this._vertexCount);
	}

	public void stop()
	{
		for (int i = 0 ; i < VBO_IDS.ATTR_MAX.ordinal() ; i++)
		{
			GL15.glDeleteBuffers(this._vbos[i]);
		}
		GL30.glDeleteVertexArrays(this.getVaoID());
	}
	
	@Override
	public String	toString()
	{
		return ("Model: " + this._name + "; vaoID: " + this._vaoID + "; vertexCount: " + this._vertexCount);
	}
	
	public enum VBO_IDS
	{
		ATTR_POS,
		ATTR_UVS,
		ATTR_NORMALS,
		ATTR_INDICES,
		ATTR_MAX
	}
}
