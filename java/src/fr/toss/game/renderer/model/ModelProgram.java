package fr.toss.game.renderer.model;

import org.lwjgl.opengl.GL20;

import fr.toss.game.renderer.program.WorldProgram;

public class ModelProgram extends WorldProgram
{
	private int _transformation_matrix;

	@Override
	protected void bindAttributesList()
	{
		GL20.glBindAttribLocation(this.getID(), ATTR.POS.ordinal(), "position");
		GL20.glBindAttribLocation(this.getID(), ATTR.UVS.ordinal(), "uv");
		GL20.glBindAttribLocation(this.getID(), ATTR.NORMALS.ordinal(), "normal");
	}

	@Override
	protected void getAllUniforms()
	{
		super.getAllUniforms();
		this._transformation_matrix = GL20.glGetUniformLocation(this.getID(), "transf_matrix");
	}

	public int getTransformationMatrixID()
	{
		return (this._transformation_matrix);
	}
}