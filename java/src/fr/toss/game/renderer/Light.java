package fr.toss.game.renderer;

import fr.toss.game.maths.Point3;

public class Light
{
	private Point3	_color;
	private Point3	_pos;
	private Point3	_attenuation;
	
	public Light(Point3 color, Point3 pos, Point3 attenuation)
	{
		this._color = color;
		this._pos = pos;
		this._attenuation = attenuation;
	}

	/** @return the _color */
	public Point3 getColor()
	{
		return (this._color);
	}

	/** @param _color the _color to set */
	public void setColor(Point3 _color)
	{
		this._color = _color;
	}

	/** @return the _pos */
	public Point3 getPos()
	{
		return (this._pos);
	}

	/** @param _pos the _pos to set */
	public void setPos(Point3 _pos)
	{
		this._pos = _pos;
	}
	
	/** @return the _pos */
	public Point3 getAttenuation()
	{
		return (this._attenuation);
	}

	/** @param _pos the _pos to set */
	public void setAttenuation(Point3 attenuation)
	{
		this._attenuation = attenuation;
	}

}
