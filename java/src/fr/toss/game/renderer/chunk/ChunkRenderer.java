package fr.toss.game.renderer.chunk;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Matrix4f;

import fr.toss.game.Game;
import fr.toss.game.entity.AEntity;
import fr.toss.game.logger.LoggerID;
import fr.toss.game.maths.MatrixUtils;
import fr.toss.game.maths.Point3;
import fr.toss.game.renderer.ARenderer;
import fr.toss.game.renderer.Renderer;
import fr.toss.game.renderer.Textures;
import fr.toss.game.renderer.model.Model;
import fr.toss.game.renderer.model.Model.VBO_IDS;
import fr.toss.game.world.World;
import fr.toss.game.world.chunk.Chunk;

public class ChunkRenderer extends ARenderer
{
	private ChunkProgram	_program;

	public ChunkRenderer(Renderer renderer)
	{
		super(renderer);
	}

	@Override
	public void start()
	{
		this._program = new ChunkProgram();
		this._program.loadFromFiles("model.vertex", "model.fragment");
	}

	@Override
	public void stop()
	{
		Game.log("Stopping chunk renderer", LoggerID.FINE);
	}

	@Override
	public void prepareRendering(World world, Renderer renderer)
	{
		this._program.use();
		this._program.loadUniformWorld(world, renderer);
		GL13.glActiveTexture(GL13.GL_TEXTURE0);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, Textures.CHUNK);
	}

	@Override
	public void	render(World world, Renderer renderer)
	{
		for (Chunk chunk : world.getChunks())
		{
			if (chunk.isLoaded())
			{
				this.startModelRendering(chunk);
				this.prepareChunk(chunk);
				this.renderChunk(chunk);
				this.stopModelRendering();
			}
		}
	}
	
	private void	prepareChunk(Chunk chunk)
	{
		Matrix4f	matrix;
		Point3		pos;
		
		pos = new Point3(chunk.getPosX(), 0, chunk.getPosZ());
		matrix = MatrixUtils.loadTransformationMatrix(pos, new Point3(0, 0, 0), new Point3(1, 1, 1));
		this._program.loadUniformMatrix(this._program.getTransformationMatrixID(), matrix);
	}
	
	private void renderChunk(Chunk chunk)
	{
		GL11.glDrawElements(GL11.GL_TRIANGLES, chunk.getModel().getVertexCount(), GL11.GL_UNSIGNED_INT, 0);
	}

	private void	startModelRendering(Chunk chunk)
	{
		GL30.glBindVertexArray(chunk.getModel().getVaoID());
		GL20.glEnableVertexAttribArray(VBO_IDS.ATTR_POS.ordinal());
		GL20.glEnableVertexAttribArray(VBO_IDS.ATTR_UVS.ordinal());
		GL20.glEnableVertexAttribArray(VBO_IDS.ATTR_NORMALS.ordinal());
	}

	private void	stopModelRendering()
	{
		GL20.glDisableVertexAttribArray(VBO_IDS.ATTR_POS.ordinal());
		GL20.glDisableVertexAttribArray(VBO_IDS.ATTR_UVS.ordinal());
		GL20.glDisableVertexAttribArray(VBO_IDS.ATTR_NORMALS.ordinal());
		GL30.glBindVertexArray(0);
	}
	
	@Override
	public void finishRendering(World world, Renderer renderer)
	{
		this._program.stopUse();
	}
}
