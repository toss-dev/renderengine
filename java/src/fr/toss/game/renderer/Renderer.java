package fr.toss.game.renderer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.lwjgl.glfw.GLFW;
import org.lwjgl.opengl.GL11;

import fr.toss.game.Game;
import fr.toss.game.entity.AEntity;
import fr.toss.game.event.Window;
import fr.toss.game.logger.LoggerID;
import fr.toss.game.maths.Color4f;
import fr.toss.game.maths.Point3;
import fr.toss.game.renderer.camera.Camera;
import fr.toss.game.renderer.chunk.ChunkRenderer;
import fr.toss.game.renderer.model.EntityRenderer;
import fr.toss.game.renderer.model.Model;
import fr.toss.game.renderer.model.ModelLoader;
import fr.toss.game.renderer.sky.SkyRenderer;
import fr.toss.game.renderer.sky.Sun;
import fr.toss.game.world.World;

public class Renderer
{
	/** renderers list */
	private List<ARenderer>	_renderers;
	
	/** hashmap containing every entities linked to it model */
	private HashMap<Model, ArrayList<AEntity>>	_batchs;

	/** camera */
	private Camera _camera;
	
	/** sun */
	private ArrayList<Light> _lights;
	private Sun	_sun;
	
	/** Model loader for obj file and chunks model */
	private ModelLoader	_modelLoader;
	
	private Color4f	_fogColor;
	private float	_fogDensity;
	private float	_fogGradient;

	private Window	_window;
	
	public Renderer()
	{
		this._modelLoader = new ModelLoader();
		this._window = new Window();
		this._batchs = new HashMap<Model, ArrayList<AEntity>>();
		this._renderers = new ArrayList<ARenderer>();
		this._renderers.add(new ChunkRenderer(this));
		this._renderers.add(new EntityRenderer(this));
		this._renderers.add(new SkyRenderer(this));
		this._camera = new Camera();
		this._sun = new Sun(new Point3(20000, 20000, 0));
		this._lights = new ArrayList<Light>();
		this._lights.add(this._sun);
		this._fogColor = new Color4f(0.5f, 0.5f, 0.5f, 1);
		this._fogDensity = 0.005f;
		this._fogGradient = 1;
	}
	
	/** call on initialization 
	 * @throws Exception */
	public void start() throws Exception
	{
		Game.log("Starting renderer", LoggerID.FINE);
		this._window.start();
		for (ARenderer renderer : this._renderers)
		{
			renderer.start();
		}
		GL11.glEnable(GL11.GL_DEPTH_TEST);
		this._camera.start();
		this._modelLoader.start();
		Textures.start();
	}
	
	
	/** clean up on program ended */
	public void stop()
	{
		Game.log("Stopping renderer...", LoggerID.FINE);
		this._window.stop();
		for (ARenderer renderer : this._renderers)
		{
			renderer.stop();
		}
		for (Model model : this._batchs.keySet())
		{
			model.stop();
		}
		this._camera.stop();
		Textures.stop();
	}
	
	
	public void render(World world, Window window)
	{
		this.prepareRendering(world);
		{
			for (ARenderer renderer : this._renderers)
			{
				renderer.prepareRendering(world, this);
				renderer.render(world, this);
				renderer.finishRendering(world, this);
			}
		}
		this.endRendering(window);
	}

	private void prepareRendering(World world)
	{		
		GL11.glClearColor(this._fogColor.getRed(), this._fogColor.getGreen(), this._fogColor.getBlue(), this._fogColor.getAlpha());
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
		
		for (AEntity entity : world.getEntities())
		{
			this.prepareEntity(entity);
		}
	}
	
	private void prepareEntity(AEntity entity)
	{
		ArrayList<AEntity>	batch;
		
		batch = this._batchs.get(entity.getModel());
		if (batch != null)
		{
			batch.add(entity);
		}
		else
		{
			batch = new ArrayList<AEntity>();
			batch.add(entity);
			this._batchs.put(entity.getModel(), batch);
		}
	}

	private void endRendering(Window window)
	{
		GLFW.glfwSwapBuffers(window.getPtr());
		this._batchs.clear();
	}

	public void update(World world)
	{
		this._camera.update(world, this);
	}
	
	public Color4f	getFogColor()
	{
		return (this._fogColor);
	}

	/** @return the _fogDensity */
	public float getFogDensity()
	{
		return (this._fogDensity);
	}

	/** @param _fogDensity the _fogDensity to set */
	public void setFogDensity(float fogDensity)
	{
		this._fogDensity = fogDensity;
	}

	/** @return the _fogGradient */
	public float getFogGradient()
	{
		return (this._fogGradient);
	}

	/** @param _fogGradient the _fogGradient to set */
	public void setFogGradient(float fogGradient)
	{
		this._fogGradient = fogGradient;
	}


	public void spawnEntity(AEntity e, Model model)
	{
		ArrayList<AEntity>	entities;
		
		entities = this._batchs.get(model);
		if (entities == null)
		{
			Game.log("Tried to spawned an unknown model for entity: " + e.getUniqueID(), LoggerID.WARNING);
			return ;
		}
		entities.add(e);
	}

	public HashMap<Model, ArrayList<AEntity>> getBatchs()
	{
		return (this._batchs);
	}
	
	public ModelLoader	getModelLoader()
	{
		return (this._modelLoader);
	}

	public Camera	getCamera()
	{
		return (this._camera);
	}

	public Light getSun()
	{
		return (this._sun);
	}

	public ArrayList<Light>	getLights()
	{
		return (this._lights);
	}

	public Window	getWindow()
	{
		return (this._window);
	}
}
