package fr.toss.game.renderer;

import fr.toss.game.world.World;

public abstract class ARenderer
{
	private Renderer	_renderer;
	
	public ARenderer(Renderer renderer)
	{
		this._renderer = renderer;
	}
	
	public abstract void	start();
	
	public abstract void	stop();
	
	public abstract void	render(World world, Renderer renderer);
	
	public abstract void 	prepareRendering(World world, Renderer renderer);
	
	public abstract void 	finishRendering(World world, Renderer renderer);
	
	public Renderer getRenderer()
	{
		return (this._renderer);
	}
}
