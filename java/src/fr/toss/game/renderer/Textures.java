package fr.toss.game.renderer;

import java.io.FileInputStream;
import java.nio.ByteBuffer;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL14;
import org.lwjgl.opengl.GL30;

import de.matthiasmann.twl.utils.PNGDecoder;
import de.matthiasmann.twl.utils.PNGDecoder.Format;
import fr.toss.game.Game;
import fr.toss.game.logger.LoggerID;

public class Textures
{
	public static int CHUNK;
	
	/** load all textures, should be call at the beginning of the prgoram and once only */
	public static void	start()
	{
		CHUNK = loadTexture("chunk.png");
	}
	
	public static void	stop()
	{
		GL11.glDeleteTextures(CHUNK);
	}

	private static int loadTexture(String string)
	{
		Texture	texture;
		String	path;
		int		textureID;

		path = "../assets/textures/" + string;
		Game.log("Loading texture: " + path, LoggerID.FINE);
		texture = new Texture();
		texture.loadFromFilename(path);
		textureID = GL11.glGenTextures();
		GL13.glActiveTexture(GL13.GL_TEXTURE0);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, textureID);
		GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGBA, texture.getWidth(), texture.getHeight(), 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, texture.getPixels());
		GL30.glGenerateMipmap(GL11.GL_TEXTURE_2D);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR_MIPMAP_LINEAR);
		GL11.glTexParameterf(GL11.GL_TEXTURE_2D, GL14.GL_TEXTURE_LOD_BIAS, -1.8f);
		return (textureID);
	}
}

class	Texture
{
	private int			_width;
	private int			_height;
	private ByteBuffer	_pixels;
	
	public Texture()
	{
		this._width = 0;
		this._height = 0;
		this._pixels = null;
	}
	
	public void	loadFromFilename(String file)
	{
		FileInputStream	in;
		PNGDecoder		decoder;
		
		try
		{
			in = new FileInputStream(file);
			decoder = new PNGDecoder(in);
			this._width = decoder.getWidth();
			this._height = decoder.getHeight();
			this._pixels = ByteBuffer.allocateDirect(4 * this._width * this._height);
			decoder.decode(this._pixels, this._width * 4, Format.RGBA);
			this._pixels.flip();
			in.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Game.log("Cant load file: " + file, LoggerID.ERROR);
		}
	}
	
	public int	getWidth()
	{
		return (this._width);
	}
	
	public int	getHeight()
	{
		return (this._height);
	}
	
	public ByteBuffer	getPixels()
	{
		return (this._pixels);
	}
}