package fr.toss.game.world;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Random;

import fr.toss.game.Game;
import fr.toss.game.entity.AEntity;
import fr.toss.game.entity.EntityDragon;
import fr.toss.game.logger.LoggerID;
import fr.toss.game.maths.Point3;
import fr.toss.game.world.chunk.Chunk;
import fr.toss.game.world.chunk.ChunkGenerator;

public class World
{
	/** minutes for a cycle (one day OR one night)*/
	private static final int CYCLE_MIN = 2;
	
	/** ticks executed per seconds */
	private static final int TPS = 60;

	/** ticks executed for one cycle */
	private static final int TPC = TPS * 60 * CYCLE_MIN;

	/** total tick  and current tick (2 * TPC max) */
	private long	_ticks;
	private int		_tick;
	
	/** chunk hashmap */
	private HashMap<ByteArrayKey, Chunk>	_chunks;
	private ArrayList<AEntity>				_entities;

	private ChunkGenerator	_chunkGenerator;

	/** random number generator */
	private Random _rng;

	public World()
	{
		this._rng = new Random();
		this._ticks = 0;
		this._tick = 0;
		this._chunks = new HashMap<>();
		this._entities = new ArrayList<AEntity>();
		this._chunkGenerator = new ChunkGenerator(this._rng);
	}
	

	/** clean up on program ended */
	public void start()
	{
		Game.log("Starting world...", LoggerID.FINE);
		//this.spawnEntity(new EntityDragon(new Point3(0, 0, 0)));
		
		this.createChunk(0, 0);
		this.loadChunk(0, 0);
		
		
		this.createChunk(1, 0);
		this.loadChunk(1, 0);
		
		
		this.createChunk(0, 1);
		this.loadChunk(0, 1);
		
		
		this.createChunk(1, 1);
		this.loadChunk(1, 1);
	}

	/** clean up on program ended */
	public void stop()
	{
		Game.log("Stopping world...", LoggerID.FINE);
	}

	public void update()
	{		
		for (AEntity entity : this._entities)
		{
			Chunk chunk = this.getChunkForPos(entity.getPosition());
			if (chunk != null && chunk.isLoaded())
			{
				entity.update();
			}
		}
		
		for (Chunk chunk : this._chunks.values())
		{
			chunk.update();
		}
	}

	public int[]	getChunkIndexForPos(Point3 pos)
	{
		int	inds[];
		
		inds = new int[2];
		if (pos.x < 0)
		{
			pos.x -= ChunkGenerator.CHUNK_SIZE;
		}
		if (pos.z < 0)
		{
			pos.z -= ChunkGenerator.CHUNK_SIZE;
		}
		inds[0] = (int) (pos.x / ChunkGenerator.CHUNK_SIZE);
		inds[1] = (int) (pos.z / ChunkGenerator.CHUNK_SIZE);
		return (inds);
	}
	
	/** return chunk at real world position */
	public Chunk	getChunkForPos(Point3 pos)
	{
		int	inds[];
		
		inds = getChunkIndexForPos(pos);
		return (getChunk(inds[0], inds[1]));
	}

	/** return chunk at {x;z} index */
	public Chunk	getChunk(int x, int z)
	{
		return (this._chunks.get(new ByteArrayKey(x, z)));
	}

	public void createChunk(int x, int z)
	{
		Chunk	chunk;
		
		chunk = this.getChunk(x, z);
		if (chunk != null && chunk.getIndX() == x && chunk.getIndZ() == z)
		{
			Game.log("Trying to create an already existing chunk at: " + x + ":" + z , LoggerID.WARNING);
			return ;
		}
		chunk = this._chunkGenerator.generateChunk(this, x, z);
		Game.log("Adding chunk: " + chunk, LoggerID.FINE);
		this._chunks.put(new ByteArrayKey(x, z), chunk);
	}
	
	public void	loadChunk(int x, int z)
	{
		Chunk	chunk;
		
		chunk = this.getChunk(x, z);
		if (chunk != null)
		{
			chunk.setLoaded(true);
			Game.log("Chunk loaded: " + chunk, LoggerID.FINE);
		}
		else
		{
			Game.log("Trying to load a NULL chunk", LoggerID.WARNING);
		}
	}
	
	public boolean isChunkLoaded(int x, int z)
	{
		Chunk	c;
		
		c = this.getChunk(x, z);
		if (c == null)
			
		{
			return (false);
		}
		return (c.isLoaded());
	}
	
	public boolean isChunkCreated(int x, int z)
	{;
		return (this.getChunk(x, z) != null);
	}

	public void	spawnEntity(AEntity entity)
	{
		entity.setWorld(this);
		this._entities.add(entity);
	}
	
	public Collection<Chunk> getChunks()
	{
		return (this._chunks.values());
	}
	
	
	public long getTotalTicks()
	{
		return (this._ticks);
	}
	
	public int getTick()
	{
		return (this._tick);
	}


	public ArrayList<AEntity> getEntities()
	{
		return (this._entities);
	}


	public Random getRNG()
	{
		return (this._rng);
	}
}

final class ByteArrayKey
{
	private final byte[] _key;

    public ByteArrayKey(int x, int z)
    {
    	this._key = new byte[4 + 4];
    	this._key[0] = (byte)(x >>> 24);
    	this._key[1] = (byte)(x >>> 16);
    	this._key[2] = (byte)(x >>> 8);
    	this._key[3] = (byte)(x >>> 0);
		this._key[4] = (byte)(z >>> 24);
		this._key[5] = (byte)(z >>> 16);
		this._key[6] = (byte)(z >>> 8);
		this._key[7] = (byte)(z >>> 0);
    }

	@Override
    public boolean equals(Object other)
    {
        if (!(other instanceof ByteArrayKey))
        {
            return (false);
        }
        return (Arrays.equals(this._key, ((ByteArrayKey)other).getKeyBytes()));
    }

    private byte[] getKeyBytes()
    {
		return (this._key);
	}

	@Override
    public int hashCode()
    {
        return (Arrays.hashCode(this._key));
    }
}
