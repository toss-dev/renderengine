package fr.toss.game.world.chunk;

import fr.toss.game.Game;
import fr.toss.game.logger.LoggerID;
import fr.toss.game.renderer.model.Model;
import fr.toss.game.world.World;

public class Chunk
{
	private World		_world;
	private Model		_model;
	private int			_indx;
	private int 		_indz;
	private float		_posx;
	private float		_posz;
	private float[][]	_ys;
	private boolean		_loaded;
	
	public Chunk(World world, int x, int z)
	{
		this._world = world;
		this._model = null;
		this._indx = x;
		this._indx = z;
		this._posx = x * ChunkGenerator.CHUNK_SIZE;
		this._posz = z * ChunkGenerator.CHUNK_SIZE;
		this._ys = new float[ChunkGenerator.CHUNK_VERTICES][ChunkGenerator.CHUNK_VERTICES];
		this._loaded = false;
	}

	public void update()
	{
		//TODO
	}
	
	public World getWorld()
	{
		return (this._world);
	}
	
	public int	getIndX()
	{
		return (this._indx);
	}
	
	public int	getIndZ()
	{
		return (this._indz);
	}
	
	public float	getPosX()
	{
		return (this._posx);
	}
	
	public float	getPosZ()
	{
		return (this._posz);
	}

	public float[][] getYs()
	{
		return (this._ys);
	}
	
	public void	setY(int x, int z, float f)
	{
		this._ys[x][z] = f;
	}
	
	public Model	getModel()
	{
		return (this._model);
	}
	
	public boolean isLoaded()
	{
		return (this._loaded);
	}
	
	public void setLoaded(boolean b)
	{
		this._loaded = b;
	}

	public void setModel(Model model)
	{
		if (this._model != null)
		{
			Game.log("Trying to assing a model to an already modeled chunk", LoggerID.WARNING);
			return ;
		}
		this._model = model;
	}

	public float getY(int x, int z)
	{
		return (this._ys[x][z]);
	}
	
	public String	toString()
	{
		return ("Chunk: x:" + this.getIndX() + " z:" + this.getIndZ());
	}
}
