package fr.toss.game.world.chunk;

import java.util.Random;

import org.lwjgl.util.vector.Vector3f;

import fr.toss.game.Game;
import fr.toss.game.logger.LoggerID;
import fr.toss.game.renderer.model.Model;
import fr.toss.game.renderer.model.ModelData;
import fr.toss.game.world.World;

public class ChunkGenerator
{
	public static final int 	CHUNK_VERTICES		= 32;
	public static final int 	CHUNK_VERTEX_COUNT	= CHUNK_VERTICES * CHUNK_VERTICES;
	public static final int 	CHUNK_FLOAT_COUNT	= CHUNK_VERTICES * CHUNK_VERTICES * 3;
	public static final int 	CHUNK_SIZE			= 64;
	public static final int 	CHUNK_INDICES_COUNT	= 6 * (CHUNK_VERTICES - 1) * (CHUNK_VERTICES - 1);
	public static final float	CHUNK_SQUARE_SIZE	= (CHUNK_SIZE / (float)(CHUNK_VERTICES - 1));
	
	private AChunkGeneratorNoise _noiseGenerator;
	
	public ChunkGenerator(Random rng)
	{
		this._noiseGenerator = new ChunkGeneratorSimplex(rng);
	}
	
	public Chunk generateChunk(World world, int x, int z)
	{
		Chunk	chunk;
		
		chunk = new Chunk(world, x, z);
		this._noiseGenerator.generateHeights(world, chunk);
		chunk.setModel(new Model(this.loadChunkModelData(chunk), "Chunk"));
		return (chunk);
	}
	
	private ModelData	loadChunkModelData(Chunk chunk)
	{
		ModelData	modeldata;
		Vector3f	vertex;
		float		ys[][];
		int 		addr;
		int			inds[];

		modeldata = new ModelData(ChunkGenerator.CHUNK_VERTEX_COUNT, ChunkGenerator.CHUNK_INDICES_COUNT);
		ys = chunk.getYs();
		addr = 0;
		for (int z = 0; z < ChunkGenerator.CHUNK_VERTICES; z++)
		{
			for (int x = 0; x < ChunkGenerator.CHUNK_VERTICES; x++)
			{
				modeldata.setVertexFloat(addr * 3 + 0, (float)x / ((float)ChunkGenerator.CHUNK_VERTICES - 1) * ChunkGenerator.CHUNK_SIZE);
				modeldata.setVertexFloat(addr * 3 + 1, ys[x][z]);
				modeldata.setVertexFloat(addr * 3 + 2, (float)z / ((float)ChunkGenerator.CHUNK_VERTICES - 1) * ChunkGenerator.CHUNK_SIZE);
				vertex = this.calculateNormal(chunk, x, z);
				modeldata.setNormalFloat(addr * 3 + 0, vertex.getX());
				modeldata.setNormalFloat(addr * 3 + 1, vertex.getY());
				modeldata.setNormalFloat(addr * 3 + 2, vertex.getZ());
				modeldata.setUvFloat(addr * 2 + 0, (float)x / ((float)ChunkGenerator.CHUNK_VERTICES - 1));
				modeldata.setUvFloat(addr * 2 + 1, (float)z / ((float)ChunkGenerator.CHUNK_VERTICES - 1));
				addr++;
			}
		}
		
		addr = 0;
		inds = new int[4];
		for (int z = 0; z < ChunkGenerator.CHUNK_VERTICES - 1; z++)
		{
			for (int x = 0; x <  ChunkGenerator.CHUNK_VERTICES - 1; x++)
			{
				inds[0] = z * ChunkGenerator.CHUNK_VERTICES + x;
				inds[1] = inds[0] + 1;
				inds[2] = (z + 1) * ChunkGenerator.CHUNK_VERTICES + x;
				inds[3] = inds[2] + 1;
				modeldata.setIndice(addr + 0, inds[0]);
				modeldata.setIndice(addr + 1, inds[2]);
				modeldata.setIndice(addr + 2, inds[1]);
				modeldata.setIndice(addr + 3, inds[1]);
				modeldata.setIndice(addr + 4, inds[2]);
				modeldata.setIndice(addr + 5, inds[3]);
				addr += 6;
			}
		}
		return (modeldata);
	}
	
	private Vector3f	calculateNormal(Chunk chunk, int x, int z)
	{
		Vector3f	normal;
		float		hl;
		float		hr;
		float		hd;
		float		hu;

		if (x == 0)
			return (calculateNormal(chunk, x + 1, z));
		if (z == 0)
			return (calculateNormal(chunk, x, z + 1));
		if (x == CHUNK_VERTICES - 1)
			return (calculateNormal(chunk, x - 1, z));
		if (z == CHUNK_VERTICES - 1)
			return (calculateNormal(chunk, x, z - 1));
		hl = chunk.getY(x - 1, z);
		hr = chunk.getY(x + 1, z);
		hd = chunk.getY(x, z - 1);
		hu = chunk.getY(x, z + 1);
		normal = new Vector3f(hl - hr, 2, hd - hu);
		return (normal.normalise(normal));
	}
}
