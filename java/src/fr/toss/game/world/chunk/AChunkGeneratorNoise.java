package fr.toss.game.world.chunk;

import fr.toss.game.maths.MathUtils;
import fr.toss.game.world.World;

public abstract class AChunkGeneratorNoise
{
	private float 	_baseHeight;
	private float 	_roughness;
	
	public AChunkGeneratorNoise()
	{
		this(0, 128);
	}
	
	public AChunkGeneratorNoise(int baseheight, int roughness)
	{
		this._baseHeight = baseheight;
		this._roughness = roughness;
	}
	
	/** generate all ys for the given chunk coordinate */
	public abstract void	generateHeights(World world, Chunk chunk);
	
	public float getBaseHeight()
	{
		return (this._baseHeight);
	}
	
	public void setBaseHeight(float baseheight)
	{
		this._baseHeight = baseheight;
	}
	
	public float getRoughness()
	{
		return (this._roughness);
	}
	
	public void setRoughness(float roughness)
	{
		this._roughness = roughness;
	}

	protected void stabilizeNoise(Chunk chunk)
	{
		for (int x = 0 ; x < ChunkGenerator.CHUNK_VERTICES ; x++)
	    {
	    	for (int z = 0 ; z < ChunkGenerator.CHUNK_VERTICES ; z++)
	    	{
	    		chunk.setY(x, z, MathUtils.clamp(chunk.getY(x, z), -1.0f, 1.0f));
	    		chunk.setY(x, z, this.getBaseHeight() + chunk.getY(x, z) * this.getRoughness());
	    	}
	    }
	}
}
