/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   skybox.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/15 15:48:50 by rpereira          #+#    #+#             */
/*   Updated: 2015/05/24 14:52:54 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

static void	skybox_delete(t_sky *sky)
{
	if (sky == NULL)
	{
		logger_log("Trying to delete a NULL sky", 2, LOG_WARNING, 0);
		return ;
	}
    glDeleteTextures(1, &(sky->cubemap_day));
    glDeleteTextures(1, &(sky->cubemap_night));
}

void		sky_delete(t_sky *sky)
{
	skybox_delete(sky);
}

static void	loadCubemap(GLuint *id, char *identifier)
{
    char       *files[] = {"right", "left", "top", "bottom", "back", "front"};
	char       buffer[64];
	t_texture  texture;
	int        i;

    glGenTextures(1, id);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_CUBE_MAP, *id);
    for (i = 0; i < 6; i++)
    {
        memset(buffer, 0, sizeof(buffer));
    	sprintf(buffer, "../assets/skybox/%s_%s.bmp", files[i], identifier);
	    if (load_bmp_file(buffer, &texture) == -1)
    	{
    		logger_log("Couldnt get skybox faces", 2, LOG_WARNING, 0);
    		continue ;
    	}
        texture_reverse_ys(&texture, OCTET_PER_PIXEL_BMP);
        glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i,
                    0, GL_RGB,
                    texture.width, texture.height,
                    0, GL_BGR, GL_UNSIGNED_BYTE,
                    texture.pixels);
    	free(texture.pixels);
    }
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
}

void        updateSky(t_world *world, t_renderer *renderer)
{
    calculateSkyColors(world, renderer);
    renderer->sky.rot.y += 0.00001f;
}

void        loadSky(t_renderer *renderer)
{
    t_sky       *sky;
    t_program   *program;

    sky = &(renderer->sky);
    program = renderer->program + PROGRAM_SKYBOX;
    loadCubemap(renderer->sky.cubemaps + CUBEMAP_DAY, "day");
    loadCubemap(renderer->sky.cubemaps + CUBEMAP_DAY2, "day2");
    loadCubemap(renderer->sky.cubemaps + CUBEMAP_NIGHT, "night");
    glUseProgram(program->id);
    load_uniform_int(program->cubemap_day, 0);
    load_uniform_int(program->cubemap_night, 1);
    glUseProgram(0);
    sky->sun.pos = new_point3(0, 0, 0);
    sky->sun.color = new_point3(0, 0, 0);
    sky->sun.skybox_pos = new_point3(0, 0, 0);
    sky->cubemap_day = renderer->sky.cubemaps[CUBEMAP_DAY];
    sky->cubemap_night = renderer->sky.cubemaps[CUBEMAP_NIGHT];
    sky->rot = new_point3(0, 0, 0);
}
