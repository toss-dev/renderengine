/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   skycolor.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/15 15:48:50 by rpereira          #+#    #+#             */
/*   Updated: 2015/05/27 17:42:54 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

# define DEFAULT_SUN_COLOR new_point3(1, 1, 1)
# define SUNRISE_COLOR new_point3(2, 1, 0.5f)

t_point3	mix_color(t_point3 a, t_point3 b, float factor)
{
	t_point3	color;

	color.x = a.x * factor + b.x * (1 - factor);
	color.y = a.y * factor + b.y * (1 - factor);
	color.z = a.z * factor + b.z * (1 - factor);
	return (color);
}

static void		calculateColors(t_sky *sky, float day_ratio)
{
	if (day_ratio >= NIGHT_END && day_ratio < DAY_START / 4)
	{
		sky->sun.color = mix_color(DEFAULT_SUN_COLOR, SUNRISE_COLOR, 1 - (day_ratio - NIGHT_END) / (DAY_START / 4));
	}
	else if (day_ratio < DAY_START)
	{
		sky->sun.color = mix_color(DEFAULT_SUN_COLOR, SUNRISE_COLOR, (day_ratio - NIGHT_END - DAY_START / 4) / (DAY_START / 4 * 3));
	}
	else
	{
		sky->sun.color = DEFAULT_SUN_COLOR;
	}
}

static void	calculateSunPos(t_sky *sky, t_camera *camera, float day_ratio)
{
	float	x;
	float	r;
	int 	i;
	float 	pi4;
	float 	f;

	pi4 = M_PI / 4.0f;
	x = day_ratio * M_PI * 2;
	f = 0;
	for (i = 1 ; i < 8 ; i++)
	{
		if (x < pi4 * i)
		{
			f = (x - pi4 * (i - 1)) / pi4;
			if (i % 2 == 0)
			{
				f = 1 - f;
			}
			break ;
		}
	}
	r = SKYBOX_SIZE + (SKYBOX_DIAGONAL - SKYBOX_SIZE) * (f * f * f); //(voir f(x) = x^3
	sky->sun.skybox_pos.x = cos(x) * r;
	sky->sun.skybox_pos.y = sin(x) * r;
	sky->sun.pos.x = camera->pos.x + sky->sun.skybox_pos.x;
	sky->sun.pos.y = sky->sun.skybox_pos.y;
	(void)camera;
}

void	calculateSkyColors(t_world *world, t_renderer *renderer)
{
	calculateSunPos(&(renderer->sky), &(renderer->camera), world->day_ratio);
	calculateColors(&(renderer->sky), world->day_ratio);
}
