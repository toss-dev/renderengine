/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   weather.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/27 17:40:22 by rpereira          #+#    #+#             */
/*   Updated: 2015/05/27 17:40:46 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

static void	startRain(t_world *world)
{
	float duration;

	duration = 0.1f + rand_float_in_range(0, 0.9f);
	world->weather.is_raining = true;
	world->weather.rain_start = world->day_ratio;
	world->weather.rain_stop = world->day_ratio + duration;
	if (world->weather.rain_stop > 1)
	{
		world->weather.rain_stop -= 1;
	}
	world->weather.rain_transition = rand_float_in_range(0, 0.1f);
}

void		updateWeather(t_world *world)
{
	if (world->weather.is_raining == false)
	{
		startRain(world);
	}
	calculateFog(world);
}

void		loadWeather(t_world *world)
{
	world->weather.fog_color = DEFAULT_FOG_COLOR;
	world->weather.fog_density = DEFAULT_FOG_DENSITY;
	world->weather.fog_gradient = DEFAULT_FOG_GRADIENT;
	world->weather.is_raining = false;
	world->weather.rain_start = 0;
	world->weather.rain_stop = 0;
	world->weather.rain_transition = 0;
}
