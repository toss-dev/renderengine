/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fog.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/27 17:41:39 by rpereira          #+#    #+#             */
/*   Updated: 2015/05/27 17:42:16 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

void	calculateFog(t_world *world)
{
	world->weather.fog_color = DEFAULT_FOG_COLOR;
	world->weather.fog_density = DEFAULT_FOG_DENSITY;
	world->weather.fog_gradient = DEFAULT_FOG_GRADIENT;
}
