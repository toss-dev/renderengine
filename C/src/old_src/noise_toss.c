/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   noise.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/16 17:11:11 by rpereira          #+#    #+#             */
/*   Updated: 2015/05/16 20:28:06 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

# define RAND_INIT 1
# define MAX_ITER (CHUNK_VERTICES * CHUNK_VERTICES * 2)
# define LINK_AMOUNT 8

/**
**	CURRENT ALGORYTHM: (c'est pas terrible, trop random et un peu bourrin)
**	- Set the whole chunk to 0 value
**	- Set a random value in range of [0.0f ; 1.0f] at RAND_INIT random point
**	- for every colliding chunk C:
**		- for i in LINK_AMOUNT
**			- set colliding points (~ i) to C's (~ i) height
**	- iterate MAX_ITER times:
**		- get a random point P
**			- find 3 closests points to P
**			- if P is inside the triangle formed by these 3 points
**				- set P value to the barycenter of the triangle
**			- else		
**				- P has default height
**	- iterate on every point P
**		- find 3 closests points to P
**		- if P is inside the triangle formed by these 3 points
**			- set P value to the barycenter of the triangle
**		- else		
**			- P has default height
*/

/**
**	get value at x z depneding on 3 given points
*/
static float 	interpolate_value(t_point3 points[3], int x, int z)
{
	float	f;

	f = barrycentric(points[0], points[1], points[2], new_point2(x, z));
	if (isnan(f) || f <= 0 || f > 1)
	{
		return (points[0].y);
	}
	return (f);
}

static int	test_nearest(t_point3 nearest[3], float ys[CHUNK_VERTICES][CHUNK_VERTICES], int x, int z, int founded)
{
	int i;

	if (founded == 3)
		return (0);
	if (x < 0 || z < 0 || x >= CHUNK_VERTICES || z >= CHUNK_VERTICES)
		return (0);
	if (ys[x][z] == 0)
		return (0);
	for (i = 0 ; i < founded ; i++)
	{
		if (nearest[i].x == x && nearest[i].y == ys[x][z] && nearest[i].z == z)
			return (0);
	}
	nearest[founded] = new_point3(x, ys[x][z], z);
	return (1);
}

static void	find_nearest(t_point3 nearest[3], float ys[CHUNK_VERTICES][CHUNK_VERTICES], int x, int z)
{
	int 	rayon;
	char	founded;
	int 	i;

	rayon = 1;
	founded = 0;
	while (founded != 3)
	{
		for (i = -rayon ; i < rayon; i++)
		{
			founded += test_nearest(nearest, ys, x + i, z + rayon, founded);
			founded += test_nearest(nearest, ys, x + i, z - rayon, founded);
			founded += test_nearest(nearest, ys, x + rayon, z + i, founded);
			founded += test_nearest(nearest, ys, x - rayon, z + i, founded);
		}
		rayon++;
	}
}

/**
**	calculate height for this coordinate
*/
static float	toss(float ys[CHUNK_VERTICES][CHUNK_VERTICES], int x, int z)
{
	t_point3	nearest[3];

	find_nearest(nearest, ys, x, z);
	return (interpolate_value(nearest, x, z));
}

/**
**	iterate all over the map to calculate ys from the existing one
*/
static void	calculate_height(float ys[CHUNK_VERTICES][CHUNK_VERTICES])
{
	int x;
	int z;
	int i;

	i = 0;

	for (i = 0 ; i < MAX_ITER ; i++)
	{
		x = rand() % CHUNK_VERTICES;
		z = rand() % CHUNK_VERTICES;
		ys[x][z] = toss(ys, x, z);
	}

	for (i = 0 ; i < 4 ; i++)
	{
		for (x = 0 ; x < CHUNK_VERTICES ; x++)
		{
			for (z = 0 ; z < CHUNK_VERTICES ; z++)
			{
				ys[x][z] = toss(ys, x, z);
			}
		}
	}

}

/**
**	iterate all over the map to calculate ys from the existing one
*/
static void	resize_height(float ys[CHUNK_VERTICES][CHUNK_VERTICES])
{
	int x;
	int z;

	for (x = 0; x < CHUNK_VERTICES; x++)
	{
		for (z = 0; z < CHUNK_VERTICES; z++)
		{
			 ys[x][z] = CHUNK_BASE_HEIGHT + ys[x][z] * CHUNK_ROUGHNESS;
			 if (rand() % 4 == 0)
			 {
			 	ys[x][z] += rand_float_in_range(0, 1);
			 }
		}
	}
}

static void	place_random_value(float ys[CHUNK_VERTICES][CHUNK_VERTICES])
{
	int	x;
	int	z;

	x = 0;
	z = 0;
	while (ys[x][z] != 0)
	{
		x = rand_float_in_range(0, CHUNK_VERTICES);
		z = rand_float_in_range(0, CHUNK_VERTICES);
	}
	ys[x][z] = rand_float_in_range(0, 1);
}

static void	prepare_ys(t_chunk *chunk)
{
	int i;

	chunk->ys[0][0] = rand_float_in_range(0, 1);
	chunk->ys[CHUNK_VERTICES - 1][0] = rand_float_in_range(0, 1);
	chunk->ys[0][CHUNK_VERTICES - 1] = rand_float_in_range(0, 1);
	chunk->ys[CHUNK_VERTICES - 1][CHUNK_VERTICES - 1] = rand_float_in_range(0, 1);
	for (i = 0 ; i < RAND_INIT ; i++)
	{
		place_random_value(chunk->ys);
	}
}

static void	convert_to_float(float ys[CHUNK_VERTICES][CHUNK_VERTICES],
							float yscpy[CHUNK_VERTICES][CHUNK_VERTICES],
							int x, int z, int rx, int rz)
{
	ys[x][z] = (yscpy[rx][rz] - CHUNK_BASE_HEIGHT) /(float)CHUNK_ROUGHNESS;
}

static void	copy_ys_brut(float ys[CHUNK_VERTICES][CHUNK_VERTICES],
						float yscpy[CHUNK_VERTICES][CHUNK_VERTICES],
						int x, int z, int rx, int rz)
{
	ys[x][z] = yscpy[rx][rz];
}

void		link_chunk(t_htab chunks, t_chunk *chunk, void (*copy_ys)())
{
	t_chunk	*tmp;
	int 	i;

	tmp = getChunk(chunks, chunk->indx - 1, chunk->indz);
	if (tmp != NULL)
	{
		for (i = 0 ; i < CHUNK_VERTICES ; i++)
		{
			copy_ys(chunk->ys, tmp->ys, 0, i, CHUNK_VERTICES - 1, i);
		}
	}

	tmp = getChunk(chunks, chunk->indx + 1, chunk->indz);
	if (tmp != NULL)
	{
		for (i = 0 ; i < CHUNK_VERTICES ; i++)
		{
			copy_ys(chunk->ys, tmp->ys, CHUNK_VERTICES - 1, i, 0, i);
		}
	}

	tmp = getChunk(chunks, chunk->indx, chunk->indz - 1);
	if (tmp != NULL)
	{
		for (i = 0 ; i < CHUNK_VERTICES ; i++)
		{
			copy_ys(chunk->ys, tmp->ys, i, 0, i, CHUNK_VERTICES - 1);
		}
	}

	tmp = getChunk(chunks, chunk->indx + 1, chunk->indz + 1);
	if (tmp != NULL)
	{
		for (i = 0 ; i < CHUNK_VERTICES ; i++)
		{
			copy_ys(chunk->ys, tmp->ys, i, CHUNK_VERTICES - 1, i, 0);
		}
	}
}

void		generate_toss(t_htab chunks, t_chunk *chunk)
{
	logger_log("Generating new chunk using `toss`", 1, LOG_FINE, 1);
	prepare_ys(chunk);
	link_chunk(chunks, chunk, convert_to_float);
	calculate_height(chunk->ys);
	resize_height(chunk->ys);
	link_chunk(chunks, chunk, copy_ys_brut);
}