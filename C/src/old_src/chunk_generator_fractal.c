/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   chunk.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/10 18:15:45 by rpereira          #+#    #+#             */
/*   Updated: 2015/05/12 13:22:35 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

static float	get_one_diamond(float ys[CHUNK_VERTICES][CHUNK_VERTICES], int x, int z)
{
	int 	nb;
	float	value;

	nb = 0;
	value = 0;
	if (x > 0)
	{
		value += ys[x - 1][z];
		++nb;
	}
	if (x < CHUNK_VERTICES - 2)
	{
		value += ys[x + 1][z];
		++nb;
	}
	if (z > 0)
	{
		value += ys[x][z - 1];
		++nb;
	}
	if (z < CHUNK_VERTICES - 2)
	{
		value += ys[x][z + 1];
		++nb;
	}
	return (value / nb);
}

static void	diamond_square(float ys[CHUNK_VERTICES][CHUNK_VERTICES], int step)
{
	float	scale;
	float	c;

	if (step < 2)
		return ;
	scale = ROUGHNESS * step;
	for (int x = 0; x < STEP; x += step)
	{
		for (int z = 0; z < STEP; z += step)
		{
			c = rand() / (float)(RAND_MAX) * scale * 2 - scale;
			ys[x + step / 2][z + step / 2] = (ys[x][z] + ys[x][z + step]
					+ ys[x + step][z] + ys[x + step][z + step]) / 4 + c;
		}
	}
	for (int x = 0; x < STEP; x += step)
	{
		for (int z = 0; z < STEP; z += step)
		{
			c = rand() / (float)(RAND_MAX) * scale * 2 - scale;
			ys[x + step / 2][z] = get_one_diamond(ys, x + step / 2, z) + c;
			ys[x][z + step / 2] = get_one_diamond(ys, x, z + step / 2) + c;
			ys[x + step / 2][z + step] = get_one_diamond(ys, x + step / 2, z + step) + c;
			ys[x + step][z + step / 2] = get_one_diamond(ys, x + step, z + step / 2) + c;
		}
	}
	diamond_square(ys, step / 2);
}

static void	generate_ys(t_htab *chunks, t_chunk *chunk, float ys[CHUNK_VERTICES][CHUNK_VERTICES])
{
	bzero(ys, sizeof(float) * CHUNK_VERTICES * CHUNK_VERTICES);
	ys[0][0] = /*rand() / (float)RAND_MAX **/ 0;
	ys[STEP][0] = /*rand() / (float)RAND_MAX * */0;
	ys[0][STEP] =/* rand() / (float)RAND_MAX * */0;
	ys[CHUNK_VERTICES - 1][CHUNK_VERTICES - 1] = /*rand() / (float)RAND_MAX **/ 0;
	diamond_square(ys, STEP);
	chunk->square[CHUNK_TOP_LEFT] = ys[0][0];
	chunk->square[CHUNK_TOP_RIGHT] = ys[STEP][0];
	chunk->square[CHUNK_BOT_LEFT] = ys[0][STEP];
	chunk->square[CHUNK_BOT_RIGHT] = ys[STEP][STEP];
	(void)chunks;
}


static void	generate_chunk_model_data(t_model_data *data, float ys[CHUNK_VERTICES][CHUNK_VERTICES])
{
	int	location;

	for (int z = 0; z < CHUNK_VERTICES; z++)
	{
		for (int x = 0; x < CHUNK_VERTICES; x++)
		{
			location = x * CHUNK_VERTICES * 3 + z * 3;
			data->positions[location + 0] = x / (float)(CHUNK_VERTICES - 1) * CHUNK_SIZE;
			data->positions[location + 1] = ys[x][z];
			data->positions[location + 2] = z / (float)(CHUNK_VERTICES - 1) * CHUNK_SIZE;
			location = x * CHUNK_VERTICES * 2 + z * 2;
			data->uvs[location + 0] = x / (float)CHUNK_VERTICES;
			data->uvs[location + 1] = z / (float)CHUNK_VERTICES;
		}
	}
}

static void	generate_chunk_model_indices(t_model_data *data)
{
	int	inds[4];
	int	idx;

	idx = 0;
	for (int x = 0; x < CHUNK_VERTICES - 1; x++)
	{
		for (int z = 0; z < CHUNK_VERTICES - 1; z++)
		{
			inds[0] = z * CHUNK_VERTICES + x;
			inds[1] = inds[0] + 1;
			inds[2] = (z + 1) * CHUNK_VERTICES + x;
			inds[3] = inds[2] + 1;
			data->indices[idx + 0] = inds[0];
			data->indices[idx + 1] = inds[2];
			data->indices[idx + 2] = inds[1];
			data->indices[idx + 3] = inds[1];
			data->indices[idx + 4] = inds[2];
			data->indices[idx + 5] = inds[3];
			idx += 6;
		}
	}
}

static void	prepare_model_data(t_model_data *data)
{
	data->positions = (GLfloat*)malloc(sizeof(GLfloat) * CHUNK_VERTEX_COUNT * 3);
	data->indices = (GLuint*)malloc(sizeof(GLuint) * CHUNK_INDICES_COUNT);
	data->uvs = (GLfloat*)malloc(sizeof(GLfloat) * CHUNK_VERTEX_COUNT * 2);
	data->positions_count = CHUNK_VERTEX_COUNT * 3;
	data->indices_count = CHUNK_INDICES_COUNT;
	data->uvs_count = CHUNK_VERTEX_COUNT * 2;
}

static void	end_model_data(t_model_data *data)
{
	free(data->positions);
	free(data->indices);
	free(data->uvs);
}

void		generate_chunk(t_htab *chunks, t_chunk *chunk)
{
	t_model_data	data;
	float			ys[CHUNK_VERTICES][CHUNK_VERTICES];
	
	prepare_model_data(&data);
	{
		generate_ys(chunks, chunk, ys);
		generate_chunk_model_data(&data, ys);
		generate_chunk_model_indices(&data);
		chunk->model = load_model(&data);
	}
	end_model_data(&data);
}
