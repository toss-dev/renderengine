/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   chunk.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/10 18:15:45 by rpereira          #+#    #+#             */
/*   Updated: 2015/05/14 14:02:23 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

static t_point3	calculate_normal(t_chunk *chunk, int x, int z)
{
	t_point3	normal;
	float		hl;
	float		hr;
	float		hd;
	float		hu;

	if (x == 0)
		return (calculate_normal(chunk, x + 1, z));
	if (z == 0)
		return (calculate_normal(chunk, x, z + 1));
	if (x == CHUNK_VERTICES - 1)
		return (calculate_normal(chunk, x - 1, z));
	if (z == CHUNK_VERTICES - 1)
		return (calculate_normal(chunk, x, z - 1));
	hl = chunk->ys[x - 1][z];
	hr = chunk->ys[x + 1][z];
	hd = chunk->ys[x][z - 1];
	hu = chunk->ys[x][z + 1];
	normal = new_point3(hl - hr, 2, hd - hu);
	return (vec_normalize(normal));
}

static void	generate_chunk_model_data(t_chunk *chunk, t_model_data *data)
{
	t_point3	normal;
	int			location;
	int		x;
	int		z;

	for (z = 0; z < CHUNK_VERTICES; z++)
	{
		for (x = 0; x < CHUNK_VERTICES; x++)
		{
			location = x * CHUNK_VERTICES * 3 + z * 3;
			data->vertices[location + 0] = x / (float)(CHUNK_VERTICES - 1) * CHUNK_SIZE;
			data->vertices[location + 1] = chunk->ys[x][z];
			data->vertices[location + 2] = z / (float)(CHUNK_VERTICES - 1) * CHUNK_SIZE;
			normal = calculate_normal(chunk, x, z);
			data->normals[location + 0] = normal.x;
			data->normals[location + 1] = normal.y;
			data->normals[location + 2] = normal.z;
			location = x * CHUNK_VERTICES * 2 + z * 2;
			data->uvs[location + 0] = x / (float)CHUNK_VERTICES;
			data->uvs[location + 1] = z / (float)CHUNK_VERTICES;
		}
	}
}

static void	generate_chunk_model_indices(t_model_data *data)
{
	int	inds[4];
	int	idx;
	int	x;
	int	z;

	idx = 0;
	for (x = 0; x < CHUNK_VERTICES - 1; x++)
	{
		for (z = 0; z < CHUNK_VERTICES - 1; z++)
		{
			inds[0] = z * CHUNK_VERTICES + x;
			inds[1] = inds[0] + 1;
			inds[2] = (z + 1) * CHUNK_VERTICES + x;
			inds[3] = inds[2] + 1;
			data->indices[idx + 0] = inds[0];
			data->indices[idx + 1] = inds[2];
			data->indices[idx + 2] = inds[1];
			data->indices[idx + 3] = inds[1];
			data->indices[idx + 4] = inds[2];
			data->indices[idx + 5] = inds[3];
			idx += 6;
		}
	}
}

static void	prepare_model_data(t_model_data *data)
{
	data->vertices = (GLfloat*)malloc(sizeof(GLfloat) * CHUNK_VERTEX_COUNT * 3);
	data->indices = (GLuint*)malloc(sizeof(GLuint) * CHUNK_INDICES_COUNT * 3);
	data->uvs = (GLfloat*)malloc(sizeof(GLfloat) * CHUNK_VERTEX_COUNT * 2);
	data->normals = (GLfloat*)malloc(sizeof(GLfloat) * CHUNK_VERTEX_COUNT * 3);
	data->vertices_count = CHUNK_VERTEX_COUNT;
	data->triangles_count = CHUNK_INDICES_COUNT;
	data->uvs_count = CHUNK_VERTEX_COUNT;
	data->normals_count = CHUNK_VERTEX_COUNT;
}

static void	end_model_data(t_model_data *data)
{
	free(data->vertices);
	free(data->indices);
	free(data->uvs);
	free(data->normals);
}

void		generate_chunk(t_htab chunks, t_chunk *chunk)
{
	t_model_data	data;

	prepare_model_data(&data);
	{
		fill_ys(chunks, chunk);
		generate_chunk_model_data(chunk, &data);
		generate_chunk_model_indices(&data);
		chunk->model = loadModel(&data);
	}
	end_model_data(&data);
}
