/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   entity.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/09 13:47:30 by rpereira          #+#    #+#             */
/*   Updated: 2015/05/14 16:09:13 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

t_entity	new_entity(t_point3 pos, t_point3 scal, t_point3 rot, t_pair p)
{
	t_entity	entity;

	entity.pos = pos;
	entity.scale = scal;
	entity.rot = rot;
	entity.pos_vec = new_point3(0, 0, 0);
	entity.scale_vec = new_point3(0, 0, 0);
	entity.rot_vec = new_point3(0, 0, 0);
	entity.id = p.a;
	entity.texture_id = p.b;
	entity.speed = 0.25f;
	return (entity);
}
