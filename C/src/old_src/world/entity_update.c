/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   entity_update.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/11 20:54:49 by rpereira          #+#    #+#             */
/*   Updated: 2015/05/15 15:37:46 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

void	update_default(t_entity *entity, t_world *world)
{
	float	height;

	height = get_height_at(world, entity->pos.x, entity->pos.z);
	if (entity->pos.y > height)
	{
		entity->pos_vec.y = -1;
	}
	else
		entity->pos.y = height;
	if (rand() % 120 == 0)
	{
		float x = rand() % 2 == 0 ? -rand() : rand();
		float z = rand() % 2 == 0 ? -rand() : rand();
		entity->pos_vec = vec_normalize(new_point3(x, 0, z));
		entity->pos_vec.x *= entity->speed;
		entity->pos_vec.z *= entity->speed;
	}

}

t_entity_function	g_entity_update[ENTITY_MAX] = {
	update_default,
	NULL
};

void	update_entity(t_entity *entity, t_world *world)
{
	if (entity->id < ENTITY_MAX)
	{
		if (g_entity_update[entity->id] != NULL)
			g_entity_update[entity->id](entity, world);
	}
	entity->rot = vec_add(entity->rot, entity->rot_vec);
	entity->pos = vec_add(entity->pos, entity->pos_vec);
	entity->scale = vec_add(entity->scale, entity->scale_vec);
}
