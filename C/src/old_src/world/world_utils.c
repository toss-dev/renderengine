/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   world_utils.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/13 21:05:44 by rpereira          #+#    #+#             */
/*   Updated: 2015/05/13 21:44:22 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

float	get_height_at(t_world *world, float x, float z)
{
	t_chunk		*chunk;
	int			gridx;
	int			gridz;
	int			x_coord;
	int			z_coord;

	chunk = get_chunk_at(world->chunks, x, z);
	if (chunk == NULL)
		return (0);
	gridx = (int)floor((x - chunk->posx) / (float)CHUNK_SQUARE_SIZE);
	gridz = (int)floor((z - chunk->posz) / (float)CHUNK_SQUARE_SIZE);
	x_coord = fmod(x, CHUNK_SIZE) / (float)CHUNK_SIZE;
	z_coord = fmod(z, CHUNK_SIZE) / (float)CHUNK_SIZE;
	if (x_coord > 1 - z_coord)
	{
		return (barrycentric(new_point3(1, chunk->ys[gridx + 1][gridz], 0),
		new_point3(1, chunk->ys[gridx + 1][gridz + 1], 1),
		new_point3(0, chunk->ys[gridx][gridz + 1], 1),
		new_point2(x_coord, z_coord)));
	}
	return (barrycentric(new_point3(0, chunk->ys[gridx][gridz], 0),
	new_point3(1, chunk->ys[gridx + 1][gridz], 0),
	new_point3(0, chunk->ys[gridx][gridz + 1], 1),
	new_point2(x_coord, z_coord)));
}
