/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   world.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/09 11:42:21 by rpereira          #+#    #+#             */
/*   Updated: 2015/05/14 16:42:06 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

static void	update_entities(t_batch *batch, t_world *world)
{
	list_iter(batch->entities, (t_iter_function)update_entity, world);
}

void		update_world(t_world *world)
{
	htab_iter(world->batchs, (t_iter_function)update_entities, world);
	world->ticks++;
}

void		spawn_entity(t_world *world, t_entity entity, char *file)
{
	t_batch	*batch;
	t_batch	tmp;

	if ((batch = (t_batch*)htab_get(world->batchs, file)) != NULL)
	{
		list_add(batch->entities, &entity, sizeof(t_entity));
	}
	else
	{
		tmp = new_batch(entity, file);
		if (tmp.model.vertex_count != 0 && tmp.entities != NULL)
		{
			htab_insert(world->batchs, file, &tmp, sizeof(t_batch));
		}
	}
}

void		init_world(t_world *world)
{
	int	i;
	int	j;

	logger_log("Loading world...", 1, LOG_FINE, 0);
	world->chunks = htab_new(CHUNK_HASH_MAP_SIZE);
	world->batchs = htab_new(BATCH_HASH_MAP_SIZE);
	load_texture_file("./assets/heightmaps/heightmap.bmp", &(world->height_map));
	for (i = 0; i < CHUNK_PER_HEIGHTMAP; i++)
	{
		for (j = 0; j < CHUNK_PER_HEIGHTMAP; j++)
		{
			create_chunk(world, i, j);
			spawn_entity(world, new_entity(new_point3(128, 128, 128), new_point3(1, 1, 1), new_point3(0, 0, 0), new_pair(0, 0)), "./assets/obj/teapot.obj");
		}
	}
}
