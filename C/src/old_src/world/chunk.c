/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   chunk.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/12 12:31:05 by rpereira          #+#    #+#             */
/*   Updated: 2015/05/13 19:17:38 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

static void		generate_chunk_key(char buff[], int x, int z)
{
	ft_putnbr_base_buffer(buff, x, "0123456789", 10);
	ft_strcat(buff, ":");
	ft_putnbr_base_buffer(buff + ft_strlen(buff), z, "0123456789", 10);
}

t_chunk			*get_chunk(t_htab chunks, int x, int z)
{
	char	buffer[64];

	generate_chunk_key(buffer, x, z);
	return (htab_get(chunks, buffer));
}

t_chunk			*get_chunk_at(t_htab chunks, float x, float z)
{
	if (x < 0)
		x -= CHUNK_SIZE;
	if (z < 0)
		z -= CHUNK_SIZE;
	return (get_chunk(chunks, x / CHUNK_SIZE, z / CHUNK_SIZE));
}

static t_chunk	new_chunk(t_world *world, int x, int z)
{
	t_chunk	chunk;

	chunk.indx = x;
	chunk.indz = z;
	chunk.posx = x * CHUNK_SIZE;
	chunk.posz = z * CHUNK_SIZE;
	chunk.loaded = false;
	generate_chunk(&chunk, &(world->height_map));
	return (chunk);
}

void			create_chunk(t_world *world, int x, int z)
{
	t_chunk	chunk;
	char	buffer[64];

	chunk = new_chunk(world, x, z);
	generate_chunk_key(buffer, x, z);
	htab_insert(world->chunks, buffer, &chunk, sizeof(t_chunk));
}

void		load_chunk(t_chunk *chunk)
{
	chunk->loaded = true;
}

void		unload_chunk(t_chunk *chunk)
{
	chunk->loaded = false;
}
