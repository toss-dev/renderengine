/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   chunk.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/12 12:31:05 by rpereira          #+#    #+#             */
/*   Updated: 2015/05/13 19:17:38 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

static float get_value(float ys[CHUNK_VERTICES][CHUNK_VERTICES], int x, int z)
{
	if (x == CHUNK_VERTICES)
	{
		x--;
	}
	if (z == CHUNK_VERTICES)
	{
		z--;
	}
	return (ys[x][z]);
}

static void set_value(float ys[CHUNK_VERTICES][CHUNK_VERTICES], int x, int z, float value)
{
	if (ys[x][z] == 0)
	{
    	ys[x][z] = value;
	}
}

static void	sample_square(float ys[CHUNK_VERTICES][CHUNK_VERTICES], int x, int z, int size, double r)
{
	int 	hs;
    double	a;
	double	b;
	double	c;
	double	d;

    hs = size / 2;
    a = get_value(ys, x - hs, z - hs);
    b = get_value(ys, x + hs, z - hs);
    c = get_value(ys, x - hs, z + hs);
    d = get_value(ys, x + hs, z + hs);
    set_value(ys, x, z, ((a + b + c + d) / 4.0) + r);
}

static void	sample_diamond(float ys[CHUNK_VERTICES][CHUNK_VERTICES], int x, int z, int size, double r)
{
    int 	hs;
    double	a;
	double	b;
	double	c;
	double	d;

    hs = size / 2; 
    a = get_value(ys, x - hs, z);
    b = get_value(ys, x + hs, z);
    c = get_value(ys, x, z - hs);
    d = get_value(ys, x, z + hs);
    set_value(ys, x, z, ((a + b + c + d) / 4.0) + r);
}

static void	diamond_square(float ys[CHUNK_VERTICES][CHUNK_VERTICES], int stepsize, double scale)
{
    int halfstep;
    int x;
    int z;

    halfstep = stepsize / 2;
    for (z = halfstep; z < CHUNK_VERTICES + halfstep; z += stepsize)
    {
        for (x = halfstep; x < CHUNK_VERTICES + halfstep; x += stepsize)
        {
            sample_square(ys, x, z, stepsize, rand_float_in_range(0, 1) * scale);
        }
    }
 
    for (z = 0 ; z < CHUNK_VERTICES ; z += stepsize)
    {
        for (x = 0; x < CHUNK_VERTICES ; x += stepsize)
        {
            sample_diamond(ys, x + halfstep, z, stepsize, rand_float_in_range(0, 1) * scale);
            sample_diamond(ys, x, z + halfstep, stepsize, rand_float_in_range(0, 1) * scale);
        }
    }
}

static void	link_diamond_square_chunks(t_htab chunks, t_chunk *chunk)
{
	t_chunk	*tmp;
	int 	i;
	int 	j;

	tmp = get_chunk(chunks, chunk->indx - 1, chunk->indz + 0);
	if (tmp != NULL)
	{
		for (i = 0 ; i < CHUNK_VERTICES ; i++)
		{
			chunk->ys[0][i] = tmp->ys[CHUNK_VERTICES - 1][i];
			for (j = 1 ; j < 8 ; j++)
			{
				chunk->ys[j][i] = tmp->ys[CHUNK_VERTICES - 1 - j][i];
			}
		}
	}
}


static void	prepare_ys(t_chunk *chunk)
{
	chunk->ys[0][0] = rand_int(CHUNK_ROUGHNESS) + CHUNK_BASE_HEIGHT;
	chunk->ys[CHUNK_VERTICES - 1][0] = rand_int(CHUNK_ROUGHNESS) + CHUNK_BASE_HEIGHT;
	chunk->ys[0][CHUNK_VERTICES - 1] = rand_int(CHUNK_ROUGHNESS) + CHUNK_BASE_HEIGHT;
	chunk->ys[CHUNK_VERTICES - 1][CHUNK_VERTICES - 1] = rand_int(CHUNK_ROUGHNESS) + CHUNK_BASE_HEIGHT;
}

void		generate_diamond_square(t_htab chunks, t_chunk *chunk)
{
    logger_log("Generating new chunk using `diamond square algorythm`", 1, LOG_FINE, 1);

    prepare_ys(chunk);
	link_diamond_square_chunks(chunks, chunk);

   	int step;
   	float scale;

    step = CHUNK_VERTICES / 2;
    scale = 1.0f;
    while (step > 1)
    {
    	diamond_square(chunk->ys, step, scale);
    	step = step / 2;
    	scale = scale / 2;
    }

	link_diamond_square_chunks(chunks, chunk);

}
