/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   window.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/10 13:45:19 by rpereira          #+#    #+#             */
/*   Updated: 2015/05/12 17:21:25 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

static void	prepare_window(void)
{
	#ifdef __APPLE__
		logger_log("Setting apple window hint", 1, LOG_FINE, 1);
		glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
  		glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 2);
  		glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
  		glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	#endif
}

static void	prepare_glew(void)
{
	# ifdef _WIN32
		printf("openGL version: %s\n", glGetString(GL_VERSION));
		GLenum err = glewInit();
		if (err != GLEW_OK)
		{
			printf("err: %s\n", glewGetErrorString(err));
			logger_log("Couldnt initialize glew", 2, LOG_FATAL, 1);
		}
	# endif
}

void	initWindow(t_window *window)
{
	logger_log("Initializing window", 1, LOG_FINE, 0);
	if (!glfwInit())
	{
		logger_log("Couldnt initialized glfw", 2, LOG_FATAL, 0);
	}
	prepare_window();
	window->ptr = glfwCreateWindow(WIDTH, HEIGHT, TITLE, NULL, NULL);
	if (window->ptr == NULL)
    {
    	logger_log("Couldnt open window", 2, LOG_ERROR, 0);
        glfwTerminate();
        exit(EXIT_FAILURE);
    }
	glfwMakeContextCurrent(window->ptr);
	prepare_glew();
	logger_log("Window initialized", 1, LOG_FINE, 0);
}
