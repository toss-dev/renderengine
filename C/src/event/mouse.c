/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mouse.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/23 11:25:38 by rpereira          #+#    #+#             */
/*   Updated: 2015/05/12 18:26:43 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

bool		g_press = false;

void	cursorMoveCallback(GLFWwindow *window, double x, double y)
{
	static t_point2	last_pos = {0, 0};
	int				screen_size[2];
	t_point2		vec;
	t_camera		*camera;

	camera = &(g_env->renderer.camera);
	glfwGetWindowSize(window, screen_size + 0, screen_size + 1);
	if (x < 0 || y < 0 || x >= WIDTH || y >= HEIGHT)
		return ;
	if (g_press)
	{
		vec.y = (x - screen_size[0] / 2) / (float)screen_size[0] * 2;
		vec.x = (y - screen_size[1] / 2) / (float)screen_size[1] * 2;
		camera->vec_rot = new_point3(vec.x, vec.y, 0);
	}
	else if (0)
	{
		camera->yaw += (x - last_pos.x) * 0.005f;
		camera->pitch += (y - last_pos.y) * 0.005f;

		camera->pitch = MIN(camera->pitch, 89.0f);
		camera->pitch = MAX(camera->pitch, -89.0f);

	}
	last_pos.x = x;
	last_pos.y = y;
	(void)window;
}

void	mouseButtonCallback(GLFWwindow* window, int button, int action, int mods)
{
	t_camera	*camera;

	camera = &(g_env->renderer.camera);
	if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_RELEASE)
	{
		g_press = false;
		camera->vec_rot = new_point3(0, 0, 0);
	}
	else if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
	{
		g_press = true;
		camera->vec_rot = new_point3(0, 0, 0);
	}
	(void)window;
	(void)mods;
}