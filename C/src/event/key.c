/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/23 12:37:38 by rpereira          #+#    #+#             */
/*   Updated: 2015/05/12 17:34:01 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

static void	test_explosion_particle(t_renderer *renderer)
{
	int i;

	for (i = 0 ; i < PARTICLES_MAX / 4; i++)
	{
		t_particle p = new_particle(renderer->camera.pos,
							new_point3(2, 2, 2),
							new_point3(0, rand_float_in_range(0, M_PI * 2), 0),
							new_point3(0.5f, 0, 0),
							ParticleMoving, TextureAtlasExplosion, 5 * 5 * 10);
		p.pos_vec.x = rand_float_in_range(-1, 1);
		p.pos_vec.y = tan(rand_float_in_range(-1, 1));
		p.pos_vec.z = rand_float_in_range(-1, 1);
		p.pos_vec = vec_normalize(p.pos_vec);
		addParticle(renderer, p);
	}
}

void	keyCallback(GLFWwindow *window, int key, int scancode, int action, int mods)
{
	if (action == GLFW_RELEASE)
	{
		cameraControlRelease(&(g_env->renderer.camera), key);
	}
	else if (action == GLFW_PRESS)
	{
		cameraControlPress(&(g_env->renderer.camera), key);
	}
	if (key == GLFW_KEY_ESCAPE)
	{
		glfwSetWindowShouldClose(window, GL_TRUE);
	}
	else if (action == GLFW_PRESS && key == GLFW_KEY_R)
	{
		test_explosion_particle(&(g_env->renderer));
	}
	(void)scancode;
	(void)mods;
}
