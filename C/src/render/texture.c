/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   texture.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/09 15:27:02 by rpereira          #+#    #+#             */
/*   Updated: 2015/05/12 19:01:26 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

void		texture_reverse_ys(t_texture *texture, int octet_per_pixel)
{
	char			*buffer;
	unsigned int	i;
	int 			sizeline;
	int				top;
	int				bot;

	sizeline = texture->width * octet_per_pixel;
	buffer = (char*)malloc(sizeof(char) * sizeline);
	for (i = 0 ; i < texture->height / 2 ; i++)
	{
		bot = i * sizeline;
		top = (texture->height - 1 - i) * sizeline;
		ft_memcpy(buffer, texture->pixels + bot, sizeline);
		ft_memcpy(texture->pixels + bot, texture->pixels + top, sizeline);
		ft_memcpy(texture->pixels + top, buffer, sizeline);
	}
	free(buffer);
}

void		textures_delete(t_renderer *renderer)
{
	int i;

	glDeleteTextures(TEXTURE_MAX, renderer->textures);
	for (i = 0 ; i < TEXTURE_ATLASES_MAX ; i++)
	{
		glDeleteTextures(1, &(renderer->textures_atlases[i].id));
	}
}

static int	load_texture(char *filepath, t_texture *texture)
{
	static t_texture_format format[2] = {
		{"*.png", load_png_file},
		{"*.bmp", load_bmp_file}
	};
	char	buffer[512];
	int		i;

	for (i = 0 ; i < 2 ; i++)
	{
		if (ft_match(filepath, format[i].extension))
		{
			return (format[i].decoder_funct(filepath, texture));
		}
	}
	sprintf(buffer, "Missing file extension for file: %s", filepath);
	logger_log(buffer, 2, LOG_ERROR, 0);
	return (-1);
}

static void	loadTexture(GLuint id, char const *texture_file)
{
	t_texture	texture;
	char		buffer[128];
	char		msg[256];

	sprintf(buffer, "../assets/textures/%s", texture_file);
	sprintf(msg, "Loading texture file: %s", buffer);
	logger_log(msg, 1, LOG_FINE, 0);
	load_texture(buffer, &texture);
	glBindTexture(GL_TEXTURE_2D, id);
	glTexImage2D(GL_TEXTURE_2D, 0,
					texture.internalformat,
					texture.width, texture.height,
					0, texture.format, GL_UNSIGNED_BYTE, texture.pixels);
	glGenerateMipmap(GL_TEXTURE_2D);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_LOD_BIAS, -2.6f);
	free(texture.pixels);
}

static void	loadTextureAtlases(t_texture_atlas *texture, char const *file, int cols, int lines)
{
	char	buffer[128];

	glGenTextures(1, &(texture->id));
	sprintf(buffer, "particles/%s", file);
	loadTexture(texture->id, buffer);
	texture->cols = cols;
	texture->lines = lines;
}

void		loadTextures(t_renderer *renderer)
{
	logger_log("Loading textures...", 1, LOG_FINE, 0);
	glGenTextures(TEXTURE_MAX, renderer->textures);
	loadTexture(renderer->textures[TextureColor], "colorful.bmp");
	loadTexture(renderer->textures[TextureGrass], "grass.bmp");
	loadTexture(renderer->textures[TextureSmallGrass], "smallgrass.png");
	loadTexture(renderer->textures[TextureChunk], "chunk.png");
	
	loadTextureAtlases(renderer->textures_atlases + TextureAtlasRaindrop, "raindrop.png", 3, 1);
	loadTextureAtlases(renderer->textures_atlases + TextureAtlasFlamedrop, "flamedrop.png", 5, 2);
	loadTextureAtlases(renderer->textures_atlases + TextureAtlasFlame, "flame.png", 8, 4);
	loadTextureAtlases(renderer->textures_atlases + TextureAtlasExplosion, "explosion.png", 5, 5);
	loadTextureAtlases(renderer->textures_atlases + TextureAtlasLeave, "leaves.png", 1, 1);
	loadTextureAtlases(renderer->textures_atlases + TextureAtlasMagic, "magic.png", 2, 2);
}
