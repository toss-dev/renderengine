/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   render_uniforms.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/05 09:39:19 by rpereira          #+#    #+#             */
/*   Updated: 2015/05/13 12:17:37 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

void	load_uniform_matrix(GLuint id, float *matrix)
{
	glUniformMatrix4fv(id, 1, GL_TRUE, matrix);
}

void	load_uniform_float(GLuint id, float value)
{
	glUniform1f(id, value);
}

void	load_uniform_vec(GLuint id, t_point3 p)
{
	glUniform3f(id, p.x, p.y, p.z);
}

void	load_uniform_int(GLuint id, GLuint integer)
{
	glUniform1i(id, integer);
}

void	load_uniform_weather(t_weather weather, t_program *program)
{
	load_uniform_vec(program->fog_color, weather.fog_color);
	load_uniform_float(program->fog_density, weather.fog_density);
	load_uniform_float(program->fog_gradient, weather.fog_gradient);
}