/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   camera.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/04 18:11:55 by rpereira          #+#    #+#             */
/*   Updated: 2015/05/15 15:34:55 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

void	updateCameraMovement(t_camera *cam)
{
	if (cam->state & C_MOVE_FORWARD)
	{
		cam->vec_pos = vec3_pitch_yaw(cam->pitch, cam->yaw);
		cam->vec_pos = vec_multiply(cam->vec_pos, -1);
	}
	else if (cam->state & C_MOVE_BACKWARD)
	{
		cam->vec_pos = vec3_pitch_yaw(cam->pitch, cam->yaw);
	}
	else if (cam->state & C_MOVE_LEFT)
	{
		cam->vec_pos = vec3_pitch_yaw(cam->pitch, cam->yaw);
		cam->vec_pos = new_point3(-cam->vec_pos.z, 0, cam->vec_pos.x);
	}
	else if (cam->state & C_MOVE_RIGHT)
	{
		cam->vec_pos = vec3_pitch_yaw(cam->pitch, cam->yaw);
		cam->vec_pos = vec_multiply(cam->vec_pos, -1);
		cam->vec_pos = new_point3(-cam->vec_pos.z, 0, cam->vec_pos.x);
	}
	else
		cam->vec_pos = new_point3(0, 0, 0);
}

static void	moveCamera(t_camera *camera)
{
	static float	rot_speed = 0.07f;
	static float	speed = CHUNK_SIZE / 2 / TPS;

	camera->pitch += camera->vec_rot.x * rot_speed;
	camera->yaw += camera->vec_rot.y * rot_speed;
	camera->pos.x += camera->vec_pos.x * speed;
	camera->pos.y += camera->vec_pos.y * speed;
	camera->pos.z += camera->vec_pos.z * speed;
}

void		updateCamera(t_camera *camera)
{
	updateCameraMovement(camera);
	moveCamera(camera);
	matrix_view(camera->view_matrix, camera->pitch, camera->yaw, camera->pos);
	camera->look_vec = vec3_pitch_yaw(camera->pitch, camera->yaw);
	
	t_pair p = getChunkIndexForPos(camera->pos.x, camera->pos.z);
	camera->chunk_idx = new_point2(p.a, p.b);
}

static void	bind_projection_matrix(t_camera camera, t_program program)
{
	float	*matrix;
	float	aspect;

	logger_log("Setting projection matrix", 1, LOG_FINE, 0);
	aspect = WIDTH / (float)HEIGHT;
	matrix = matrix_projection(camera.fov, camera.near, camera.far, aspect);
	if (matrix == NULL)
		logger_log("Couldnt log projection matrix", 2, LOG_FATAL, 0);
	glUseProgram(program.id);
	logger_log("Binding projection matrix...", 1, LOG_FINE, 0);
	load_uniform_matrix(program.project_matrix, matrix);
	glUseProgram(0);
	free(matrix);
}

void		loadCamera(t_renderer *renderer)
{
	int	i;

	i = 0;
	logger_log("Loading camera...", 1, LOG_FINE, 0);
	logger_log("Setting camera's data", 1, LOG_FINE, 0);
	renderer->camera.pos = new_point3(0, CHUNK_BASE_HEIGHT + CHUNK_ROUGHNESS, 0);
	renderer->camera.vec_rot = new_point3(0, 0, 0);
	renderer->camera.vec_pos = new_point3(0, 0, 0);
	renderer->camera.fov = 70;
	renderer->camera.near = 1;
	renderer->camera.far = CHUNK_RENDER_DISTANCE * CHUNK_SIZE;
	printf("far: %f\n", SKYBOX_DIAGONAL);
	for (i = 0; i < PROGRAM_MAX; i++)
	{
		bind_projection_matrix(renderer->camera, renderer->program[i]);
	}
	renderer->camera.state = 0;
	renderer->camera.pitch = 0;
	renderer->camera.yaw = M_PI / 2;
	if ((renderer->camera.view_matrix = new_matrix()) == NULL)
		logger_log("Not enough memory", 2, LOG_FATAL, 0);
}
