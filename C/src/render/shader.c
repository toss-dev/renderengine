/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shader.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/04 12:55:18 by rpereira          #+#    #+#             */
/*   Updated: 2015/05/12 17:49:06 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

void			shader_delete(GLuint program_id, GLuint shader_id)
{
	glDetachShader(program_id, shader_id);
	glDeleteShader(shader_id);
}

static void		print_log(GLuint object)
{
	char	*log;
	GLint	log_length;

	log_length = 0;
	if (glIsShader(object))
		glGetShaderiv(object, GL_INFO_LOG_LENGTH, &log_length);
	else if (glIsProgram(object))
		glGetProgramiv(object, GL_INFO_LOG_LENGTH, &log_length);
	else
		logger_log("Not a shader program", 2, LOG_FATAL, 0);
	log = ft_strnew(sizeof(char) * log_length);
	if (glIsShader(object))
		glGetShaderInfoLog(object, log_length, NULL, log);
	else if (glIsProgram(object))
		glGetProgramInfoLog(object, log_length, NULL, log);
	log = ft_strjoin("Error on shader program:\n", log);
	logger_log(log, 2, LOG_ERROR, 0);
	free(log);
}

static GLchar	*map_file(char const *file)
{
	FILE	*fp;
	size_t	size;
	GLchar	*buffer;

	fp = fopen(file, "rb");
	if (fp == NULL)
		return (NULL);
	fseek(fp, 0, SEEK_END);
	size = ftell(fp);
	rewind(fp);
	if ((buffer = ft_memalloc(size + 1)) == NULL)
		return (NULL);
	fread(buffer, size, 1, fp);
	buffer[size] = 0;
	fclose(fp);
	return (buffer);
}

GLuint			loadShader(char const *str, GLenum type)
{
	GLuint			shader;
	GLchar const	*file;
	GLint			compilation;

	logger_log(str, 1, LOG_FINE, 1);
	if ((file = map_file(str)) == NULL)
	{
		logger_log("Cant map shader program", 2, LOG_ERROR, 0);
		return (0);
	}
	shader = glCreateShader(type);
	glShaderSource(shader, 1, &file, NULL);
	free((void*)file);
	glCompileShader(shader);
	glGetShaderiv(shader, GL_COMPILE_STATUS, &compilation);
	if (compilation == GL_FALSE)
	{
		print_log(shader);
		return (0);
	}
	logger_log("... loaded properly.", 1, LOG_FINE, 1);
	return (shader);
}
