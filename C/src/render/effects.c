/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   effects.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/12 17:43:08 by rpereira          #+#    #+#             */
/*   Updated: 2015/05/14 16:03:12 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

# define PERIMETER (CHUNK_SIZE * 2)

static void	doRain(t_world *world, t_renderer *renderer)
{
	return ;
	
	t_particle	p;
	t_point3	pos;
	int			i;
	float 		x;
	float 		z;

	for (i = 0 ; i < PARTICLES_MAX / 16 ; i++)
	{
		x = renderer->camera.pos.x + ((rand() % 2 == 0) ? -(rand() % PERIMETER) : rand() % PERIMETER);
		z = renderer->camera.pos.z + ((rand() % 2 == 0) ? -(rand() % PERIMETER) : rand() % PERIMETER);
		pos = new_point3(x, renderer->camera.pos.y + rand_float_in_range(10, 400), z),
		p = new_particle(pos,
						new_point3(1, 1, 1),
						new_point3(0, rand_float_in_range(0, 2 * M_PI), 0),
						new_point3(0, 0, 0.5f),
						ParticleRain, TextureAtlasRaindrop, 1);
		addParticle(renderer, p);
	}
	(void)world;
}

/** update visuals effects (rain, lighting bolt etc...) */
void	updateEffects(t_world *world, t_renderer *renderer)
{
	if (world->weather.is_raining)
	{
		doRain(world, renderer);
	}
}