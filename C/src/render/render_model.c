/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   render_model.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/10 16:33:37 by rpereira          #+#    #+#             */
/*   Updated: 2015/05/15 15:27:48 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

void 	loadModels(t_renderer *renderer)
{
	renderer->model_batchs[ModelTree] = batch_new("tree_fir", 1024);
	renderer->water.model = loadObjModel("water_tile");
	renderer->particle_model = loadObjModel("particles");

	GLfloat skybox_vertices[] = {        
		-SKYBOX_SIZE,  SKYBOX_SIZE, -SKYBOX_SIZE,
	    -SKYBOX_SIZE, -SKYBOX_SIZE, -SKYBOX_SIZE,
	    SKYBOX_SIZE, -SKYBOX_SIZE, -SKYBOX_SIZE,
	     SKYBOX_SIZE, -SKYBOX_SIZE, -SKYBOX_SIZE,
	     SKYBOX_SIZE,  SKYBOX_SIZE, -SKYBOX_SIZE,
	    -SKYBOX_SIZE,  SKYBOX_SIZE, -SKYBOX_SIZE,

	    -SKYBOX_SIZE, -SKYBOX_SIZE,  SKYBOX_SIZE,
	    -SKYBOX_SIZE, -SKYBOX_SIZE, -SKYBOX_SIZE,
	    -SKYBOX_SIZE,  SKYBOX_SIZE, -SKYBOX_SIZE,
	    -SKYBOX_SIZE,  SKYBOX_SIZE, -SKYBOX_SIZE,
	    -SKYBOX_SIZE,  SKYBOX_SIZE,  SKYBOX_SIZE,
	    -SKYBOX_SIZE, -SKYBOX_SIZE,  SKYBOX_SIZE,

	     SKYBOX_SIZE, -SKYBOX_SIZE, -SKYBOX_SIZE,
	     SKYBOX_SIZE, -SKYBOX_SIZE,  SKYBOX_SIZE,
	     SKYBOX_SIZE,  SKYBOX_SIZE,  SKYBOX_SIZE,
	     SKYBOX_SIZE,  SKYBOX_SIZE,  SKYBOX_SIZE,
	     SKYBOX_SIZE,  SKYBOX_SIZE, -SKYBOX_SIZE,
	     SKYBOX_SIZE, -SKYBOX_SIZE, -SKYBOX_SIZE,

	    -SKYBOX_SIZE, -SKYBOX_SIZE,  SKYBOX_SIZE,
	    -SKYBOX_SIZE,  SKYBOX_SIZE,  SKYBOX_SIZE,
	     SKYBOX_SIZE,  SKYBOX_SIZE,  SKYBOX_SIZE,
	     SKYBOX_SIZE,  SKYBOX_SIZE,  SKYBOX_SIZE,
	     SKYBOX_SIZE, -SKYBOX_SIZE,  SKYBOX_SIZE,
	    -SKYBOX_SIZE, -SKYBOX_SIZE,  SKYBOX_SIZE,

	    -SKYBOX_SIZE,  SKYBOX_SIZE, -SKYBOX_SIZE,
	     SKYBOX_SIZE,  SKYBOX_SIZE, -SKYBOX_SIZE,
	     SKYBOX_SIZE,  SKYBOX_SIZE,  SKYBOX_SIZE,
	     SKYBOX_SIZE,  SKYBOX_SIZE,  SKYBOX_SIZE,
	    -SKYBOX_SIZE,  SKYBOX_SIZE,  SKYBOX_SIZE,
	    -SKYBOX_SIZE,  SKYBOX_SIZE, -SKYBOX_SIZE,

	    -SKYBOX_SIZE, -SKYBOX_SIZE, -SKYBOX_SIZE,
	    -SKYBOX_SIZE, -SKYBOX_SIZE,  SKYBOX_SIZE,
	     SKYBOX_SIZE, -SKYBOX_SIZE, -SKYBOX_SIZE,
	     SKYBOX_SIZE, -SKYBOX_SIZE, -SKYBOX_SIZE,
	    -SKYBOX_SIZE, -SKYBOX_SIZE,  SKYBOX_SIZE,
	     SKYBOX_SIZE, -SKYBOX_SIZE,  SKYBOX_SIZE
	};
	renderer->sky.model = loadModelFromVertices(skybox_vertices, sizeof(skybox_vertices) / sizeof(GLfloat));
}

/**
**	Model renderer factory, the model has to be bind, drawn and then unbind
**	Only one model can be binding at once
*/

t_model	g_bind_model[TOSS_MODEL_MAX];

void	bindModel(unsigned dst, t_model model)
{
	g_bind_model[dst] = model;
	glBindVertexArray(model.vao_id);
	glEnableVertexAttribArray(ATTR_POS);
	glEnableVertexAttribArray(ATTR_TEXTURE);
	glEnableVertexAttribArray(ATTR_NORMALS);
}

void	unbindModel(unsigned dst)
{
	glDisableVertexAttribArray(ATTR_POS);
	glDisableVertexAttribArray(ATTR_TEXTURE);
	glDisableVertexAttribArray(ATTR_NORMALS);
	glBindVertexArray(0);
	ft_memset(g_bind_model + dst, 0, sizeof(t_model));
}

void	drawModel(unsigned dst)
{
	glDrawElements(GL_TRIANGLES, g_bind_model[dst].vertex_count, GL_UNSIGNED_INT, NULL);
}
