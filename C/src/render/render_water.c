/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   render_water.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/10 16:37:59 by rpereira          #+#    #+#             */
/*   Updated: 2015/05/13 20:11:20 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

static void	prepareWaterRendering(t_water_tile *tile, t_renderer *renderer)
{
	float		m[16];
	t_program	*program;

	program = renderer->program + PROGRAM_WATER;
	matrix_identity(m);
	matrix_translate(m, tile->pos.x, tile->pos.y, tile->pos.z);
	matrix_rotate(m, new_point3(1, 0, 0), 0);
	matrix_rotate(m, new_point3(0, 1, 0), 0);
	matrix_rotate(m, new_point3(0, 0, 1), 0);
	matrix_scale(m, tile->size.x, 0, tile->size.y);
	load_uniform_matrix(program->transf_matrix, m);
}

static bool	shouldRenderWaterTile(t_camera camera, t_water_tile *tile)
{
	t_point3	pos;
	t_point3	vec;
	double 		dot;

	pos = new_point3(tile->pos.x + tile->size.x / 2, tile->pos.y, tile->pos.z + tile->size.y / 2);
	vec = vec_normalize(vec_sub(camera.pos, pos));
	dot = vec_dot_product(vec, camera.look_vec);
	return (dot > 0);
}

static void	renderWaterTile(t_water_tile *tile, t_renderer *renderer)
{
	if (tile->loaded && shouldRenderWaterTile(renderer->camera, tile))
	{
		prepareWaterRendering(tile, renderer);
		drawModel(TOSS_MODEL0);
	}
}

void		renderWaterTiles(t_world *world, t_renderer *renderer)
{
	t_program	*program;

	program = renderer->program + PROGRAM_WATER;
	glUseProgram(program->id);
	load_uniform_matrix(program->view_matrix, renderer->camera.view_matrix);
	load_uniform_vec(program->light_position, renderer->sky.sun.pos);
	load_uniform_vec(program->light_color, renderer->sky.sun.color);
	load_uniform_weather(world->weather, program);
	bindModel(TOSS_MODEL0, renderer->water.model);
	htab_iter(world->water_tiles, (t_iter_function)renderWaterTile, renderer);
	unbindModel(TOSS_MODEL0);
	glUseProgram(0);
}
