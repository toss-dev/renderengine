/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   render_entity.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/10 16:33:11 by rpereira          #+#    #+#             */
/*   Updated: 2015/06/02 18:57:35 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

static void	prepareEntityInstance(t_program program, t_entity *entity)
{
	static float	m[16];

	matrix_identity(m);
	matrix_translate(m, entity->pos.x, entity->pos.y, entity->pos.z);
	matrix_rotate(m, new_point3(1, 0, 0), entity->rot.x);
	matrix_rotate(m, new_point3(0, 1, 0), entity->rot.y);
	matrix_rotate(m, new_point3(0, 0, 1), entity->rot.z);
	matrix_scale(m, entity->scale.x, entity->scale.y, entity->scale.z);
	load_uniform_matrix(program.transf_matrix, m);
}

static void renderEntity(t_entity *entity, t_renderer *renderer)
{
	prepareEntityInstance(renderer->program[PROGRAM_MODEL], entity);
	glBindTexture(GL_TEXTURE_2D, renderer->textures[entity->texture_id]);
	drawModel(TOSS_MODEL0);
}

static void	renderBatch(t_batch *batch, t_renderer *renderer)
{
	bindModel(TOSS_MODEL0, batch->model);
	array_list_iter(batch->entities, (t_iter_array_function)renderEntity, renderer);
	unbindModel(TOSS_MODEL0);
}

static bool	shouldRenderEntity(t_camera camera, t_entity *entity)
{
	t_point3	vec;
	double 		dot;
	double 		dist;

	vec = vec_normalize(vec_sub(camera.pos, entity->pos));
	dot = vec_dot_product(vec, camera.look_vec);
	dist = point3_dist(camera.pos, entity->pos);
	return (dot > 0 && dist < CHUNK_SIZE * 8);
}

void		addEntityRendering(t_entity *entity, t_renderer *renderer)
{
	if (shouldRenderEntity(renderer->camera, entity))
	{
		array_list_add(&(renderer->model_batchs[entity->model_id].entities), entity);
	}
}

/**
**	TODO: entities has to be added to their batch array list before each rendering
*/
void		renderEntities(t_world *world, t_renderer *renderer)
{
	t_program	*program;
	int 		i;

	program = renderer->program + PROGRAM_MODEL;
	glUseProgram(program->id);
	glActiveTexture(GL_TEXTURE0);
	load_uniform_matrix(program->view_matrix, renderer->camera.view_matrix);
	load_uniform_vec(program->light_position, renderer->sky.sun.pos);
	load_uniform_vec(program->light_color, renderer->sky.sun.color);
	load_uniform_weather(world->weather, program);

	list_iter(&(world->entities), (t_iter_function)addEntityRendering, renderer);

	for (i = 0 ; i < MODEL_MAX ; i++)
	{		
		renderBatch(renderer->model_batchs + i, renderer);
		array_list_clear(&(renderer->model_batchs[i].entities));	//clear list for next frame
	}
	glUseProgram(0);
}
