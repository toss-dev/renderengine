/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   render_particles.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/10 16:33:11 by rpereira          #+#    #+#             */
/*   Updated: 2015/05/28 16:17:49 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

static void	prepareParticleInstance(t_program program, t_particle *particle)
{
	static float	m[16];

	matrix_identity(m);
	matrix_translate(m, particle->pos.x, particle->pos.y, particle->pos.z);
	matrix_rotate(m, new_point3(1, 0, 0), particle->rot.x);
	matrix_rotate(m, new_point3(0, 1, 0), particle->rot.y);
	matrix_rotate(m, new_point3(0, 0, 1), particle->rot.z);
	matrix_scale(m, particle->scale.x, particle->scale.y, particle->scale.z);
	load_uniform_matrix(program.transf_matrix, m);
	load_uniform_vec(program.particle_color, particle->color);
}

static void	prepareParticleTexture(t_renderer *renderer, t_particle *particle)
{
	t_program	*program;

	program = renderer->program + PROGRAM_PARTICLES;
	load_uniform_int(program->texture_atlas_cols, renderer->textures_atlases[particle->texture_id].cols);
	load_uniform_int(program->texture_atlas_lines, renderer->textures_atlases[particle->texture_id].lines);
	load_uniform_int(program->particle_texture_atlas_id, particle->texture_atlas_id);
	glBindTexture(GL_TEXTURE_2D, renderer->textures_atlases[particle->texture_id].id);
}

static bool	shouldParticleBeRendered(t_camera *camera, t_particle *particle)
{
	t_point3	vec;
	double 		dot;

	vec = vec_normalize(vec_sub(camera->pos, particle->pos));
	dot = vec_dot_product(vec, camera->look_vec);
	return (dot > 0);
}

static void	renderParticle(t_particle *particle, t_renderer *renderer)
{
	if (shouldParticleBeRendered(&(renderer->camera), particle))
	{
		prepareParticleInstance(renderer->program[PROGRAM_PARTICLES], particle);
		prepareParticleTexture(renderer, particle);
		drawModel(TOSS_MODEL0);
	}
}

static int 	sortParticles(void const *a, void const *b)
{
	return (((t_particle*)a)->camera_dist < ((t_particle*)b)->camera_dist);
}

void		renderParticles(t_world *world, t_renderer *renderer)
{
	t_program	*program;

	program = renderer->program + PROGRAM_PARTICLES;
	glUseProgram(program->id);
	bindModel(TOSS_MODEL0, renderer->particle_model);
	glActiveTexture(GL_TEXTURE0);
	load_uniform_matrix(program->view_matrix, renderer->camera.view_matrix);
	load_uniform_weather(world->weather, program);
	array_list_sort(renderer->particles, sortParticles);
	array_list_iter(renderer->particles, (t_iter_array_function)renderParticle, renderer);
	unbindModel(TOSS_MODEL0);
	glUseProgram(0);
}
