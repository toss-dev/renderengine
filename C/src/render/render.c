/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   render.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/23 18:33:41 by rpereira          #+#    #+#             */
/*   Updated: 2015/05/26 14:44:15 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

void	initRenderer(t_renderer *renderer)
{
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	//glfwSwapInterval(0);	//disable vsynch
	prepareCosSin();	//libmaths, to use ft_cos, ft_sin ...
	logger_log("Loading renderer...", 1, LOG_FINE, 0);
	{
		logger_log("Loading programs...", 1, LOG_FINE, 0);
		loadPrograms(renderer);
		logger_log("Loading camera...", 1, LOG_FINE, 0);
		loadCamera(renderer);
		logger_log("Loading textures...", 1, LOG_FINE, 0);
		loadTextures(renderer);
		logger_log("Loading sky...", 1, LOG_FINE, 0);
		loadSky(renderer);
		logger_log("Loading water...", 1, LOG_FINE, 0);
		loadWater(&(renderer->water));
		logger_log("Loading particles...", 1, LOG_FINE, 0);
		loadParticles(renderer);
		logger_log("Loading models...", 1, LOG_FINE, 0);
		loadModels(renderer);
	}
	logger_log("Renderer loaded.", 1, LOG_FINE, 0);
}

void 	updateRenderer(t_world *world, t_renderer *renderer)
{
	updateSky(world, renderer);
	updateEffects(world, renderer);
	updateParticles(renderer);
	updateCamera(&(renderer->camera));
}

void	render(t_world *world, t_renderer *renderer, t_window *window)
{
	glClearColor(world->weather.fog_color.x, world->weather.fog_color.y, world->weather.fog_color.z, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	renderSky(world, renderer);
	renderChunks(world, renderer);
	renderWaterTiles(world, renderer);
	renderEntities(world, renderer);
	renderParticles(world, renderer);
	glfwSwapBuffers(window->ptr);
}
