/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   render_chunk.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/10 16:37:59 by rpereira          #+#    #+#             */
/*   Updated: 2015/05/13 20:11:20 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

static void	prepareChunkInstance(t_chunk *chunk, t_renderer *renderer)
{
	float		m[16];
	t_program	*program;

	program = renderer->program + PROGRAM_MODEL;
	matrix_identity(m);
	matrix_translate(m, chunk->posx, 0, chunk->posz);
	matrix_rotate(m, new_point3(1, 0, 0), 0);
	matrix_rotate(m, new_point3(0, 1, 0), 0);
	matrix_rotate(m, new_point3(0, 0, 1), 0);
	matrix_scale(m, 1, 1, 1);
	load_uniform_matrix(program->transf_matrix, m);
}

static bool	shouldChunkBeRendered(t_camera camera, t_chunk *chunk)
{
	float 		max_dist;
	t_point3	world_chunk_pos;
	t_point3	vec;
	double 		dist;
	double 		dot;

	//test chunk render distance
	max_dist = sqrt(CHUNK_RENDER_DISTANCE * CHUNK_RENDER_DISTANCE * 2);
	dist = point2_dist(camera.chunk_idx, new_point2(chunk->indx, chunk->indz));
	if (dist <= 1.4142f)
		return (true);

	// test chunk is inside field of view
	world_chunk_pos = new_point3(chunk->posx + CHUNK_SIZE / 2, chunk->ys[CHUNK_VERTICES / 2][CHUNK_VERTICES / 2], chunk->posz + CHUNK_SIZE / 2);
	vec = vec_normalize(vec_sub(camera.pos, world_chunk_pos));
	dot = vec_dot_product(vec, camera.look_vec);
	return (dot > 0 && dist < max_dist);
}

static void	renderChunk(t_chunk *chunk, t_renderer *renderer)
{
	list_iter(&(chunk->entities), (t_iter_function)addEntityRendering, renderer);

	if (chunk->loaded && shouldChunkBeRendered(renderer->camera, chunk))
	{
		prepareChunkInstance(chunk, renderer);
		bindModel(TOSS_MODEL0, chunk->model);
		drawModel(TOSS_MODEL0);
		unbindModel(TOSS_MODEL0);
	}
}

void		renderChunks(t_world *world, t_renderer *renderer)
{
	t_program	*program;

	program = renderer->program + PROGRAM_MODEL;
	glUseProgram(program->id);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, renderer->textures[TextureChunk]);
	load_uniform_matrix(program->view_matrix, renderer->camera.view_matrix);
	load_uniform_vec(program->light_position, renderer->sky.sun.pos);
	load_uniform_vec(program->light_color, renderer->sky.sun.color);
	load_uniform_weather(world->weather, program);
	htab_iter(world->chunks, (t_iter_function)renderChunk, renderer);
	glUseProgram(0);
}
