/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   water.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/31 15:55:17 by rpereira          #+#    #+#             */
/*   Updated: 2015/05/31 15:55:20 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/**
**	Water frame buffers object
*/

#include "main.h"

void	loadWater(t_water *water)
{
	(void)water;
}

void 	waterDelete(t_water *water)
{
	glDeleteFramebuffers(1, water->fbos);
}