/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   batchs.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/12 17:43:08 by rpereira          #+#    #+#             */
/*   Updated: 2015/06/02 20:13:44 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

void	batch_delete(t_batch *batch)
{
	modelDelete(&(batch->model));
	array_list_delete(&(batch->entities));
}

t_batch	batch_new(char *file, unsigned default_size)
{
	t_batch	batch;

	batch.model = loadObjModel(file);
	batch.entities = array_list_new(default_size, sizeof(t_entity));
	return (batch);
}
