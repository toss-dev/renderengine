/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   render_sky.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/15 18:43:01 by rpereira          #+#    #+#             */
/*   Updated: 2015/05/15 18:43:42 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

static void	load_view_matrix(t_renderer *renderer, t_program *program)
{
	float 	m[16];

	memcpy(m, renderer->camera.view_matrix, sizeof(m));
	m[3] = 0, m[7] = 0, m[11] = 0; //make it independant from camera positionm will always be rendered in screen
	matrix_rotate(m, new_point3(1, 0, 0), renderer->sky.rot.x * M_PI * 2);
	matrix_rotate(m, new_point3(0, 1, 0), renderer->sky.rot.y * M_PI * 2);
	matrix_rotate(m, new_point3(0, 0, 1), renderer->sky.rot.z * M_PI * 2);
	load_uniform_matrix(program->view_matrix, m);
}

static void prepare_sky_uniform(t_world *world, t_renderer *renderer, t_program *program)
{
	load_view_matrix(renderer, program);
	load_uniform_vec(program->fog_color, world->weather.fog_color);
	load_uniform_float(program->day_factor, get_skybox_texture_factor(world));
	load_uniform_float(program->day_ratio, world->day_ratio);
	load_uniform_vec(program->sun_position, renderer->sky.sun.skybox_pos);
	load_uniform_vec(program->sun_color, renderer->sky.sun.color);
}

static void	render_skybox(t_world *world, t_renderer *renderer)
{
	t_program	*program;

	program = renderer->program + PROGRAM_SKYBOX;
	glUseProgram(program->id);
	{
		prepare_sky_uniform(world, renderer, program);
		glBindVertexArray(renderer->sky.model.vao_id);
		glEnableVertexAttribArray(ATTR_POS);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_CUBE_MAP, renderer->sky.cubemap_day);
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_CUBE_MAP, renderer->sky.cubemap_night);
		glDrawArrays(GL_TRIANGLES, 0, renderer->sky.model.vertex_count);
		glDisableVertexAttribArray(ATTR_POS);
		glBindVertexArray(0);
	}
	glUseProgram(0);

}

void		renderSky(t_world *world, t_renderer *renderer)
{
	glDepthMask(GL_FALSE);
	{
		render_skybox(world, renderer);
	}
	glDepthMask(GL_TRUE);
}