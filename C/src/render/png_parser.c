/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   png_parser.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/26 13:38:46 by rpereira          #+#    #+#             */
/*   Updated: 2015/05/26 13:39:11 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

int	load_png_file(char const *file, t_texture *texture)
{
	unsigned	error;
	char		buffer[1024];
	
	texture->internalformat = GL_RGBA;
	texture->format = GL_RGBA;
	error = lodepng_decode32_file(&(texture->pixels),
									&(texture->width),
									&(texture->height),
									file);
	if (error)
	{
		sprintf(buffer, "error while loading png file %u: %s\n", error, lodepng_error_text(error));
		logger_log(buffer, 2, LOG_ERROR, 0);
		return (-1);
	}
	return (1);
}
