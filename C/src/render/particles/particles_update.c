/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   particles.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/12 17:43:08 by rpereira          #+#    #+#             */
/*   Updated: 2015/05/28 15:16:35 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

static void	updateMove(t_particle *particle)
{
	particle->pos = vec_add(particle->pos, particle->pos_vec);
	if (particle->timer % 10 == 0)
	{
		particle->texture_atlas_id++;
	}
	particle->health--;
}

static void	updateRain(t_particle *particle)
{
	particle->health = particle->pos.y > CHUNK_BASE_HEIGHT;
	particle->pos.y -= 4;
	if (particle->timer % 2 == 0)
	{
		particle->texture_atlas_id++;
	}
}

static void	updateMagic(t_particle *particle)
{
	if (particle->timer % 2 == 0)
		particle->texture_atlas_id++;
	if (particle->timer % 60 == 0)
	{
		particle->pos_vec.x = rand_float_in_range(-1, 1);
		particle->pos_vec.y = rand_float_in_range(-1, 1);
		particle->pos_vec.z = rand_float_in_range(-1, 1);
		particle->pos_vec = vec_normalize(particle->pos_vec);
	}
	particle->pos = vec_add(particle->pos, vec_multiply(particle->pos_vec, 2));
	particle->health--;
}

static int updateParticle(t_particle *particle, t_renderer *renderer)
{
	static t_function	f_update[PARTICLES_MAX_TYPE] = {
		updateRain,
		updateMove,
		updateMagic
	};
	particle->camera_dist = point3_dist(renderer->camera.pos, particle->pos);
	particle->timer++;
	if (f_update[particle->id] != NULL)
	{
		f_update[particle->id](particle);
	}
	return (particle->health <= 0);
}

void		updateParticles(t_renderer *renderer)
{
	array_list_iter_remove_if(&(renderer->particles), (t_iter_array_function)updateParticle, renderer);
}