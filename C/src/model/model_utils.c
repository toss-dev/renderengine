/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   model_utils.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/08 10:23:25 by rpereira          #+#    #+#             */
/*   Updated: 2015/05/13 16:06:18 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

void	modelDelete(t_model *model)
{
	if (model == NULL)
	{
		logger_log("Trying to delete a NULL Model", 1, LOG_WARNING, 0);
		return ;
	}
	glDeleteBuffers(1, model->vbos + ATTR_POS);
	glDeleteBuffers(1, model->vbos + ATTR_INDICES);
	glDeleteBuffers(1, model->vbos + ATTR_NORMALS);
	glDeleteVertexArrays(1, &(model->vao_id));
}

void	dump_model_data(t_model_data *data)
{
	GLuint	i;	

	printf("Vertices:\n");
	for (i = 0; i < data->vertices_count; i++)
	{
		printf("x:%f y:%f z:%f\n", data->vertices[i * 3], data->vertices[i * 3 + 1], data->vertices[i * 3 + 2]);
	}

	printf("Indices:\n");
	for (i = 0; i < data->triangles_count; i++)
	{
		printf("%d %d %d\n", data->indices[i * 3], data->indices[i * 3 + 1], data->indices[i * 3 + 2]);
	}
}
