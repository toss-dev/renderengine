/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   model_parser_to_data.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/08 10:47:38 by rpereira          #+#    #+#             */
/*   Updated: 2015/05/15 14:57:09 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

static void	handle_indices(t_model_data *data, GLfloat *normals,
			GLfloat *uvs, GLuint *all_indices, size_t indices_index)
{
	size_t	vertex_index;
	size_t	uv_addr;
	size_t	normal_addr;

	vertex_index = all_indices[0];
	data->indices[indices_index] = vertex_index;
	if (uvs)
	{
		uv_addr = all_indices[1] * 2;
		if (uv_addr + 1 < data->vertices_count * 2)
		{
			data->uvs[vertex_index * 2 + 0] = uvs[uv_addr + 0];
			data->uvs[vertex_index * 2 + 1] = uvs[uv_addr + 1];
		}
	}
	if (normals)
	{
		normal_addr = all_indices[2] * 3;
		if (normal_addr + 2 < data->vertices_count * 3)
		{
			data->normals[vertex_index * 3 + 0] = normals[normal_addr + 0];
			data->normals[vertex_index * 3 + 1] = normals[normal_addr + 1];
			data->normals[vertex_index * 3 + 2] = normals[normal_addr + 2];
		}
	}
}

static void	load_vertex_indices(t_model_data *data, t_obj_parser *parser, GLuint *all_indices)
{
	GLfloat	*normals;
	GLfloat	*uvs;
	size_t	index;
	size_t	indices_index;
	size_t	i;
	
	normals = list_to_array(parser->normals, parser->normals_count * 3 * sizeof(GLfloat));
	uvs = list_to_array(parser->uvs, parser->uvs_count * 2 * sizeof(GLfloat));
	index = 0;
	indices_index = 0;
	for (i = 0; i < data->triangles_count; i++)
	{
		handle_indices(data, normals, uvs, all_indices + index + 0, indices_index + 0);
		handle_indices(data, normals, uvs, all_indices + index + 3, indices_index + 1);
		handle_indices(data, normals, uvs, all_indices + index + 6, indices_index + 2);
		indices_index += 3;
		index += 9;
	}
	free(normals);
	free(uvs);
}

static void	prepare_model_data(t_obj_parser *parser, t_model_data *data)
{
	data->vertices_count = parser->vertices_count;
	data->uvs_count = parser->uvs_count;
	data->normals_count = parser->normals_count;
	data->triangles_count = parser->triangles_count;

	data->vertices = list_to_array(parser->vertices, data->vertices_count * 3 * sizeof(GLfloat));
	data->normals = (GLfloat*)malloc(data->vertices_count * 3 * sizeof(GLfloat));
	data->uvs = (GLfloat*)malloc(data->vertices_count * 2 * sizeof(GLfloat));
	data->indices = (GLuint*)malloc(sizeof(GLuint) * data->triangles_count * 3);
}

void		objParserToModelData(t_obj_parser *parser, t_model_data *data)
{
	GLuint	*all_indices;

	prepare_model_data(parser, data);
	
	all_indices = list_to_array(parser->indices, data->triangles_count * 9 * sizeof(GLint));
	load_vertex_indices(data, parser, all_indices); 
	free(all_indices);
}
