/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   model_loader.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/06 18:01:02 by rpereira          #+#    #+#             */
/*   Updated: 2015/05/14 16:05:27 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

static void	free_parser(t_obj_parser *parser)
{
	list_delete(&(parser->indices), free);
	list_delete(&(parser->vertices), free);
	list_delete(&(parser->uvs), free);
	list_delete(&(parser->normals), free);
}

static void	free_model_data(t_model_data *data)
{
	free(data->vertices);
	free(data->indices);
	free(data->uvs);
	free(data->normals);
}

static bool	load_model_data_from_obj(t_model_data *model_data, char *fileobj, char *filetoss)
{
	t_obj_parser	parser;
	char			buffer[128];
	int 			fd;

	if (parseObjFile(fileobj, &parser) == -1)
	{
		sprintf(buffer, "Couldnt parser obj file: %s", fileobj);
		logger_log(buffer, 2, LOG_ERROR, 0);
		return (false);
	}
	objParserToModelData(&parser, model_data);
	free_parser(&parser);
	if ((fd = open(filetoss, O_CREAT | O_RDWR | O_TRUNC, 0644)) == -1)
		return (true);
	write(fd, model_data, sizeof(t_model_data));
	write(fd, model_data->vertices, sizeof(GLfloat) * model_data->vertices_count * 3);
	write(fd, model_data->uvs, sizeof(GLfloat) * model_data->vertices_count * 2);
	write(fd, model_data->normals, sizeof(GLfloat) * model_data->vertices_count * 3);
	write(fd, model_data->indices, sizeof(GLuint) * model_data->triangles_count * 3);
	close(fd);
	return (true);
}

static bool	load_model_data_from_toss(t_model_data *model_data, char *filetoss)
{
	int	fd;

	return (false);

	if ((fd = open(filetoss, O_RDONLY)) == -1)
	{
		return (false);
	}
	read(fd, model_data, sizeof(t_model_data));
	model_data->vertices = (GLfloat*)malloc(sizeof(GLfloat) * model_data->vertices_count * 3);
	model_data->uvs = (GLfloat*)malloc(sizeof(GLfloat) * model_data->vertices_count * 2);
	model_data->normals = (GLfloat*)malloc(sizeof(GLfloat) * model_data->vertices_count * 3);
	model_data->indices = (GLuint*)malloc(sizeof(GLfloat) * model_data->triangles_count * 3);
	read(fd, model_data->vertices, sizeof(GLfloat) * model_data->vertices_count * 3);
	read(fd, model_data->uvs, sizeof(GLfloat) * model_data->vertices_count * 2);
	read(fd, model_data->normals, sizeof(GLfloat) * model_data->vertices_count * 3);
	read(fd, model_data->indices, sizeof(GLuint) * model_data->triangles_count * 3);

	close(fd);
	return (true);
}

t_model		loadObjModel(char const *file)
{
	t_model			model;
	t_model_data	model_data;
	char			fileobj[512];
	char			filetoss[512];
	char			buffer[1024];

	ft_memset(&model, 0, sizeof(t_model));
	sprintf(fileobj, "../assets/obj/%s.obj", file);
	sprintf(filetoss, "../assets/obj/%s.toss", file);
	if (load_model_data_from_toss(&model_data, filetoss) == false)
	{
		if (load_model_data_from_obj(&model_data, fileobj, filetoss) == false)
			return (model);
		sprintf(buffer, "Loading model: %s", fileobj);
	}
	else
	{
		sprintf(buffer, "Loading model: %s", filetoss);
	}
	logger_log(buffer, 1, LOG_FINE, 3);
	model = loadModel(&model_data);
	free_model_data(&model_data);
	logger_log("Done", 1, LOG_FINE, 2);
	return (model);
}
