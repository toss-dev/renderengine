/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   obj_parser.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/06 18:06:33 by rpereira          #+#    #+#             */
/*   Updated: 2015/05/14 15:33:14 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

static void	show_err(char const *err, int c_line)
{
	char	buffer[512];

	sprintf(buffer, "%s : at line %d", err, c_line);
	logger_log(buffer, 2, LOG_WARNING, 0);
}


static void	add_vertex(t_obj_parser *parser, char const *data)
{
	static GLfloat	v[3];
	int				r;

	r = sscanf(data, "%f %f %f %f", v, v + 1, v + 2, v + 3);
	if (r != 3)
	{
		show_err("Wrong vertex at line: ", parser->c_line);
		return ;
	}
	list_push(&(parser->vertices), v, sizeof(GLfloat) * 3);
	parser->vertices_count++;
}

static void	process_indices(GLuint inds[9], char **indices)
{
	if (inds[0])
	{
		inds[0] = ft_atoi(indices[0]) - 1;
		if (indices[1])
		{
			inds[1] = ft_atoi(indices[1]) - 1;
			if (indices[2])
			{
				inds[2] = ft_atoi(indices[2]) - 1;
			}
			else
			{
				inds[2] = 0;
			}
		}
		else
		{
			inds[1] = 0;
		}
	}
	else
	{
		inds[0] = 0;
	}
}

static void	add_indices(t_obj_parser *parser, char const *data)
{
	GLuint	inds[9];
	char	**split;
	int 	indices_count;
	char	***indices;
	int 	i;

	split = ft_strsplit_whitespaces(data);
	indices_count = ft_strsplit_size(split);
	indices = (char***)malloc(sizeof(char**) * indices_count);

	for (i = 0 ; i < indices_count ; i++)
	{
		indices[i] = ft_strsplit(split[i], '/');
	}

	for (i = 0 ; split[i + 2] ; i++)
	{
		process_indices(inds + 0, indices[0]);
		process_indices(inds + 3, indices[i + 1]);
		process_indices(inds + 6, indices[i + 2]);
		list_push(&(parser->indices), inds, sizeof(inds));
		parser->triangles_count++;
	}
	
	for (i = 0 ; i < indices_count ; i++)
	{
		ft_strsplit_free(indices[i]);
	}
	free(indices);
	ft_strsplit_free(split);
}

static void	add_uv(t_obj_parser *parser, char const *line)
{
	GLfloat	uv[2];
	int		r;

	r = sscanf(line, "%f %f", uv + 0, uv + 1);
	if (r != 2)
	{
		show_err("Wrong vertex texture uv at line: ", parser->c_line);
		return ;
	}
	uv[1] = 1 - uv[1];
	list_push(&(parser->uvs), uv, sizeof(uv));
	parser->uvs_count++;
}

static void	add_normal(t_obj_parser *parser, char const *line)
{
	GLfloat	n[3];
	int		r;

	r = sscanf(line, "%f %f %f %f", n, n + 1, n + 2, n + 3);
	if (r != 3)
	{
		show_err("Wrong normal at line: ", parser->c_line);
		return ;
	}
	list_push(&(parser->normals), n, sizeof(GLfloat) * 3);
	parser->normals_count++;
}

static void	parse_obj_line(t_obj_parser *parser, char const *line)
{
	if (line == NULL || *line == 0)
		return ;
	if (ft_strncmp(line, "v ", 2) == 0)
		add_vertex(parser, line + 2);
	else if (ft_strncmp(line, "f ", 2) == 0)
		add_indices(parser, line + 2);
	else if (ft_strncmp(line, "vn ", 3) == 0)
		add_normal(parser, line + 3);
	else if (ft_strncmp(line, "vt ", 3) == 0)
		add_uv(parser, line + 3);
}

static void	init_parser(t_obj_parser *parser)
{
	parser->vertices = list_new();
	parser->indices = list_new();
	parser->uvs = list_new();
	parser->normals = list_new();
	parser->vertices_count = 0;
	parser->triangles_count = 0;
	parser->uvs_count = 0;
	parser->normals_count = 0;
	parser->c_line = 0;
}

int			parseObjFile(char const *file, t_obj_parser *parser)
{
	FILE	*f;
	char	line[256];
	char	*trim;
	double	t;

	t = glfwGetTime();
	if ((f = fopen(file, "r")) == NULL)
		return (-1);
	init_parser(parser);
	while (fgets(line, sizeof(line), f) != NULL)
	{
		trim = ft_strtrim(line);
		parse_obj_line(parser, trim);
		free(trim);
		parser->c_line = parser->c_line + 1;
	}
	fclose(f);
	printf("took: %f\n", glfwGetTime() - t);
	return (1);
}
