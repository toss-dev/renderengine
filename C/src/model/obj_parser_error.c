/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   obj_parser_error.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/10 14:02:40 by rpereira          #+#    #+#             */
/*   Updated: 2015/05/10 14:04:12 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

void	show_err(char const *err, int c_line)
{
	char	*str;
	char	*tmp;

	tmp = ft_itoa(c_line);
	str = ft_strjoin(err, tmp);
	free(tmp);
	logger_log(str, 2, LOG_WARNING, 0);
	free(str);
}
