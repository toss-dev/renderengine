/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   model.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/04 09:50:46 by rpereira          #+#    #+#             */
/*   Updated: 2015/05/13 19:50:37 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

static void			store_floats(t_model *model, int idx, GLfloat *data, int count, int vertex_size)
{
	size_t	size;

	size = count * sizeof(GLfloat);
	glGenBuffers(1, model->vbos + idx);
	glBindBuffer(GL_ARRAY_BUFFER, model->vbos[idx]);
	glBufferData(GL_ARRAY_BUFFER, size, data, GL_STATIC_DRAW);
	glVertexAttribPointer(idx, vertex_size, GL_FLOAT, GL_FALSE, 0, NULL);
}

static void			store_indices(t_model *model, GLuint indice[], GLuint count)
{
	size_t	size;

	size = count * sizeof(GLuint);
	glGenBuffers(1, model->vbos + ATTR_INDICES);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, model->vbos[ATTR_INDICES]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, size, indice, GL_STATIC_DRAW);
}

t_model				loadModel(t_model_data *data)
{
	t_model 	model;

	model.vertex_count = data->triangles_count * 3;
	glGenVertexArrays(1, &(model.vao_id));
	glBindVertexArray(model.vao_id);
	store_floats(&model, ATTR_POS, data->vertices, data->vertices_count * 3, 3);
	store_floats(&model, ATTR_TEXTURE, data->uvs, data->vertices_count * 2, 2);
	store_floats(&model, ATTR_NORMALS, data->normals, data->vertices_count * 3, 3);
	store_indices(&model, data->indices, data->triangles_count * 3);
	glBindVertexArray(0);
	return (model);
}

t_model 			loadModelFromVertices(GLfloat vertices[], size_t vertices_count)
{
	t_model	model;

	model.vertex_count = vertices_count / 3;
	glGenVertexArrays(1, &(model.vao_id));
	glBindVertexArray(model.vao_id);
	store_floats(&model, ATTR_POS, vertices, vertices_count, 3);
	glBindVertexArray(0);
	return (model);
}