/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   world_time.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/20 11:36:18 by rpereira          #+#    #+#             */
/*   Updated: 2015/05/21 15:39:29 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

/** this ratio is the amount between [0.0 ; 1.0] of day texture usage */
float	get_skybox_texture_factor(t_world *world)
{
	if (world->day_ratio >= NIGHT_END && world->day_ratio < DAY_START / 2)
	{
		return ((world->day_ratio - NIGHT_END) / (DAY_START / 2 - NIGHT_END));
	}
	if (world->day_ratio >= DAY_END && world->day_ratio < NIGHT_START)
	{
		return (1 - (world->day_ratio - DAY_END) / (NIGHT_START - DAY_END));
	}
	return (world->day_ratio < NIGHT_START);
}
