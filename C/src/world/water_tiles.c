/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   water.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/28 15:07:20 by rpereira          #+#    #+#             */
/*   Updated: 2015/05/28 15:27:24 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

t_water_tile	new_water_tile(t_point3 pos, t_point2 size)
{
	t_water_tile	tile;

	tile.pos = pos;
	tile.size = size;
	tile.loaded = true;
	return (tile);
}

static void		generateWaterTileKey(t_water_tile tile, char buffer[64])
{
	sprintf(buffer, "%.2f%.2f%.2f%.2f%.2f",
		tile.pos.x, tile.pos.y, tile.pos.z,
		tile.size.x, tile.size.y);
	printf("key: %s\n", buffer);
}

void			createWaterTile(t_htab tiles, t_water_tile tile)
{
	char	buffer[64];

	generateWaterTileKey(tile, buffer);
	htab_insert(tiles, buffer, &tile, sizeof(t_water_tile));
}