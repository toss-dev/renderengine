/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   chunk.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/10 18:15:45 by rpereira          #+#    #+#             */
/*   Updated: 2015/06/02 13:30:32 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

# define CHUNK_INDICES_COUNT (6 * (CHUNK_VERTICES - 1) * (CHUNK_VERTICES - 1))

static t_point3	calculate_normal(t_chunk *chunk, int x, int z)
{
	t_point3	normal;
	float		right;
	float		left;
	float		up;
	float		down;

	left = (x - 1 >= 0) ? chunk->ys[x - 1][z] : chunk->ys[x][z] + (chunk->ys[x][z] - chunk->ys[x + 1][z]);
	right = (x + 1 < CHUNK_VERTICES) ? chunk->ys[x + 1][z] : chunk->ys[x][z] + (chunk->ys[x][z] - chunk->ys[x - 1][z]);
	down = (z - 1 >= 0) ? chunk->ys[x][z - 1] : chunk->ys[x][z] + (chunk->ys[x][z] - chunk->ys[x][z + 1]);
	up = (z + 1 < CHUNK_VERTICES) ? chunk->ys[x][z + 1] : chunk->ys[x][z] + (chunk->ys[x][z] - chunk->ys[x][z - 1]);
	normal = new_point3(left - right, 2.0f, down - up);
	return (vec_normalize(normal));
}

static void	generate_chunk_model_data(t_chunk *chunk, t_model_data *data)
{
	t_point3	normal;
	int			location;
	int		x;
	int		z;

	for (z = 0; z < CHUNK_VERTICES; z++)
	{
		for (x = 0; x < CHUNK_VERTICES; x++)
		{
			location = x * CHUNK_VERTICES * 3 + z * 3;
			data->vertices[location + 0] = x / (float)(CHUNK_VERTICES - 1) * CHUNK_SIZE;
			data->vertices[location + 1] = chunk->ys[x][z];
			data->vertices[location + 2] = z / (float)(CHUNK_VERTICES - 1) * CHUNK_SIZE;
			normal = calculate_normal(chunk, x, z);
			data->normals[location + 0] = normal.x;
			data->normals[location + 1] = normal.y;
			data->normals[location + 2] = normal.z;
			location = x * CHUNK_VERTICES * 2 + z * 2;
			data->uvs[location + 0] = x / (float)CHUNK_VERTICES;
			data->uvs[location + 1] = z / (float)CHUNK_VERTICES;
		}
	}
}

static void	generate_chunk_model_indices(t_model_data *data)
{
	int	inds[4];
	int	idx;
	int	x;
	int	z;

	idx = 0;
	for (x = 0; x < CHUNK_VERTICES - 1; x++)
	{
		for (z = 0; z < CHUNK_VERTICES - 1; z++)
		{
			inds[0] = x * CHUNK_VERTICES + z;
			inds[1] = inds[0] + 1;
			inds[2] = (x + 1) * CHUNK_VERTICES + z;
			inds[3] = inds[2] + 1;
			data->indices[idx + 0] = inds[0];
			data->indices[idx + 1] = inds[2];
			data->indices[idx + 2] = inds[1];
			data->indices[idx + 3] = inds[1];
			data->indices[idx + 4] = inds[2];
			data->indices[idx + 5] = inds[3];
			idx += 6;
		}
	}
}

static void	prepare_model_data(t_model_data *data)
{
	data->vertices = (GLfloat*)calloc(1, sizeof(GLfloat) * CHUNK_VERTICES * CHUNK_VERTICES * 3);
	data->indices = (GLuint*)calloc(1, sizeof(GLuint) * CHUNK_INDICES_COUNT * 3);
	data->uvs = (GLfloat*)calloc(1, sizeof(GLfloat) * CHUNK_VERTICES * CHUNK_VERTICES * 2);
	data->normals = (GLfloat*)calloc(1, sizeof(GLfloat) * CHUNK_VERTICES * CHUNK_VERTICES * 3);
	data->vertices_count = CHUNK_VERTICES * CHUNK_VERTICES;
	data->triangles_count = CHUNK_INDICES_COUNT;
	data->uvs_count = CHUNK_VERTICES * CHUNK_VERTICES;
	data->normals_count = CHUNK_VERTICES * CHUNK_VERTICES;
}

static void	end_model_data(t_model_data *data)
{
	free(data->vertices);
	free(data->indices);
	free(data->uvs);
	free(data->normals);
}

void		generateChunk(t_chunk *chunk)
{
	t_model_data	data;

	prepare_model_data(&data);
	{
		generate_chunk_model_data(chunk, &data);
		generate_chunk_model_indices(&data);
		chunk->model = loadModel(&data);
	}
	end_model_data(&data);
	chunk->generated = true;
}
