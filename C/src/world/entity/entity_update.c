/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   entity_update.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/11 20:54:49 by rpereira          #+#    #+#             */
/*   Updated: 2015/05/15 15:37:46 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

static void	updateEntityMove(t_entity *entity)
{
	entity->rot = vec_add(entity->rot, entity->rot_vec);
	entity->pos = vec_add(entity->pos, entity->pos_vec);
	entity->scale = vec_add(entity->scale, entity->scale_vec);
}

static void	updateGravity(t_world *world, t_entity *entity)
{	
	float	height;

	height = getHeightAt(world, entity->pos.x, entity->pos.z);
	if (entity->pos.y > height)
	{
		entity->pos_vec.y = (entity->pos_vec.y > -1) ? -1 : (entity->pos_vec.y * 1.05f);
	}
	else
	{
		entity->pos.y = height;
	}
}

static void	updateStatic(t_entity *entity, t_world *world)
{
	updateGravity(world, entity);
	updateEntityMove(entity);
}

int			updateEntity(t_entity *entity, t_world *world)
{
	static t_function	f_update[ENTITY_MAX_TYPE] = {
		updateStatic
	};

	f_update[entity->id](entity, world);
	return (entity->health > 0);
}
