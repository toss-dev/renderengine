/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   entity.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/09 13:47:30 by rpereira          #+#    #+#             */
/*   Updated: 2015/06/02 17:12:20 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

t_entity	new_entity(t_point3 pos, t_point3 scal, t_point3 rot, int id,
			unsigned texture_id, unsigned model_id)
{
	t_entity	entity;

	entity.pos = pos;
	entity.scale = scal;
	entity.rot = rot;
	entity.pos_vec = new_point3(0, 0, 0);
	entity.scale_vec = new_point3(0, 0, 0);
	entity.rot_vec = new_point3(0, 0, 0);
	entity.id = id;
	entity.texture_id = texture_id;
	entity.model_id = model_id;
	entity.health = 10.0f;
	entity.speed = 1;
	return (entity);
}

/**
**	add a static entity to the world, which mean it chunk will never change
**	and this entity can be updated if the chunk is updated
*/
t_entity		*spawnStaticEntity(t_world *world, t_entity entity)
{
	t_chunk	*chunk;

	chunk = getChunkAt(world->chunks, entity.pos.x, entity.pos.z);
	if (chunk == NULL)
	{
		t_pair p = getChunkIndexForPos(entity.pos.x, entity.pos.z);
		chunk = addChunk(world, p.a, p.b);
	}
	return (list_add(&(chunk->entities), &entity, sizeof(t_entity)));
}

/** add an entity that move (can change chunks, which basically mean we need to get it chunk at each update) */
t_entity		*spawnDynamicEntity(t_world *world, t_entity entity)
{
	return (list_add(&(world->entities), &entity, sizeof(t_entity)));
}