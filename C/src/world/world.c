/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   world.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/09 11:42:21 by rpereira          #+#    #+#             */
/*   Updated: 2015/05/14 16:42:06 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

void		updateWorld(t_world *world)
{
	list_iter(&(world->entities), (t_iter_function)updateEntity, world);
	updateWeather(world);
	updateChunks(world);
}

static void prepareWorld(t_world *world)
{
	world->chunks = htab_new(CHUNK_HASH_MAP_SIZE);
	world->entities = list_new();
	world->water_tiles = htab_new(WATER_TILES_HASH_MAP_SIZE);
	world->ticks = NIGHT_END * TPC;
	world->day_ratio = NIGHT_END;
}

void		initWorld(t_world *world)
{
	logger_log("Loading world...", 1, LOG_FINE, 0);
	srand(time(NULL));
	prepareWorld(world);
	initSimplexNoise();
	loadWeather(world);

/*********************************************************************************/

	int 	i;
	int 	j;
	float	x;
	float	y;

	
	//createWaterTile(world->water_tiles, new_water_tile(new_point3(200, CHUNK_ROUGHNESS - 16, 0), new_point2(200, 200)));

	for (i = -5 ; i < 5 ; i++)
	{
		for (j = -5 ; j < 5 ; j++)
		{
			createChunk(world, i, j);
			loadChunkAt(world->chunks, i, j);
		}
	}
	return ;
	for (i = 0 ; i < 256 ; i++)
	{
		for (j = 0 ; j < 256 ; j++)
		{
			x =  (CHUNK_SIZE / 4) * i + rand() % (CHUNK_SIZE / 4);
			y =  (CHUNK_SIZE / 4) * j + rand() % (CHUNK_SIZE / 4);
			spawnStaticEntity(world,
				new_entity(
					new_point3(x, getHeightAt(world, x, y), y),
					new_point3(0.18f, 0.18f, 0.18f),
					new_point3(0, rand(), 0),
					EntityStatic, TextureGrass, ModelTree));
		}
	}
	/*********************************************************************************/
}
