/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   chunk_utils.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/18 12:51:51 by rpereira          #+#    #+#             */
/*   Updated: 2015/05/18 12:51:52 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

void		generateChunkKey(char buff[CHUNK_KEY_SIZE], int x, int z)
{
	sprintf(buff, "%d:%d\n", x, z);
}

t_chunk		*getChunk(t_htab chunks, int indx, int indz)
{
	char	buffer[64];

	generateChunkKey(buffer, indx, indz);
	return (htab_get(chunks, buffer));
}

t_chunk		*getChunkAt(t_htab chunks, float x, float z)
{
	if (x < 0)
		x -= CHUNK_SIZE;
	if (z < 0)
		z -= CHUNK_SIZE;
	return (getChunk(chunks, x / CHUNK_SIZE, z / CHUNK_SIZE));
}

void		loadChunk(t_chunk *chunk)
{
	if (!chunk->loaded)
	{
		chunk->loaded = true;
	}
}

void		unloadChunk(t_chunk *chunk)
{
	if (chunk->loaded)
	{
		chunk->loaded = false;
	}
}

void		loadChunkAt(t_htab htab, int indx, int indz)
{
	t_chunk	*chunk;

	chunk = getChunk(htab, indx, indz);
	if (chunk != NULL)
	{
		chunk->loaded = true;
	}
	else
	{
		logger_log("Trying to load a non existing chunk.", 2, LOG_WARNING, 1);
	}
}

void		unloadChunkAt(t_htab chunks, int indx, int indz)
{
	t_chunk	*chunk;

	chunk = getChunk(chunks, indx, indz);
	if (chunk != NULL)
	{
		chunk->loaded = false;
	}
	else
	{
		logger_log("Trying to unload a non existing chunk.", 2, LOG_WARNING, 1);
	}
}

t_pair	getChunkIndexForPos(float posx, float posz)
{
	t_pair	pair;

	if (posx < 0)
	{
		posx -= CHUNK_SIZE;
	}
	if (posz < 0)
	{
		posz -= CHUNK_SIZE;
	}
	pair.a = posx / CHUNK_SIZE;
	pair.b = posz / CHUNK_SIZE;
	return (pair);
}