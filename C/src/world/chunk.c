/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   chunk.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/12 12:31:05 by rpereira          #+#    #+#             */
/*   Updated: 2015/05/13 19:17:38 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

static t_chunk	new_chunk(int x, int z)
{
	t_chunk	chunk;

	chunk.indx = x;
	chunk.indz = z;
	chunk.posx = x * CHUNK_SIZE;
	chunk.posz = z * CHUNK_SIZE;
	chunk.loaded = false;
	chunk.generated = false;
	chunk.entities = list_new();
	memset(chunk.ys, 0, sizeof(chunk.ys));
	return (chunk);
}

void			chunk_delete(t_chunk *chunk)
{
	list_delete(&(chunk->entities), free);
	free(chunk);
}

/**
**	Create / generate chunk at x:z if needed
**/
t_chunk			*createChunk(t_world *world, int x, int z)
{
	t_chunk	*tmp;
	t_chunk	chunk;
	char	buffer[CHUNK_KEY_SIZE];

	tmp = getChunk(world->chunks, x, z);
	if (tmp != NULL)
	{
		if (tmp->generated == false)
		{
			generateSimplexNoise(tmp);
			generateChunk(tmp);
		}
		return (tmp);
	}
	else
	{
		chunk = new_chunk(x, z);
		generateSimplexNoise(&chunk);
		generateChunk(&chunk);
		generateChunkKey(buffer, x, z);
		return (htab_insert(world->chunks, buffer, &chunk, sizeof(t_chunk)));
	}
}

/**
**	Prepare the chunk (IT DOES NOT GENERATE IT) at the given coordinate
*/
t_chunk			*addChunk(t_world *world, int x, int z)
{
	t_chunk	chunk;
	char	buffer[CHUNK_KEY_SIZE];

	chunk = new_chunk(x, z);
	generateChunkKey(buffer, x, z);
	return (htab_insert(world->chunks, buffer, &chunk, sizeof(t_chunk)));
}

static void		updateChunk(t_world *world, t_chunk *chunk)
{
	list_iter(&(chunk->entities), (t_iter_function)updateEntity, world);
}

static int 	unloadChunks(t_chunk *chunk)
{
	float	dist;

	dist = point2_dist(new_point2(chunk->indx, chunk->indz), g_env->renderer.camera.chunk_idx);
	unloadChunk(chunk);
	return (dist > CHUNK_LOAD_DISTANCE && !chunk->loaded);
}
/*
void			updateChunks(t_world *world)
{
	t_camera	camera;
	t_chunk		*chunk;
	int 		indx;
	int 		indz;
	int 		x;
	int 		z;

	camera = g_env->renderer.camera; //c'est degueu ca , faut le changer
	htab_iter_remove_if(world->chunks, (t_iter_function)unloadChunks, NULL);
	for (x = -CHUNK_RENDER_DISTANCE * 2 ; x < CHUNK_RENDER_DISTANCE * 2 ; x++)
	{
		for (z = -CHUNK_RENDER_DISTANCE * 2 ; z < CHUNK_RENDER_DISTANCE * 2 ; z++)
		{
			indx = camera.chunk_idx.x + x;
			indz = camera.chunk_idx.y + z;
			if (point2_dist(new_point2(indx, indz), camera.chunk_idx) < CHUNK_RENDER_DISTANCE)
			{
				chunk = createChunk(world, indx, indz);
				loadChunk(chunk);
				updateChunk(world, chunk);
			}
		}
	}
}
*/

void			updateChunks(t_world *world)
{
	t_camera	camera;
	t_chunk		*chunk;
	int 		angle;
	int 		indx;
	int 		indz;
	int 		rayon;
	int 		pas;

	camera = g_env->renderer.camera; //c'est degueu ca , faut le changer
	htab_iter_remove_if(world->chunks, (t_iter_function)unloadChunks, NULL);
	for (rayon = 1 ; rayon < CHUNK_RENDER_DISTANCE * 2 ; rayon++)
	{
		pas = (180 / 4) / rayon;
		for (angle = 0 ; angle < 360 ; angle += pas)
		{
			indx = (cos(TO_RADIAN(angle)) * rayon + camera.chunk_idx.x);
			indz = (sin(TO_RADIAN(angle)) * rayon + camera.chunk_idx.y);
			chunk = createChunk(world, indx, indz);
			loadChunk(chunk);
			updateChunk(world, chunk);
		}
	}
}
