/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   logger.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/23 11:53:09 by rpereira          #+#    #+#             */
/*   Updated: 2015/05/09 19:38:07 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "logger.h"

static void	print_entry_nbr(int nb, int fd)
{
	if (nb < 10)
	{
		write(1, "0", 1);
	}
	ft_putnbr_fd(nb, fd);
}

static void	print_entry(int const fd, char const *level, char const *color)
{
	time_t		now;
	struct tm	*t;

	ft_putstr(color);
	now = time(0);
	t = localtime(&now);
	write(fd, "[", 1);
	print_entry_nbr(t->tm_hour, fd);
	write(fd, ":", 1);
	print_entry_nbr(t->tm_min, fd);
	write(fd, ":", 1);
	print_entry_nbr(t->tm_sec, fd);
	write(fd, "] [", 3);
	ft_putstr(level);
	write(1, "] ", 2);
	ft_putstr(C_RESET);
}

void		logger_log(char const *str, int fd, int level, int indent)
{
	static char	*str_level[5] = {"INFO", "FINE", "WARNING", "ERROR", "FATAL"};
	static char *color_level[5] = {"", C_GREEN, C_BYELLOW, C_BRED, C_RED};
	static char	*str_indent = "\t\t\t\t\t\t\t\t\t\t";

	if (level < 0 || level >= 5 || indent >= 10 || indent < 0)
	{
		return ;
	}
	print_entry(fd, str_level[level], color_level[level]);
	write(1, str_indent, indent);
	write(fd, str, ft_strlen(str));
	write(fd, "\n", 1);
	if (level == LOG_FATAL)
	{
		ft_putstr_fd("* Fatal error occured, stopping program\n", fd);
		exit(EXIT_FAILURE);
	}
}
