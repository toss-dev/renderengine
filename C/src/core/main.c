/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/23 11:12:52 by rpereira          #+#    #+#             */
/*   Updated: 2015/05/15 15:05:11 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

t_env	*g_env;

int	main(void)
{
	g_env = malloc(sizeof(t_env));
	if (g_env == NULL)
	{
		logger_log("Not enough memory", 2, LOG_FATAL, 0);
	}
	initWindow(&(g_env->window));
	initEvent(&(g_env->window));
	initRenderer(&(g_env->renderer));
	initWorld(&(g_env->world));
	startLoop(g_env);
	exitProperly(g_env);
	return (EXIT_SUCCESS);
}
