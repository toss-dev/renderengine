/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exit.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/09 16:07:00 by rpereira          #+#    #+#             */
/*   Updated: 2015/05/13 21:24:49 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

static void	cleanRenderer(t_renderer *renderer)
{
	int	i;

	sky_delete(&(renderer->sky));
	textures_delete(renderer);
	for (i = 0 ; i < PROGRAM_MAX ; i++)
		program_delete(renderer->program + i);
	array_list_delete(&(renderer->particles));
	modelDelete(&(renderer->particle_model));
	modelDelete(&(renderer->water.model));
	waterDelete(&(renderer->water));
	for (i = 0 ; i < MODEL_MAX ; i++)
		batch_delete(renderer->model_batchs + i);
}

static void	cleanWorld(t_world *world)
{
	htab_delete(&(world->chunks), chunk_delete);
	htab_delete(&(world->water_tiles), free);
}

void		exitProperly(t_env *env)
{
	cleanRenderer(&(env->renderer));
	cleanWorld(&(env->world));
	free(g_env);
	exit(EXIT_SUCCESS);
}
