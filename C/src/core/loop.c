/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   loop.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/23 12:38:06 by rpereira          #+#    #+#             */
/*   Updated: 2015/05/16 20:45:52 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

void		checkGlError(void)
{
	char	*str;
	char	*tmp;
	int		err;

	err = glGetError();
	if (err != 0)
	{
		tmp = ft_itoa(err);
		str = ft_strjoin("OpenGL error occured: ", tmp);
		free(tmp);
		logger_log(str, 1, LOG_WARNING, 0);
		free(str);
	}
}

static void	updateTicks(t_world *world)
{
	static double	last = 0;
	double			current;

	if (world->ticks >= TPC)
	{
		world->ticks = 0;
	}
	else
	{
		current = glfwGetTime();
		if (current - last >= SECOND_PER_TICK)
		{
			world->ticks += (current - last) / SECOND_PER_TICK;
			last = current;
		}
	}
	world->day_ratio = world->ticks / (float)TPC;
}

void		loop(t_env *env)
{
	while (!glfwWindowShouldClose(env->window.ptr))
	{
		updateTicks(&(env->world));
		updateWorld(&(env->world));
		updateRenderer(&(env->world), &(env->renderer));
		render(&(env->world), &(env->renderer), &(env->window));
		glfwPollEvents();
		checkGlError();
	}
}

void		startLoop(t_env *env)
{
	srand(time(NULL));
	logger_log("Starting loop ...", 1, LOG_FINE, 0);
	loop(env);
}
