
#include "libft.h"


int main()
{
	t_array_list	array;
	char			buff[16][16];

	array = array_list_new(1, sizeof(buff[0]));

	ft_strcpy(buff[0], "hello");
	ft_strcpy(buff[1], "les");
	ft_strcpy(buff[2], "gens");
	ft_strcpy(buff[3], "a");
	ft_strcpy(buff[4], "b");
	ft_strcpy(buff[5], "c");
	ft_strcpy(buff[6], "d");
	ft_strcpy(buff[7], "e");
	ft_strcpy(buff[8], "f");
	ft_strcpy(buff[9], "g");
	ft_strcpy(buff[10], "h");
	ft_strcpy(buff[11], "i");
	ft_strcpy(buff[12], "j");
	ft_strcpy(buff[13], "k");
	ft_strcpy(buff[14], "l");
	ft_strcpy(buff[15], "m");

	array_list_add_all(&array, buff, 16);

	array_list_iter(array, (t_iter_array_function)ft_putendl, NULL);
	return (0);
}