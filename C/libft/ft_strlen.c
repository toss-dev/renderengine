/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlen.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/09 18:46:15 by rpereira          #+#    #+#             */
/*   Updated: 2015/05/10 12:45:39 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlen(const char *s)
{
	unsigned long	*x;

	return (strlen(s));
	x = (unsigned long*)s;
	while (1)
	{
		if ((*x & 0xFF) == 0)
			return ((const char*)x - s);
		if ((*x & 0xFF00) == 0)
			return ((const char*)x - s + 1);
		if ((*x & 0xFF0000) == 0)
			return ((const char*)x - s + 2);
		if ((*x & 0xFF000000) == 0)
			return ((const char*)x - s + 3);
		if ((*x & 0xFF00000000) == 0)
			return ((const char*)x - s + 4);
		if ((*x & 0xFF0000000000) == 0)
			return ((const char*)x - s + 5);
		if ((*x & 0xFF000000000000) == 0)
			return ((const char*)x - s + 6);
		if ((*x & 0xFF000000000000) == 0)
			return ((const char*)x - s + 7);
		x++;
	}
	return (0);
}
