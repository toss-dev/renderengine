/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <rpereira@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/03 20:32:19 by rpereira          #+#    #+#             */
/*   Updated: 2015/05/21 17:15:44 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strcpy(char *dest, const char *src)
{
	size_t	len;

	if (dest == NULL || src == NULL)
		return (NULL);
	len = ft_strlen(src);
	ft_memcpy(dest, src, sizeof(char) * len);
	dest[len] = 0;
	return (dest);
}
