/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_base.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/01 16:12:23 by rpereira          #+#    #+#             */
/*   Updated: 2015/05/14 14:45:37 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_putnbr_to_tab(char str[], unsigned long long int n,
			char *charset, unsigned int base)
{
	char	tmp;
	int		i;
	int		nbr_len;

	i = 0;
	nbr_len = 0;
	while (n != 0)
	{
		str[nbr_len] = charset[(n % base)];
		n = n / base;
		nbr_len = nbr_len + 1;
	}
	while (i < nbr_len / 2)
	{
		tmp = str[i];
		str[i] = str[nbr_len - 1 - i];
		str[nbr_len - 1 - i] = tmp;
		i++;
	}
	return (nbr_len);
}

static int	ft_reverse_base_nbr(char str[], unsigned long long int n,
			char *charset, unsigned int base)
{
	int		nbr_len;

	if (n == 0)
	{
		str[0] = '0';
		return (1);
	}
	nbr_len = ft_putnbr_to_tab(str, n, charset, base);
	return (nbr_len);
}

int			ft_putnbr_base(unsigned long int n, char *chars, unsigned int base)
{
	int		nbr_len;
	char	str[(nbr_len = ft_nbrlen_base(n, base))];

	ft_reverse_base_nbr(str, n, chars, base);
	write(1, str, nbr_len);
	return (nbr_len);
}