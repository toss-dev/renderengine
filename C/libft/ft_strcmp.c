/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/07/09 10:19:08 by rpereira          #+#    #+#             */
/*   Updated: 2015/05/25 13:38:45 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_strcmp(const char *s1, const char *s2)
{
	int i;

	return (strcmp(s1, s2));
	i = 0;
	if (s1 == s2)
		return (0);
	if (s1 == NULL)
		return (!(s2 == NULL));
	if (s2 == NULL)
		return (!(s1 == NULL));
	while (s1[i] == s2[i] && s1[i] && s2[i])
	{
		if (s1[i] == '\0')
			return (0);
		i++;
	}
	return ((unsigned char)s1[i] - (unsigned char)s2[i]);
}
