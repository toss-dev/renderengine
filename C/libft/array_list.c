/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   array_list.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/11 10:50:23 by rpereira          #+#    #+#             */
/*   Updated: 2015/06/04 17:40:34 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_array_list	array_list_new(unsigned nb, unsigned elem_size)
{
	t_array_list	array;

	array.data = calloc(nb, elem_size);
	array.capacity = nb;
	array.elem_size = elem_size;
	array.size = 0;
	array.default_capacity = nb;
	return (array);
}

void		array_list_add(t_array_list *array, void *data)
{
	if (array->size == array->capacity)
	{
		array_list_expand(array);
	}
	ft_memcpy(array->data + array->size * array->elem_size, data, array->elem_size);
	array->size++;
}

void		array_list_add_all(t_array_list *array, void *buffer, unsigned nb)
{
	unsigned	array_idx;
	unsigned	copy_nb;
	unsigned	copy_size;

	array_idx = array->size * array->elem_size;
	while (nb)
	{
		copy_nb = MIN(array->capacity - array->size, nb);
		if (copy_nb == 0)
		{
			array_list_expand(array);
			continue ;
		}
		copy_size = copy_nb * array->elem_size;
		ft_memcpy(array->data + array_idx, buffer, copy_size);
		nb -= copy_nb;
		array->size += copy_nb;
		buffer += copy_size;
		array_idx += copy_size;
	}
}

void 		array_list_expand(t_array_list *array)
{
	printf("libft warning: expanding array list is bad for performance! (%u => %u)\n",
		array->capacity, array->capacity + array->default_capacity);
	array->data = realloc(array->data, (array->capacity + array->default_capacity) * array->elem_size);
	array->capacity += array->default_capacity;
}

void		*array_list_get(t_array_list *array, unsigned idx)
{
	return (array->data + idx * array->elem_size);
}

void		array_list_remove(t_array_list *array, int idx)
{
	memmove(array->data + idx * array->elem_size, array->data + (array->size - 1) * array->elem_size, array->elem_size);
	array->size--;
}

void		array_list_clear(t_array_list *array)
{
	array->size = 0;
}

/** this functions assumes the array is defragmented */
void		array_list_iter(t_array_list array, t_iter_array_function f, void *extra)
{
	unsigned	i;
	unsigned	addr;

	i = 0;
	addr = 0;
	while (i < array.size)
	{
		f(array.data + addr, extra, i);
		addr += array.elem_size;
		++i;
	}
}

/** this functions assumes the array is defragmented */
void		array_list_iter_remove_if(t_array_list *array, t_iter_array_function f, void *extra)
{
	unsigned	i;
	unsigned	addr;

	i = 0;
	addr = 0;
	while (i < array->size)
	{
		if (f(array->data + addr, extra, i))
		{
			array_list_remove(array, i);
		}
		else
		{
			addr += array->elem_size;
			++i;
		}
	}
}

void 		array_list_delete(t_array_list *array)
{
	free(array->data);
	array->data = NULL;
	array->size = 0;
	array->capacity = 0;
}

void		array_list_resize(t_array_list *array, unsigned size)
{
	array->data = realloc(array->data, size * array->elem_size);
	array->capacity = size;
}

void		array_list_sort(t_array_list array, int (*cmpf)(void const *, void const *))
{
	qsort(array.data, array.size, array.elem_size, cmpf);
}