/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <rpereira@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/03 11:21:23 by rpereira          #+#    #+#             */
/*   Updated: 2015/06/02 20:06:30 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H

# define ABS(x) ((x) > 0 ? (x) : -(x))
# define MAX(x, y) ((x) > (y) ? (x) : (y))
# define MIN(x, y) ((x) < (y) ? (x) : (y))
# define SIGN(x) ((x) < 0 ? (-1) : (1))

# define BUFF_SIZE 8192

# include <unistd.h>
# include <float.h>
# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <strings.h>
# include <sys/time.h>
# include <stdlib.h>
# include <unistd.h>
# include <fcntl.h>
# include <wchar.h>
# include <stdbool.h>
# include "color.h"
# include "lodepng.h"

typedef void		(*t_function)();
typedef int 		(*t_iter_function) (void *data, void *extra);
typedef int 		(*t_iter_array_function) (void *data, void *extra, unsigned idx);

typedef struct		s_btree
{
	struct s_btree	*left;
	struct s_btree	*right;
	void			*item;
}					t_btree;

typedef struct		s_node
{
	void			*content;
	size_t			content_size;
	struct s_node	*next;
}					t_node;

typedef struct		s_list
{
	t_node	*begin;
	t_node	*end;
	size_t	size;
}					t_list;

typedef struct		s_htab_elem
{
	void	*content;
	size_t	content_size;
	char	*key;
}					t_htab_elem;

typedef struct		s_htab
{
	t_list	*elems;
	size_t	size;
}					t_htab;

enum e_array_list_size
{
	ARRAY_LIST_TINY = 128,
	ARRAY_LIST_SMALL = 1024,
	ARRAY_LIST_MEDIUM = 2048,
	ARRAY_LIST_LARGE = 8096,
	ARRAY_LIST_HUGE = 32384
};

typedef struct		s_array_list
{
	char 		*data;
	unsigned	capacity;
	unsigned	size;
	unsigned	elem_size;
	unsigned 	default_capacity;
}					t_array_list;

typedef struct		s_gnl
{
	int		fd;
	char	*data;
}					t_gnl;

size_t				htab_hash(char *str, size_t size);
void				*htab_insert(t_htab tab, char *key, void *d, size_t s);
t_htab				htab_new(size_t size);
void				*htab_get(t_htab tab, char *key);
void				htab_delete(t_htab *tab, void (*ft_free)());
void				htab_iter(t_htab tab, t_iter_function f, void *extra);
void				htab_iter_remove_if(t_htab tab, t_iter_function f, void *extra);
bool				htab_remove(t_htab tab, char *key);

int					get_next_line(int const fd, char **line);

void				*list_to_array(t_list lst, size_t size);
void				*list_push(t_list *lst, void const *content, size_t content_size);
void				*list_add(t_list *lst, void const *content, size_t content_size);
void				list_iter(t_list *lst, t_iter_function f, void *extra);
void				list_iter_remove_if(t_list *lst, t_iter_function f, void *extra);
t_list				list_new(void);
t_list				list_new_init(void *content, size_t content_size);
void				list_delete(t_list *lst, void (*delete_node)(void *content));

t_array_list		array_list_new(unsigned nb, unsigned elem_size);
void				array_list_add(t_array_list *array, void *data);
void				array_list_iter(t_array_list array, t_iter_array_function f, void *extra);
void				array_list_iter_remove_if(t_array_list *array, t_iter_array_function f, void *extra);
void 				array_list_delete(t_array_list *array);
void				array_list_resize(t_array_list *array, unsigned size);
void				array_list_remove(t_array_list *array, int idx);
void 				array_list_sort(t_array_list array, int (*cmpf)(void const *, void const *));
void				array_list_clear(t_array_list *array);
void 				array_list_expand(t_array_list *array);
void				array_list_add_all(t_array_list *array, void *buffer, unsigned nb);

t_btree				*btree_create_node(void *item);
void				btree_apply_infix(t_btree *root, void (*applyf)(void *));
void				btree_apply_prefix(t_btree *root, void (*applyf)(void *));
void				btree_apply_suffix(t_btree *root, void (*applyf)(void *));
void				btree_insert_data(t_btree **root, void *item,
					int (*cmpf)(void*, void*));
void				*btree_search_item(t_btree *root, void *data_ref,
					int (*cmpf)(void*, void*));
int					btree_level_count(t_btree *root);

size_t				ft_strlen(const char *s);
size_t				ft_wcrtomb(char *s, wchar_t wc);
size_t				ft_wstrlen(const wchar_t *s);
void				ft_putchar(char c);
void				ft_putchar_(char c, int canal);
int					ft_nbrlen(long long int n);
int					ft_nbrlen_base(long long int n, int base);
int					ft_putnbr_base(unsigned long int n, char *chars, unsigned int base);
int					ft_putnbr_base_buffer(char buffer[], unsigned long int n,
					char *chars, unsigned int base);
void				ft_putnbr(int i);
void				ft_putnbr_(int i, int canal);
void				ft_putstr(const char *s);
void				ft_putstr_(const char *s, int canal);
void				ft_putendl(char const *s);
void				ft_putendl_fd(char const *s, int fd);
void				ft_putchar_fd(char c, int fd);
void				ft_putstr_fd(char const *s, int fd);
void				ft_putnbr_fd(int n, int fd);
int					ft_putwc(wchar_t wc);
int					ft_putws(wchar_t *ws);
int					ft_atoi(const char *s);
float				ft_atof(const char *s);

void				*ft_memset(void *b, int c, size_t len);
void				ft_bzero(void *s, size_t n);
void				*ft_memcpy(void *dest, const void *src, size_t n);
void				*ft_memccpy(void *dest, const void *src, int c, size_t n);
void				*ft_memmove(void *dest, const void *src, size_t len);
void				*ft_memchr(const void *s, int c, size_t n);
int					ft_memcmp(const void *s1, const void *s2, size_t n);
void				*ft_memalloc(size_t size);
void				ft_memdel(void **ap);

int					ft_isascii(int c);
int					ft_isprint(int c);
int					ft_isdigit(int c);
int					ft_isalpha(int c);
int					ft_isalnum(int c);
int					ft_is_whitespace(int c);
int					ft_toupper(int c);
int					ft_tolower(int c);
void				ft_swap(int *a, int *b);

int					ft_strsplit_size(char **tab);
int					ft_strsplit_contains(char **tab, char *sequence);
void				ft_strsplit_free(char **tab);
char				**ft_strsplit(char const *s, char c);
void				ft_swip_strsplit(char **tab, int from, int amount);
char				**ft_strsplit_whitespaces(char const *s);

char				*ft_strdup(const char *s1);
char				*ft_strndup(const char *src, int length);
char				*ft_strndup(const char *src, int n);
char				*ft_strcpy(char *dst, const char *str);
char				*ft_strncpy(char *dst, const char *src, size_t n);
char				*ft_strstr(const char *s1, const char *s2);
char				*ft_strnstr(const char *s1, const char *s2, size_t n);
size_t				ft_strlcat(char *dest, const char *src, size_t size);
char				*ft_strncat(char *dest, const char *src, size_t size);
char				*ft_strcat(char *s1, const char *s2);
int					ft_strcmp(const char *s1, const char *s2);
int					ft_void_cmp(void *a, void *b);
int					ft_voidvoid(void *a, void *b);
int					ft_strncmp(const char *s1, const char *s2, size_t n);
char				*ft_strchr(const char *s, int c);
char				*ft_strrchr(const char *s, int c);
char				*ft_strnew(size_t size);
char				*ft_itoa(int n);
void				ft_strdel(char **as);
int					ft_tab_contains(char **tab, char *sequence);
int					ft_indexof(char *str, char c);
int					ft_last_indexof(char *str, char c);

char				*ft_strrev(char *str);
void				ft_strclr(char *s);
void				ft_striter(char *s, void (*f)(char*));
void				ft_striteri(char *s, void (*f)(unsigned int, char*));
char				*ft_strmap(char const *s, char (*f)(char));
char				*ft_strmapi(char const *s, char(*f)(unsigned int, char));
int					ft_strequ(char const *s1, char const *s2);
int					ft_strnequ(char const *s1, char const *s2, size_t n);
char				*ft_strsub(char const *s, unsigned int start, size_t len);
char				*ft_strjoin(char const *s1, char const *s2);
char				*ft_strtrim(char const *s);
int					ft_ccount(char *str, char c);

int					ft_match(char *s1, char *s2);

#endif
