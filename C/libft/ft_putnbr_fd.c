/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_fd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <rpereira@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/04 16:39:55 by rpereira          #+#    #+#             */
/*   Updated: 2014/12/27 12:19:48 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
static int	ft_putnbr_to_tab(char str[], unsigned long long int n,
			char *charset, unsigned int base)
{
	char	tmp;
	int		i;
	int		nbr_len;

	i = 0;
	nbr_len = 0;
	while (n != 0)
	{
		str[nbr_len] = charset[(n % base)];
		n = n / base;
		nbr_len = nbr_len + 1;
	}
	while (i < nbr_len / 2)
	{
		tmp = str[i];
		str[i] = str[nbr_len - 1 - i];
		str[nbr_len - 1 - i] = tmp;
		i++;
	}
	return (nbr_len);
}

static int	ft_reverse_base_nbr(char str[], unsigned long long int n,
			char *charset, unsigned int base)
{
	int		nbr_len;

	if (n == 0)
	{
		str[0] = '0';
		return (1);
	}
	nbr_len = ft_putnbr_to_tab(str, n, charset, base);
	return (nbr_len);
}

void		ft_putnbr_fd(int n, int fd)
{
	int		nbr_len;
	char	str[(nbr_len = ft_nbrlen(n))];

	ft_reverse_base_nbr(str, n, "0123456798", 10);
	write(fd, str, nbr_len);
}
