/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atof.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/25 19:19:44 by rpereira          #+#    #+#             */
/*   Updated: 2015/05/06 18:59:15 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "float.h"

static float	ft_atof_do(char const *str, int sign, int in_fract, int divisor)
{
	float	integer;
	float	fraction;

	integer = 0;
	fraction = 0;
	while (*str != '\0')
	{
		if (*str >= '0' && *str <= '9')
		{
			if (in_fract && (divisor = divisor * 10))
				fraction = fraction * 10 + (*str - '0');
			else
				integer = integer * 10 + (*str - '0');
		}
		else
		{
			if (!in_fract && *str != '.')
				break ;
			else
				in_fract = 1;
		}
		++str;
	}
	return (sign * (integer + fraction / divisor));
}

float			ft_atof(char const *str)
{
	int		sign;

	if (str == NULL || !*str)
		return (0);
	if (ft_strstr(str, "MAX") != NULL)
		return (FLT_MAX);
	if (ft_strstr(str, "MIN") != NULL)
		return (-FLT_MAX);
	while (*str && (*str == ' ' || *str == '\t' || *str == '\n'))
		str++;
	sign = 1;
	if (*str == '-')
	{
		++str;
		sign = -1;
	}
	else if (*str == '+')
		++str;
	return (ft_atof_do(str, sign, 0, 1));
}
