/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dimension.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/02 13:21:50 by rpereira          #+#    #+#             */
/*   Updated: 2015/06/02 13:22:04 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/**
**	Theses dimension have to match with GLSL shaders ones
*/

#ifndef DIMENSION_H

# define DIMENSION_H

# define CHUNK_RENDER_DISTANCE 16	//in chunks
# define CHUNK_LOAD_DISTANCE (CHUNK_RENDER_DISTANCE * 2)

# define CHUNK_VERTICES 16	//numbers of vertices per chunk line (so CHUNK_VERICES * CHUNK_VERTICES vertices per chunk)
# define CHUNK_SIZE 512		//unit size of a chunk

# define CHUNK_ROUGHNESS CHUNK_SIZE
# define CHUNK_BASE_HEIGHT (CHUNK_ROUGHNESS / 2)	//so the median height is always at 0

# define SKYBOX_SIZE (500.0f)
# define SKYBOX_DIAGONAL (sqrt(SKYBOX_SIZE * SKYBOX_SIZE * 2))

#endif
