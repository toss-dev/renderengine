/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/23 11:13:17 by rpereira          #+#    #+#             */
/*   Updated: 2015/06/02 20:30:23 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MAIN_H
# define MAIN_H

# include "opengl.h"

# include "libft.h"
# include "logger.h"
# include "maths.h"
# include "texture.h"
# include "dimension.h"
# include "model.h"

# define WIDTH 1200
# define HEIGHT (WIDTH / (16 / 9.0f))
# define TITLE "Render engine"

# define CHUNK_HASH_MAP_SIZE 8192
# define CHUNK_BATCH_HTAB_SIZE 512
# define BATCH_HASH_MAP_SIZE 1024
# define WATER_TILES_HASH_MAP_SIZE 2048
# define PARTICLES_MAX 4096
# define CHUNK_KEY_SIZE 64

# define CYCLE_MIN 2 				// minutes for a cycle (real time)
# define TPS 20						// tick per seconds
# define TPC (TPS * 60 * CYCLE_MIN)	// tick per cycle
# define SECOND_PER_TICK (1 / (double)TPS)

/** following define has to match with the ones in "skybox.vertex" shader */
# define NIGHT_END 0.0f //constant, should not be changed
# define DAY_START 0.10f
# define DAY_END 0.50f
# define NIGHT_START 0.60f
# define DAY_MID ((DAY_START + DAY_END) / 2)
# define NIGHT_MID ((NIGHT_START + NIGHT_END) / 2)
# define DAY_TRANSITION (DAY_START - NIGHT_END)
# define NIGHT_TRANSITION (NIGHT_START - DAY_END)

# define DEFAULT_FOG_COLOR new_point3(1, 1, 1)
# define DEFAULT_FOG_DENSITY 0.0004f
# define DEFAULT_FOG_GRADIENT 1.5f

typedef struct	s_entity
{
	t_point3	rot;
	t_point3	scale;
	t_point3	pos;
	t_point3	rot_vec;
	t_point3	scale_vec;
	t_point3	pos_vec;
	unsigned	texture_id;
	unsigned 	model_id;
	unsigned	id;
	float		health;
	float 		speed;
}				t_entity;

typedef struct	s_particle
{
	t_point3	rot;
	t_point3	scale;
	t_point3	pos;
	t_point3	rot_vec;
	t_point3	scale_vec;
	t_point3	pos_vec;
	int			texture_id;
	unsigned	id;
	float		health;
	float 		speed;
	t_point3	color;
	int 		texture_atlas_id;
	unsigned	timer;
	float 		camera_dist;
}				t_particle;

enum	e_entity
{
	EntityStatic,
	ENTITY_MAX_TYPE
};

enum 	e_model_type
{
	ModelTree,
	MODEL_MAX
};

enum	e_particles
{
	ParticleRain,
	ParticleMoving,
	ParticleMagic,
	PARTICLES_MAX_TYPE
};

enum	e_camera_config
{
	C_MOVE_LEFT = 1,
	C_MOVE_RIGHT = 2,
	C_MOVE_FORWARD = 4,
	C_MOVE_BACKWARD = 8
};

typedef struct	s_camera
{
	float		*view_matrix;
	float		*project_matrix;
	t_point3	pos;
	t_point3	vec_pos;
	t_point3	vec_rot;
	t_point3	look_vec;
	t_point2	chunk_idx;
	float		fov;
	float		near;
	float		far;
	float		pitch;
	float		yaw;
	int			state;
}				t_camera;

typedef struct	s_program
{
	GLuint	id;
	GLuint	vertex_shader;
	GLuint	fragment_shader;
	GLuint	transf_matrix;
	GLuint	project_matrix;
	GLuint	view_matrix;
	GLuint	light_position;
	GLuint	light_color;
	GLuint	fog_color;
	GLuint	fog_gradient;
	GLuint	fog_density;
	GLuint	day_factor;
	GLuint	cubemap_day;
	GLuint	cubemap_night;
	GLuint	sun_position;
	GLuint	sun_color;
	GLuint	day_ratio;
	GLuint	particle_color;
	GLuint 	texture_atlas_cols;
	GLuint 	texture_atlas_lines;
	GLuint 	particle_texture_atlas_id;
}				t_program;

enum e_cumebap_id
{
	CUBEMAP_DAY,
	CUBEMAP_DAY2,
	CUBEMAP_NIGHT,
	MAX_CUBEMAP
};

typedef struct 	s_sun
{
	t_point3	color;
	t_point3	pos;
	t_point3	skybox_pos;
}				t_sun;

typedef struct 	s_sky
{
	t_sun		sun;
	t_model		model;
	GLuint		cubemaps[MAX_CUBEMAP]; 	// every cubemaps, see enum e_cubemap_id
	GLuint		cubemap_day;
	GLuint		cubemap_night;
	t_point3	rot;
}				t_sky;

typedef struct 	s_weather
{
	t_point3	fog_color;
	float 		fog_density;
	float 		fog_gradient;
	bool		is_raining;
	float 		rain_start;			//rain start day time
	float 		rain_stop;			// rain end day time
	float 		rain_transition;	//transition from sunny to rainy weather
}				t_weather;

enum e_program_id
{
	PROGRAM_MODEL,
	PROGRAM_SKYBOX,
	PROGRAM_PARTICLES,
	PROGRAM_WATER,
	PROGRAM_MAX
};

typedef struct 	s_water_tile
{
	t_point3	pos;
	t_point2	size;
	bool		loaded;
}				t_water_tile;

typedef struct	s_chunk
{
	t_model model;
	t_list	entities;
	int		indx;
	int		indz;
	float	posx;
	float	posz;
	float	ys[CHUNK_VERTICES][CHUNK_VERTICES];
	bool	loaded;
	bool 	generated;
}				t_chunk;

typedef struct	s_world
{
	t_htab		chunks;
	t_list		entities;
	t_htab		water_tiles;
	t_weather	weather;
	float 		day_ratio;
	double		ticks;
}				t_world;

typedef struct 	s_water
{
	t_model model;
	GLuint	fbos[1];
}				t_water;

typedef struct	s_renderer
{
	t_batch 		model_batchs[MODEL_MAX];
	t_camera		camera;
	t_program		program[PROGRAM_MAX];
	GLuint			textures[TEXTURE_MAX];
	t_texture_atlas	textures_atlases[TEXTURE_ATLASES_MAX];
	t_sky 			sky;
	t_array_list	particles;
	t_model 		particle_model;
	t_water 		water;
}				t_renderer;

typedef struct 	s_window
{
	GLFWwindow	*ptr;
}				t_window;

typedef struct	s_env
{
	t_window	window;
	t_renderer	renderer;
	t_world		world;
}				t_env;

extern t_env	*g_env;

/**
**	initializers
*/
void			initArgs(t_env *env, int argc, char **argv);
void			initOpenGL(t_window *win, t_renderer *renderer);
void			initWindow(t_window *win);
void			initEvent(t_window *win);
void			initWorld(t_world *world);
void			initRenderer(t_renderer *renderer);

void			startLoop(t_env *env);
void			checkGlError(void);


/**
**	Chunk related functions
*/
void			updateChunks(t_world *world);

t_chunk			*getChunk(t_htab chunks, int indx, int indz);
t_chunk			*getChunkAt(t_htab chunks, float x, float z);
void			generateChunkKey(char buff[], int indx, int indz);
float			getHeightAt(t_world *world, float x, float z);
t_pair			getChunkIndexForPos(float posx, float posz);

void			loadChunk(t_chunk *chunk);
void			unloadChunk(t_chunk *chunk);
void			loadChunkAt(t_htab htab, int indx, int indz);
void			unloadChunkAt(t_htab htab, int indx, int indz);

t_chunk			*createChunk(t_world *world, int x, int z);
t_chunk 		*addChunk(t_world *world, int x, int z);
void			chunk_delete(t_chunk *chunk);
void			generateChunk(t_chunk *chunk);
void			generateSimplexNoise(t_chunk *chunk);

void			generate_toss(t_htab chunks, t_chunk *chunk);
void			generate_diamond_square(t_htab chunks, t_chunk *chunk);
void			initSimplexNoise(void);

float			rand_float_in_range(float min, float max);
int 			rand_in_range(int min, int max);
int 			rand_int(int max);


/**
**	renderer functions
*/
void			updateCamera(t_camera *camera);
void 			updateRenderer(t_world *world, t_renderer *renderer);
void			updateEffects(t_world *world, t_renderer *renderer);
void			render(t_world *world, t_renderer *renderer, t_window *win);
void			renderEntities(t_world *world, t_renderer *renderer);
void			renderParticles(t_world *world, t_renderer *renderer);
void			renderChunks(t_world *world, t_renderer *renderer);
void			renderModel(t_program program, t_model model);
void			addEntityRendering(t_entity *entity, t_renderer *renderer);
void			renderSky(t_world *world, t_renderer *renderer);
void			renderWaterTiles(t_world *world, t_renderer *renderer);

/**
**	uniform loading (for shading uniform variables)
*/
void			load_uniform_matrix(GLuint id, float *matrix);
void			load_uniform_float(GLuint id, float value);
void			load_uniform_vec(GLuint id, t_point3 p);
void			load_uniform_weather(t_weather weather, t_program *program);
void			load_uniform_int(GLuint id, GLuint integer);


/** particles*/
void 			updateParticles(t_renderer *renderer);
void 			addParticle(t_renderer *renderer, t_particle particle);
t_particle		new_particle(t_point3 pos, t_point3 scal, t_point3 rot, t_point3 color,
				int id, GLuint texture_id, float life);

/**
**	Loading functions (called at initialization only)
*/
void			loadModels(t_renderer *renderer);
void			loadTextures(t_renderer *renderer);
void			loadPrograms(t_renderer *renderer);
GLuint			loadShader(char const *vertex, GLuint i);
void			loadCamera(t_renderer *renderer);
void 			loadSky(t_renderer *renderer);
void 			loadParticles(t_renderer *renderer);
void 			loadEntitiesRenderer(t_renderer *renderer);
void			loadWater(t_water *water);
void			loadWeather(t_world *world);

/**
**	World
*/
void			updateWorld(t_world *world);

/**
**	water
*/
void			createWaterTile(t_htab tiles, t_water_tile tile);
t_water_tile	new_water_tile(t_point3 pos, t_point2 size);

/**
**	weather functions
*/
void			updateWeather(t_world *world);
void			updateSky(t_world *world, t_renderer *renderer);
void			calculateSkyColors(t_world *world, t_renderer *renderer);
void			calculateFog(t_world *world);
t_point3		mix_color(t_point3 a, t_point3 b, float factor);
float			get_skybox_texture_factor(t_world *world);

/**
**	entities
*/
t_entity		*spawnStaticEntity(t_world *world, t_entity entity);
t_entity		*spawnDynamicEntity(t_world *world, t_entity entity);
int				updateEntity(t_entity *entity, t_world *world);
t_entity		new_entity(t_point3 pos, t_point3 scal, t_point3 rot, int id, unsigned texture_id, unsigned model_id);


/**
**	Memory managment
*/
void			batch_delete(t_batch *batch);
void			shader_delete(GLuint program_id, GLuint shader_id);
void			program_delete(t_program *program);
void			sky_delete(t_sky *sky);
void			textures_delete(t_renderer *renderer);
void 			waterDelete(t_water *water);
void			exitProperly(t_env *env);

/**
**	events
*/
void			cameraControlRelease(t_camera *cam, int k);
void			cameraControlPress(t_camera *cam, int k);
void			keyCallback(GLFWwindow *window, int key, int scancode, int action, int mods);
void			cursorMoveCallback(GLFWwindow *window, double x, double y);
void			mouseButtonCallback(GLFWwindow* window, int button, int action, int mods);

#endif
