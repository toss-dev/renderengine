/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   model.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/23 11:13:17 by rpereira          #+#    #+#             */
/*   Updated: 2015/06/02 20:30:23 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MODEL_H
# define MODEL_H

# include "libft.h"
# include "opengl.h"

enum	e_attr
{
	ATTR_POS,
	ATTR_TEXTURE,
	ATTR_NORMALS,
	ATTR_INDICES,
	ATTR_MAX
};

typedef struct	s_model
{
	GLuint		vao_id;
	GLuint		vbos[ATTR_MAX];
	GLuint		vertex_count;
}				t_model;

typedef struct	s_batch
{
	t_model			model;
	t_array_list	entities;
}				t_batch;

typedef struct	s_model_data
{
	GLfloat	*vertices;
	GLuint	vertices_count;
	GLuint	*indices;
	GLuint	triangles_count;
	GLfloat	*uvs;
	GLuint	uvs_count;
	GLfloat	*normals;
	GLuint	normals_count;
}				t_model_data;

typedef struct	s_obj_parser
{
	t_list	vertices;
	t_list	indices;
	t_list	uvs;
	t_list	normals;
	GLuint	vertices_count;
	GLuint	triangles_count;
	GLuint	uvs_count;
	GLuint	normals_count;
	int		c_line;
}				t_obj_parser;


/** parsing */
t_model			loadObjModel(char const *path);
int				parseObjFile(char const *file, t_obj_parser *parser);
void			objParserToModelData(t_obj_parser *parser, t_model_data *d);

t_model			loadModel(t_model_data *data);
t_model 		loadModelFromVertices(GLfloat vertices[], size_t vertices_count);

t_batch			batch_new(char *file, unsigned entities);

void			modelDelete(t_model *model);

enum e_model_id
{
	TOSS_MODEL0,
	TOSS_MODEL1,
	TOSS_MODEL2,
	TOSS_MODEL3,
	TOSS_MODEL_MAX
};

/** rendering models */
void			bindModel(unsigned dst, t_model model);
void			unbindModel(unsigned dst);
void			drawModel(unsigned dst);

#endif