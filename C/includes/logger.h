/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   logger.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/23 12:05:50 by rpereira          #+#    #+#             */
/*   Updated: 2015/04/23 12:17:46 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LOGGER_H
# define LOGGER_H

# include "libft.h"
# include <time.h>

enum	e_log_level
{
	LOG_DEFAULT,
	LOG_FINE,
	LOG_WARNING,
	LOG_ERROR,
	LOG_FATAL
};

void	logger_log(char const *str, int fd, int level, int indent);

#endif
