/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   scope.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/23 11:13:17 by rpereira          #+#    #+#             */
/*   Updated: 2015/05/15 15:36:20 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MAIN_H
# define MAIN_H

# include "opengl.h"

# include "libft.h"
# include "logger.h"
# include "libft_maths.h"
# include "texture.h"
# include "parser.h"

# define WIDTH 1900
# define HEIGHT (WIDTH / 1.6)
# define TITLE "Render engine"

# define CHUNK_HASH_MAP_SIZE 8192
# define BATCH_HASH_MAP_SIZE 1024

# define CHUNK_VERTICES 32
# define CHUNK_VERTEX_COUNT (CHUNK_VERTICES * CHUNK_VERTICES)
# define CHUNK_FLOAT_COUNT (CHUNK_VERTICES * CHUNK_VERTICES * 3)
# define CHUNK_SIZE 64
# define CHUNK_INDICES_COUNT (6 * (CHUNK_VERTICES - 1) * (CHUNK_VERTICES - 1))

# define CHUNK_SQUARE_SIZE (CHUNK_SIZE / (float)(CHUNK_VERTICES - 1))

# define CHUNK_PER_HEIGHTMAP 6
# define CHUNK_Y_FACTOR (CHUNK_SIZE)

typedef void	(*t_entity_function)();

enum	e_entity
{
	EntityDefault,
	EntityPassive,
	ENTITY_MAX
};

typedef struct	s_entity
{
	t_point3	rot;
	t_point3	scale;
	t_point3	pos;
	t_point3	rot_vec;
	t_point3	scale_vec;
	t_point3	pos_vec;
	int			texture_id;
	unsigned	id;
	float		speed;
}				t_entity;

enum	e_camera_config
{
	C_MOVE_LEFT = 1,
	C_MOVE_RIGHT = 2,
	C_MOVE_FORWARD = 4,
	C_MOVE_BACKWARD = 8
};

typedef struct	s_camera
{
	float		*view_matrix;
	float		*project_matrix;
	t_point3	pos;
	t_point3	vec_pos;
	t_point3	vec_rot;
	float		fov;
	float		near;
	float		far;
	float		pitch;
	float		yaw;
	int			state;
}				t_camera;

typedef struct	s_program
{
	GLuint	id;
	GLuint	vertex_shader;
	GLuint	fragment_shader;
	GLuint	transf_matrix;
	GLuint	project_matrix;
	GLuint	view_matrix;
	GLuint	light_position;
	GLuint	light_color;
	GLuint	sky_color;
}				t_program;

typedef struct	s_light
{
	t_point3	pos;
	t_point3	color;
}				t_light;

typedef struct	s_vao_attr
{
	int		index;
	GLfloat	*floats;
	int		count;
	int		vertex_size;
}				t_vao_attr;

enum	e_attr
{
	ATTR_POS,
	ATTR_TEXTURE,
	ATTR_INDICES,
	ATTR_NORMALS,
	ATTR_MAX
};

typedef struct	s_material
{
	float	shiness;
}				t_material;

typedef struct	s_model
{
	GLuint		vao_id;
	GLuint		vbos[ATTR_MAX];
	GLuint		vertex_count;
	t_material	material;
}				t_model;

typedef struct	s_batch
{
	t_model	model;
	t_list	*entities;
}				t_batch;

typedef struct 	s_skybox
{
	GLuint	cubemap;
	t_model	model;
}				t_skybox;

typedef struct 	s_sky
{
	t_skybox	skybox;
	t_point3	color;
	t_light		sun;
}				t_sky;

enum e_program_id
{
	PROGRAM_MODEL,
	PROGRAM_CHUNK,
	PROGRAM_SKYBOX,
	PROGRAM_MAX
};

typedef struct	s_renderer
{
	t_camera	camera;
	t_program	program[PROGRAM_MAX];
	t_sky 		sky;
	GLuint		textures[TEXTURE_MAX];
}				t_renderer;

typedef struct	s_chunk
{
	t_model model;
	int		indx;
	int		indz;
	float	posx;
	float	posz;
	float	ys[CHUNK_VERTICES][CHUNK_VERTICES];
	bool	loaded;
}				t_chunk;

typedef struct	s_world
{
	t_htab	chunks;
	t_htab	batchs;
	t_bmp	height_map;
	size_t	ticks;
}				t_world;

typedef struct 	s_window
{
	GLFWwindow	*ptr;
}				t_window;

typedef struct 	s_timer
{
	size_t	begin;
	size_t	end;
	size_t	maxfps;
	size_t	fps;
	size_t	sleep_needed;
}				t_timer;

typedef struct	s_env
{
	t_window	window;
	t_renderer	renderer;
	t_world		world;
	t_timer		timer;
}				t_env;

extern t_env	*g_env;

/**
**	initializers
*/
void			init_args(t_env *env, int argc, char **argv);
void			init_opengl(t_window *win, t_renderer *renderer);
void			init_window(t_window *win);
void			init_event(t_env *env);
void			init_world(t_world *world);
void			init_renderer(t_renderer *renderer);

void			start_loop(t_env *env);

void			timer_prepare(t_timer *c);
void			timer_begin(t_timer *c);
void			timer_end(t_timer *c);


/**
**	Chunk related functions
*/
t_chunk			*get_chunk(t_htab chunks, int x, int z);
t_chunk			*get_chunk_at(t_htab chunks, float x, float z);
float			get_height_at(t_world *world, float x, float z);
void			create_chunk(t_world *world, int x, int z);
void			generate_chunk(t_chunk *chunk, t_bmp *heightmap);
void			load_chunk(t_chunk *chunk);
void			unload_chunk(t_chunk *chunk);


/**
**	renderer funcitons
*/
void			update_camera(t_camera *camera);

void			render(t_world *world, t_renderer *renderer, t_window *win);
void			render_entities(t_world *world, t_renderer *renderer);
void			render_chunks(t_world *world, t_renderer *renderer);
void			render_model(t_program program, t_model model);
void			render_sky(t_sky *sky, t_renderer *renderer);

void			prepare_model_rendering(t_model model);
void			end_model_rendering(void);
void			draw_model(t_model model);

void			load_textures(t_renderer *renderer);
void			load_programs(t_renderer *renderer);
GLuint			load_shader(char const *vertex, GLuint i);
void			load_camera(t_renderer *renderer);
void			load_sky(t_sky *sky);

void			load_uniform_matrix(GLuint id, float *matrix);
void			load_uniform_float(GLuint id, float value);
void			load_uniform_vec(GLuint id, t_point3 p);

t_batch			new_batch(t_entity entity, char *file);
t_light			new_light(t_point3 pos, t_point3 color);
t_model			load_model(t_model_data *data);
t_model 		load_model_from_vertices(GLfloat vertices[], size_t vertices_count);
t_model			load_obj_model(char const *file);


/**
**	World
*/
void			update_world(t_world *world);

/**
**	entities
*/
void			spawn_entity(t_world *world, t_entity e, char *obj);
void			update_entity(t_entity *entity, t_world *world);
t_entity		new_entity(t_point3 pos, t_point3 scal, t_point3 rot, t_pair p);


/**
**	Memory managment
*/
void			batch_delete(t_batch *batch);
void			model_delete(t_model *model);
void			shader_delete(GLuint program_id, GLuint shader_id);
void			program_delete(t_program *program);
void			sky_delete(t_sky *sky);
void			textures_delete(t_renderer *renderer);
void			exit_properly(t_env *env);


/**
**	camera
*/
void			update_camera_move(t_camera *cam);
void			camera_control_release(t_camera *cam, int k);
void			camera_control_press(t_camera *cam, int k);

/**
**	events
*/
void			prepare_key_event(t_window *window);
void			prepare_mouse_event(t_window *window);

#endif
