/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   logger.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/23 12:05:50 by rpereira          #+#    #+#             */
/*   Updated: 2015/04/23 12:17:46 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef OPEN_GL_H
# define OPEN_GL_H

# ifdef __APPLE__
# 	define GLFW_INCLUDE_GLCOREARB
# endif

# ifdef _WIN32
# 	include <GL/glew.h>
# endif

# include <GLFW/glfw3.h>

#endif
