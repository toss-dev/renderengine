/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   texture.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpereira <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/09 15:24:31 by rpereira          #+#    #+#             */
/*   Updated: 2015/05/12 19:01:36 by rpereira         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TEXTURE_H
# define TEXTURE_H

# include "opengl.h"

# define BMP_HEADER_SIZE 54
# define OCTET_PER_PIXEL_BMP 3

# define OCTET_PER_PIXEL_PNG 4

enum	e_texture_id
{
	TextureColor,
	TextureGrass,
	TextureSmallGrass,
	TextureChunk,
	TEXTURE_MAX
};

enum	e_texture_atlases_id
{
	TextureAtlasRaindrop,
	TextureAtlasLeave,
	TextureAtlasMagic,
	TextureAtlasFlamedrop,
	TextureAtlasFlame,
	TextureAtlasExplosion,
	TEXTURE_ATLASES_MAX
};

typedef struct	s_bmp
{
	GLubyte	*pixels;
	GLubyte	header[BMP_HEADER_SIZE];
	int		data_idx;
}				t_bmp;

typedef struct	s_texture
{
	unsigned	width;
	unsigned	height;
	GLubyte		*pixels;
	GLenum		internalformat;
	GLenum		format;
}				t_texture;

typedef struct 	s_texture_format
{
	char *extension;
	int (*decoder_funct)(char const *, t_texture *);
}				t_texture_format;


typedef struct 	s_texture_atlas
{
	unsigned	cols;
	unsigned	lines;
	GLuint		id;
}				t_texture_atlas;

int				load_png_file(char const *file, t_texture *texture);
int				load_bmp_file(char const *file, t_texture *texture);

void			texture_reverse_ys(t_texture *texture, int octet_per_pixel);

#endif
