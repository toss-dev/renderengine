#version 400 core

in vec3 position;
in vec2 uv;

out vec2 pass_uv;
out	vec3 transf_normal;
out float visibility;

uniform mat4 proj_matrix;
uniform mat4 view_matrix;
uniform mat4 transf_matrix;

uniform float fog_density;
uniform float fog_gradient;

uniform int texture_atlas_cols;
uniform int texture_atlas_lines;
uniform int particle_texture_atlas_id;

void	main(void)
{
	// position calculation
	vec4	transf_position;
	vec4	relative_position_to_camera;

	transf_position = transf_matrix * vec4(position, 1.0);
	relative_position_to_camera = view_matrix * transf_position;
	gl_Position = proj_matrix * relative_position_to_camera;
	transf_normal = (transf_matrix * vec4(0, 1, 0, 0)).xyz;



	// fog calculation
	float	distance;

	distance = length(relative_position_to_camera.xyz);
	visibility = exp(-pow(distance * fog_density, fog_gradient));
	visibility = clamp(visibility, 0, 1);


	//texture atlas calculation
	float	texx;
	float	texy;
	float	textunitx;
	float	textunity;

	texx = particle_texture_atlas_id % texture_atlas_cols;		// x texture to use in texture atlases
	texy = particle_texture_atlas_id / texture_atlas_lines;	// y texture to use in texture atlases
	textunitx = 1.0 / texture_atlas_cols;
	textunity = 1.0 / texture_atlas_lines;
	pass_uv.x = uv.x / texture_atlas_cols + texx * textunitx;
	pass_uv.y = uv.y / texture_atlas_lines + texy * textunity;
}
